use master
go;
-- Cở sở dữ liệu quản lý bánh hàng

-- -- join, sp, UDF, View, CTE 

-- KhachHang(MaKH, HoTen, DiaChi, SoDT, NgSinh, Doanhso, NgDk)

-- NhanVien(MaNV, HoTen, SoDT, NgVaoLam)

-- SanPham(MaSP, TenSP, DVT, NuocSX, Gia)

-- HoaDon(SoHD, NgHD, MaKH, MaNV, TriGia)

-- CTHD(SoHD, MaSP, SL)



-----------

-- Tạo bảng KhachHang
CREATE TABLE KhachHang (
    MaKH INT PRIMARY KEY,
    HoTen NVARCHAR(255),
    DiaChi NVARCHAR(255),
    SoDT NVARCHAR(15),
    NgSinh DATE,
    DoanhSo MONEY,
    NgDk DATE
);

-- Thêm dữ liệu vào bảng KhachHang
INSERT INTO KhachHang (MaKH, HoTen, DiaChi, SoDT, NgSinh, DoanhSo, NgDk)
VALUES
    (1, N'Nguyễn Văn A', N'123 Đường ABC, Quận XYZ', '0123456789', '1990-01-01', 1000.50, '2020-01-15'),
    (2, N'Trần Thị B', N'456 Đường XYZ, Quận ABC', '0987654321', '1985-03-15', 800.75, '2019-12-10'),
    (3, N'Lê Văn C', N'789 Đường LMN, Quận PQR', '0123987654', '1995-05-20', 1200.25, '2021-02-20');

-- Tạo bảng NhanVien
CREATE TABLE NhanVien (
    MaNV INT PRIMARY KEY,
    HoTen NVARCHAR(255),
    SoDT NVARCHAR(15),
    NgVaoLam DATE
);

-- Thêm dữ liệu vào bảng NhanVien
INSERT INTO NhanVien (MaNV, HoTen, SoDT, NgVaoLam)
VALUES
    (101, N'Nguyễn Thị D', '0123456789', '2018-03-10'),
    (102, N'Trần Văn E', '0987654321', '2019-01-20'),
    (103, N'Lê Thị F', '0123987654', '2020-05-15');

-- Tạo bảng SanPham
CREATE TABLE SanPham (
    MaSP INT PRIMARY KEY,
    TenSP NVARCHAR(255),
    DVT NVARCHAR(10),
    NuocSX NVARCHAR(50),
    Gia MONEY
);

-- Thêm dữ liệu vào bảng SanPham
INSERT INTO SanPham (MaSP, TenSP, DVT, NuocSX, Gia)
VALUES
    (501, N'Bánh Gato', N'Hộp', N'Việt Nam', 12.50),
    (502, N'Bánh Quy', N'Gói', N'Việt Nam', 5.75),
	(504, N'Lẩu Tứ Xuyên', N'Hộp', N'Trung', 5.75),
    (503, N'Bánh Snack', N'Hộp', N'Nhật Bản', 15.99);

-- Tạo bảng HoaDon
CREATE TABLE HoaDon (
    SoHD INT PRIMARY KEY,
    NgHD DATE,
    MaKH INT,
    MaNV INT,
    TriGia DECIMAL(18, 2),
    FOREIGN KEY (MaKH) REFERENCES KhachHang(MaKH),
    FOREIGN KEY (MaNV) REFERENCES NhanVien(MaNV)
);

-- Thêm dữ liệu vào bảng HoaDon
INSERT INTO HoaDon (SoHD, NgHD, MaKH, MaNV, TriGia)
VALUES
    (1001, '2023-05-23', 1, 101, 35.75),
    (1002, '2023-05-23', 2, 102, 25.50),
    (1003, '2023-05-22', 3, 103, 42.25);

-- Tạo bảng CTHD
CREATE TABLE CTHD (
    SoHD INT,
    MaSP INT,
    SL INT,
    PRIMARY KEY (SoHD, MaSP),
    FOREIGN KEY (SoHD) REFERENCES HoaDon(SoHD),
    FOREIGN KEY (MaSP) REFERENCES SanPham(MaSP)
);

-- Thêm dữ liệu vào bảng CTHD
INSERT INTO CTHD (SoHD, MaSP, SL)
VALUES
    (1001, 501, 2),
    (1001, 502, 3),
	(1001, 503, 3),
    (1002, 503, 4),
	(1002, 502, 4),
	(1003, 501, 4),
	(1003, 502, 4)
	;

-----------------------------------------

-- Câu 1: In ra danh sách các sản phẩm(MaSP, TenSP) do "Viet Nam" sản xuất hoặc các sản phẩm bán trong ngày 23/052023

select MaSP, TenSP
from SanPham
where NuocSX = N'Việt Nam' 
    or MaSP in (
        select MaSP    -- duplicate
        from CTHD, HoaDon
        where NgHD = '2023-05-23' and HoaDon.SoHD = MaSP.SoHD
    );


select MaSP, TenSP
from SanPham
where NuocSX = N'Việt Nam' 
    or MaSP in (
        select MaSP    -- duplicate
        from CTHD
            join HoaDon on HoaDon.SoHD = MaSP.SoHD
        where NgHD = '2023-05-23'
    );


select MaSP, TenSP
from SanPham
where NuocSX = N'Việt Nam' 
    or MaSP in (
        select MaSP   -- duplicate
        from CTHD
        where SoHD in (
            select SoHD 
            from HoaDon
            where NgHD = '2023-05-23'
        )
    );


-- Câu 2: In ra danh sách các sản phẩm (MaSP, TenSP) không bán được

select SanPham.MaSP, TenSP
from SanPham
    left join CTHD on SanPham.MaSP = CTHD.MaSP
where CTHD.MaSP is null

select MaSP, TenSP
from SanPham
where MaSP not in (
    select MaSP
    from CTHD
);


-- Câu 3: Tìm số hóa đon đã mua tất cả các sản phẩm do Viet Nam sản xuất

;with ProductsFromVietNam as (
    select MaSP
    from SanPham
    where NuocSX = N'Việt Nam'
)
select SoHD
from CTHD
where MaSP in (select MaSP from ProductsFromVietNam)
group by SoHD
having count(MaSP) = (select count(MaSP) from ProductsFromVietNam)


-- KhachHang(MaKH, HoTen, DiaChi, SoDT, NgSinh, Doanhso, NgDk)

-- NhanVien(MaNV, HoTen, SoDT, NgVaoLam)

-- SanPham(MaSP, TenSP, DVT, NuocSX, Gia)

-- HoaDon(SoHD, NgHD, MaKH, MaNV, TriGia)

-- CTHD(SoHD, MaSP, SL)