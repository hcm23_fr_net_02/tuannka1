USE master
GO

IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'BSQL_Assignment202_TuanNKA1'
)
	DROP DATABASE BSQL_Assignment202_TuanNKA1
GO


-- Create database ASQL_Assignment202_TuanNKA1
CREATE DATABASE BSQL_Assignment202_TuanNKA1

GO

USE BSQL_Assignment202_TuanNKA1

GO
-- 1. Create the tables (with the most appropriate/economic field/column constraints & types) and add at least 10 records into each created table.

create table Trainee (
    TraineeID int PRIMARY KEY identity(1,1),
    Full_Name varchar(255) not null,
    Birth_Date Date,
    Gender varchar(10) check (Gender in ('male', 'female')),
    ET_IQ int CHECK (ET_IQ between 0 and 20),
    ET_Gmath int check (ET_Gmath between 0 and 20),
    ET_English int check (ET_English between 0 and 50),
    Training_Class varchar(50) not null,
    Evaluation_Notes varchar(max)
)

-- 2. Change the table TRAINEE to add one more field named Fsoft_Account which is a not-null & unique field.
alter table Trainee
add Fsoft_Account varchar(255) unique not null

-- Insert at least 10 records into the Trainee table
INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes, Fsoft_Account)
VALUES
    ('John Doe', '1995-01-15', 'male', 15, 10, 30, 'Class A', 'Good performance', 'john.doe@example.com'),
    ('Jane Smith', '1994-05-20', 'female', 18, 12, 45, 'Class B', 'Excellent', 'jane.smith@example.com'),
    ('Alice Johnson', '1996-03-10', 'female', 16, 11, 42, 'Class A', 'Above average', 'alice.j@example.com'),
    ('Bob Brown', '1997-08-25', 'male', 14, 9, 38, 'Class B', 'Satisfactory', 'bob.b@example.com'),
    ('Eva Adams', '1993-11-30', 'female', 19, 14, 49, 'Class C', 'Outstanding', 'eva.a@example.com'),
    ('Michael Williams', '1995-06-05', 'male', 17, 13, 46, 'Class A', 'Impressive', 'michael.w@example.com'),
    ('Olivia Davis', '1998-02-12', 'female', 20, 16, 50, 'Class B', 'Exceptional', 'olivia.d@example.com'),
    ('Daniel Lee', '1994-09-18', 'male', 12, 8, 37, 'Class C', 'Average', 'daniel.l@example.com'),
    ('Sophia Johnson', '1997-04-03', 'female', 14, 10, 40, 'Class A', 'Good progress', 'sophia.j@example.com'),
    ('William Martin', '1992-12-20', 'male', 13, 7, 35, 'Class B', 'Needs improvement', 'william.m@example.com');


-- 3. Create a VIEW that includes all the ET-passed trainees

create view ETPassedTrainees as 
	SELECT *
	FROM TRAINEE
	WHERE ET_IQ + ET_Gmath >= 20 AND ET_IQ >= 8 AND ET_Gmath >= 8 AND ET_English >= 18;
go

select * from ETPassedTrainees

-- 4. Query all the trainees who are passed the entry test, group them into different birth months.
SELECT DATEPART(MONTH, Birth_Date) AS Birth_Month, COUNT(*) AS Trainee_Count
FROM ETPassedTrainees
GROUP BY DATEPART(MONTH, Birth_Date)
ORDER BY Birth_Month;

;with ETPT as (
    select TraineeID, Full_Name, Birth_Date
    from Trainee
    where ET_IQ + ET_Gmath >= 20 AND ET_IQ >= 8 AND ET_Gmath >= 8 AND ET_English >= 18
)
select DATEPART(month, Birth_Date), count(TraineeID) as count
from ETPT
group by DATEPART(month, Birth_Date)

-- 5. Query the trainee who has the longest name, showing his/her age along with his/her basic information (as defined in the table).
select TraineeID, Full_Name, DateDiff(year, Birth_Date, getdate()) age
from trainee 
where len(Full_Name) = (
    select max(len(Full_Name))
    from trainee
)
