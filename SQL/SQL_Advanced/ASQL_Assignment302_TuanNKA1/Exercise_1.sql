CREATE DATABASE ASQL_Assignment302_TuanNKA1

GO

USE ASQL_Assignment302_TuanNKA1

GO

-------------------------------------- 1. Create the tables (with the most appropriate field/column constraints & types) and add at least 3 records into each created table. --------------------------------------

-- Create the San_Pham (Product) table
CREATE TABLE San_Pham (
    Ma_SP INT PRIMARY KEY,
    Ten_SP NVARCHAR(255) NOT NULL,
    Don_Gia MONEY NOT NULL
);

-- Insert sample data into the San_Pham table
INSERT INTO San_Pham (Ma_SP, Ten_SP, Don_Gia)
VALUES
    (1, 'Product A', 10.99),
    (2, 'Product B', 15.99),
    (3, 'Product C', 7.99);

-- Create the Khach_Hang (Customer) table
CREATE TABLE Khach_Hang (
    Ma_KH INT PRIMARY KEY,
    Ten_KH NVARCHAR(255) NOT NULL,
    Phone_No NVARCHAR(15),
    Ghi_Chu NVARCHAR(MAX)
);

-- Insert sample data into the Khach_Hang table
INSERT INTO Khach_Hang (Ma_KH, Ten_KH, Phone_No, Ghi_Chu)
VALUES
    (1, 'John Smith', '123-456-7890', 'Regular customer'),
    (2, 'Jane Doe', '987-654-3210', 'VIP customer'),
    (3, 'Robert Johnson', '555-555-5555', 'New customer');

-- Create the Don_Hang (Order) table
CREATE TABLE Don_Hang (
    Ma_DH INT NOT NULL,
    Ngay_DH DATE NOT NULL,
    Ma_SP INT NOT NULL,
    So_Luong INT NOT NULL,
    Ma_KH INT NOT NULL,
    PRIMARY KEY (Ma_DH, Ma_SP, Ma_KH),
    FOREIGN KEY (Ma_SP) REFERENCES San_Pham(Ma_SP),
    FOREIGN KEY (Ma_KH) REFERENCES Khach_Hang(Ma_KH)
);

-- Insert sample data into the Don_Hang table
INSERT INTO Don_Hang (Ma_DH, Ngay_DH, Ma_SP, So_Luong, Ma_KH)
VALUES
    (1, '2023-09-15', 1, 3, 1),
    (1, '2023-09-15', 3, 7, 1),
    (2, '2023-09-16', 2, 2, 2),
    (3, '2023-09-17', 3, 5, 3);

-------------------------------------- 2. Create an order slip VIEW which has the same number of lines as the Don_Hang, with the following information: Ten_KH, Ngay_DH, Ten_SP, So_Luong, Thanh_Tien --------------------------------------
CREATE VIEW OrderSlip AS
SELECT
    KH.Ten_KH AS Ten_KH,
    DH.Ngay_DH AS Ngay_DH,
    SP.Ten_SP AS Ten_SP,
    DH.So_Luong AS So_Luong,
    (DH.So_Luong * SP.Don_Gia) AS Thanh_Tien
FROM Don_Hang DH
JOIN Khach_Hang KH ON DH.Ma_KH = KH.Ma_KH
JOIN San_Pham SP ON DH.Ma_SP = SP.Ma_SP;
GO

SELECT * FROM OrderSlip;
GO