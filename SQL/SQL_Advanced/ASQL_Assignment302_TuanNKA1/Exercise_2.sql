USE ASQL_Assignment302_TuanNKA1

GO

CREATE SCHEMA Exercise_2

GO

-------------------------------------- 1. Create the tables (with the most appropriate field/column constraints & types) and add at least 3 records into each created table. --------------------------------------
-- Create the Department
CREATE TABLE Exercise_2.Department (
    Department_Number INT PRIMARY KEY,
    Department_Name VARCHAR(255) NOT NULL
);

-- Create the Employee_Table
CREATE TABLE Exercise_2.Employee_Table (
    Employee_Number INT PRIMARY KEY,
    Employee_Name VARCHAR(255) NOT NULL,
    Department_Number INT,
    FOREIGN KEY (Department_Number) REFERENCES Exercise_2.Department(Department_Number)
);

-- Create the Skill_Table
CREATE TABLE Exercise_2.Skill_Table (
    Skill_Code INT PRIMARY KEY,
    Skill_Name VARCHAR(255) NOT NULL
);

-- Create the Employee_Skill_Table
CREATE TABLE Exercise_2.Employee_Skill_Table (
    Employee_Number INT,
    Skill_Code INT,
    Date_Registered DATE,
    FOREIGN KEY (Employee_Number) REFERENCES Exercise_2.Employee_Table(Employee_Number),
    FOREIGN KEY (Skill_Code) REFERENCES Exercise_2.Skill_Table(Skill_Code)
);

-- Add sample data to Department
INSERT INTO .Exercise_2.Department (Department_Number, Department_Name)
VALUES
    (1, 'HR'),
    (2, 'IT'),
    (3, 'Finance');

-- Add sample data to Employee_Table
INSERT INTO Exercise_2.Employee_Table (Employee_Number, Employee_Name, Department_Number)
VALUES
    (1, 'John Smith', 1),
    (2, 'Jane Doe', 2),
    (3, 'Robert Johnson', 1);

-- Add sample data to Skill_Table
INSERT INTO Exercise_2.Skill_Table (Skill_Code, Skill_Name)
VALUES
    (101, 'Java'),
    (102, 'SQL'),
    (103, 'Python');

-- Add sample data to Employee_Skill_Table
INSERT INTO Exercise_2.Employee_Skill_Table (Employee_Number, Skill_Code, Date_Registered)
VALUES
    (1, 101, '2021-01-15'),
    (1, 103, '2022-04-23'),
    (2, 101, '2021-02-20'),
    (3, 102, '2021-03-10');

-------------------------------------- 2. Specify the names of the employees whose have skill of ‘Java’ – give >=2 solutions --------------------------------------
-- Using JOIN to find employees with the skill 'Java'
SELECT E.Employee_Name
FROM Exercise_2.Employee_Table AS E
	JOIN Exercise_2.Employee_Skill_Table AS ES ON E.Employee_Number = ES.Employee_Number
	JOIN Exercise_2.Skill_Table AS S ON ES.Skill_Code = S.Skill_Code
WHERE S.Skill_Name = 'Java';

-- Using a subquery to find employees with the skill 'Java'
SELECT Employee_Name
FROM Exercise_2.Employee_Table
WHERE Employee_Number IN (
    SELECT Employee_Number
    FROM Exercise_2.Employee_Skill_Table
    WHERE Skill_Code IN (SELECT Skill_Code FROM Exercise_2.Skill_Table WHERE Skill_Name = 'Java')
);

-------------------------------------- 3. Specify the departments which have >=3 employees, print out the list of departments’ employees right after each department. --------------------------------------
-- Get departments with >=3 employees and list employees for each department
;WITH DepartmentEmployees AS (
    SELECT E.Department_Number, STRING_AGG(Trim(E.Employee_Name), ',') AS Employees
    FROM  Exercise_2.Employee_Table E
	GROUP BY E.Department_Number
	HAVING COUNT(*) >= 2
)
SELECT * FROM DepartmentEmployees;

;WITH DepartmentEmployees AS (
    SELECT E.Department_Number, STRING_AGG(Trim(E.Employee_Name), ',') AS Employees
    FROM  Exercise_2.Employee_Table E
	GROUP BY E.Department_Number
	HAVING COUNT(*) >= 2
)
SELECT D.Department_Number, D.Department_Name, Employees
FROM DepartmentEmployees DE
	JOIN Exercise_2.Department D ON DE.Department_Number = D.Department_Number
;

-------------------------------------- 4. Use SUB-QUERY technique to list out the different employees (include employee number and employee names) who have multiple skills. --------------------------------------
-- List employees with multiple skills
SELECT E.Employee_Number, E.Employee_Name
FROM Exercise_2.Employee_Table AS E
WHERE E.Employee_Number IN (
    SELECT ES.Employee_Number
    FROM Exercise_2.Employee_Skill_Table AS ES
	GROUP BY ES.Employee_Number
	HAVING COUNT(*) > 1
);




-------------------------------------- 5.	Create a view to show different employees (with following information: employee number and employee name, department name) who have multiple skills. --------------------------------------

CREATE VIEW EmployeesWithMultipleSkills AS
SELECT E.Employee_Number, E.Employee_Name, D.Department_Name
FROM 
	(
		SELECT E.Employee_Number, E.Employee_Name, E.Department_Number 
		FROM Exercise_2.Employee_Table E
		WHERE E.Employee_Number IN (SELECT ES.Employee_Number FROM Exercise_2.Employee_Skill_Table AS ES GROUP BY ES.Employee_Number HAVING COUNT(*) > 1)
	)
	AS E
	JOIN Exercise_2.Department AS D ON E.Department_Number = D.Department_Number
	;

SELECT * FROM EmployeesWithMultipleSkills;
