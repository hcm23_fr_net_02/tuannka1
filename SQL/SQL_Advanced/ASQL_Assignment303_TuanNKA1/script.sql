-- Create database ASQL_Assignment303_TuanNKA1
CREATE DATABASE ASQL_Assignment303_TuanNKA1

GO

USE ASQL_Assignment303_TuanNKA1

GO

-------------------- 1.	Create the tables with the most appropriate/economic field/column constraints & types. Add at least 8 records into each created tables. --------------------
-- Create the DEPARTMENT table
CREATE TABLE DEPARTMENT (
    DeptNo INT PRIMARY KEY IDENTITY(1,1),
    DeptName NVARCHAR(30) NOT NULL,
    Note NVARCHAR(100)
);

-- Insert data into the DEPARTMENT table
INSERT INTO DEPARTMENT (DeptName, Note)
VALUES
    ('HR', 'Human Resources Department'),
    ('IT', 'Information Technology Department'),
    ('Sales', 'Sales Department'),
    ('Finance', 'Finance Department'),
    ('Marketing', 'Marketing Department'),
    ('Engineering', 'Engineering Department'),
    ('Operations', 'Operations Department'),
    ('Legal', 'Legal Department');

-- Create the SKILL table
CREATE TABLE SKILL (
    SkillNo INT PRIMARY KEY IDENTITY(1,1),
    SkillName NVARCHAR(30) NOT NULL,
    Note NVARCHAR(100)
);

-- Insert data into the SKILL table
INSERT INTO SKILL (SkillName, Note)
VALUES
    ('SQL', 'Structured Query Language'),
    ('C++', 'C++ Programming Language'),
    ('.NET', '.NET Programming Language'),
    ('Python', 'Python Programming Language'),
    ('Project Management', 'Project Management Skills'),
    ('Data Analysis', 'Data Analysis Skills'),
    ('Communication', 'Communication Skills'),
    ('Problem Solving', 'Problem Solving Skills');

-- Create the EMPLOYEE table
CREATE TABLE EMPLOYEE (
    EmpNo INT PRIMARY KEY,
    EmpName NVARCHAR(30) NOT NULL,
    BirthDay DATETIME NOT NULL,
    DeptNo INT NOT NULL,
    MgrNo INT DEFAULT 0 NOT NULL,
    StartDate DATETIME NOT NULL,
    Salary MONEY NOT NULL,
    Status INT DEFAULT 0 NOT NULL,
    Email NVARCHAR(30) UNIQUE NOT NULL,
    Note NVARCHAR(100),
    Level INT CHECK (Level BETWEEN 1 AND 7) NOT NULL,
    FOREIGN KEY (DeptNo) REFERENCES DEPARTMENT (DeptNo)
);

-- Insert data into the EMPLOYEE table
INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Email, Note, Level)
VALUES
    (1, 'John Smith', '1980-01-15', 1, 0, '2005-05-10', 60000, 0, 'john.smith@example.com', NULL, 5),
    (2, 'Jane Doe', '1985-03-20', 2, 1, '2010-02-18', 75000, 0, 'jane.doe@example.com', NULL, 6),
    (3, 'Robert Johnson', '1990-07-12', 1, 0, '2012-09-30', 55000, 0, 'robert.johnson@example.com', NULL, 4),
    (4, 'Mary Williams', '1988-11-28', 3, 2, '2008-08-05', 68000, 0, 'mary.williams@example.com', NULL, 5),
    (5, 'David Lee', '1992-05-03', 2, 1, '2015-04-22', 72000, 0, 'david.lee@example.com', NULL, 4),
    (6, 'Jennifer Brown', '1987-09-08', 4, 3, '2007-03-15', 58000, 0, 'jennifer.brown@example.com', NULL, 3),
    (7, 'Michael Clark', '1991-02-19', 1, 0, '2019-11-12', 63000, 0, 'michael.clark@example.com', NULL, 4),
    (8, 'Susan Wilson', '1983-04-25', 3, 2, '2023-06-08', 71000, 0, 'susan.wilson@example.com', NULL, 5);

-- Create the EMP_SKILL table
CREATE TABLE EMP_SKILL (
    SkillNo INT NOT NULL,
    EmpNo INT NOT NULL,
    SkillLevel INT CHECK (SkillLevel BETWEEN 1 AND 3) NOT NULL,
    RegDate DATETIME NOT NULL,
    Description NVARCHAR(100),
    PRIMARY KEY (SkillNo, EmpNo),
    FOREIGN KEY (SkillNo) REFERENCES SKILL (SkillNo),
    FOREIGN KEY (EmpNo) REFERENCES EMPLOYEE (EmpNo)
);

-- Insert data into the EMP_SKILL table
INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES
    (3, 1, 3, '2020-01-15', '.NET proficiency'),
    (2, 1, 2, '2019-09-23', 'C++ development skills'),
    (2, 2, 2, '2020-02-20', 'C++ development skills'),
    (3, 3, 1, '2019-12-10', '.NET knowledge'),
    (4, 4, 3, '2021-03-05', 'Python scripting skills'),
    (5, 5, 2, '2021-04-12', 'Project management certification'),
    (6, 6, 1, '2020-07-22', 'Data analysis skills'),
    (7, 7, 2, '2022-05-18', 'Excellent communication skills'),
    (8, 8, 1, '2023-08-10', 'Problem-solving expertise');


-------------------- 2.	Specify name, email and department name of the employees that have been working at least six months. --------------------
SELECT E.EmpName, E.Email, D.DeptName
FROM Employee E
	JOIN Department D ON E.DeptNo = D.DeptNo
WHERE DATEDIFF(MONTH, E.StartDate, GETDATE()) >= 6;

SELECT E.EmpName, E.Email, D.DeptName
FROM 
	(
		SELECT E.EmpName, E.DeptNo, E.Email 
		FROM Employee E 
		WHERE DATEDIFF(MONTH, E.StartDate, GETDATE()) >= 6
	) E
	JOIN Department D ON E.DeptNo = D.DeptNo
;

-------------------- 3.	Specify the names of the employees whore have either 'C++' or '.NET' skills. --------------------

SELECT DISTINCT E.EmpName
FROM Employee E
	JOIN Emp_Skill ES ON E.EmpNo = ES.EmpNo
	JOIN Skill S ON ES.SkillNo = S.SkillNo
WHERE S.SkillName IN ('C++', '.NET');

SELECT E.EmpName
FROM Employee E
WHERE E.EmpNo IN (
	SELECT EmpNo
	FROM Emp_Skill ES
	WHERE ES.SkillNo IN (
		SELECT S.SkillNo
		FROM Skill S
		WHERE S.SkillName IN ('C++', '.NET')
	)
);

-------------------- 4.	List all employee names, manager names, manager emails of those employees. --------------------

SELECT
    E.EmpName AS EmployeeName,
    M.EmpName AS ManagerName,
    M.Email AS ManagerEmail
FROM
    Employee E
	LEFT JOIN Employee M ON E.MgrNo = M.EmpNo;


-------------------- 5.	Specify the departments which have >=2 employees, print out the list of departments’ employees right after each department. --------------------
SELECT D.DeptName
FROM Department D
	JOIN Employee E ON D.DeptNo = E.DeptNo
GROUP BY D.DeptName
HAVING COUNT(E.EmpNo) >= 2;

SELECT D.DeptName
FROM Department D
WHERE D.DeptNo IN (
	SELECT E.DeptNo
	FROM Employee E
	GROUP BY E.DeptNo
	HAVING COUNT(*) >=2
);

SELECT
    D.DeptName,
    STRING_AGG(E.EmpName, ',') AS Employees
FROM Department D
	JOIN Employee E ON D.DeptNo = E.DeptNo
GROUP BY D.DeptName
HAVING COUNT(E.EmpNo) >= 2;

;WITH DEPARTMORETHAN2EMPS AS (
	SELECT D.DeptName, D.DeptNo
	FROM Department D
	WHERE D.DeptNo IN (
		SELECT E.DeptNo
		FROM Employee E
		GROUP BY E.DeptNo
		HAVING COUNT(*) >=2
	)
)
SELECT
    D.DeptName,
    STRING_AGG(E.EmpName, ',') AS Employees
FROM DEPARTMORETHAN2EMPS D
	JOIN Employee E ON D.DeptNo = E.DeptNo
GROUP BY D.DeptName
HAVING COUNT(E.EmpNo) >= 2;
-------------------- 6.	List all name, email and skill number of the employees and sort ascending order by employee’s name. --------------------

SELECT
    E.EmpName,
    E.Email,
    COUNT(ES.SkillNo) AS NumberOfSkills
FROM Employee E
	LEFT JOIN Emp_Skill ES ON E.EmpNo = ES.EmpNo
GROUP BY E.EmpName, E.Email
ORDER BY E.EmpName ASC;

-------------------- 7.	Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills. --------------------

SELECT DISTINCT E.EmpName, E.Email, E.BirthDay
FROM Employee E
WHERE E.Status = 1
AND E.EmpNo IN (
    SELECT ES.EmpNo
    FROM Emp_Skill ES
    GROUP BY ES.EmpNo
    HAVING COUNT(ES.SkillNo) > 1
);


-------------------- 8.	Create a view to list all employees are working (include: name of employee and skill name, department name) --------------------

CREATE VIEW VWorkingEmployeesWithSkills AS
SELECT E.EmpName AS EmployeeName, S.SkillName, D.DeptName AS DepartmentName
FROM Employee E
	JOIN Emp_Skill ES ON E.EmpNo = ES.EmpNo
	JOIN Skill S ON ES.SkillNo = S.SkillNo
	JOIN Department D ON E.DeptNo = D.DeptNo
WHERE E.Status = 1;

CREATE VIEW VWorkingEmployeesWithSkills AS
SELECT E.EmpName AS EmployeeName, S.SkillName, D.DeptName AS DepartmentName
FROM (SELECT E.EmpName, E.EmpNo, E.DeptNo FROM Employee E WHERE E.Status = 1) E
	JOIN Emp_Skill ES ON E.EmpNo = ES.EmpNo
	JOIN Skill S ON ES.SkillNo = S.SkillNo
	JOIN Department D ON E.DeptNo = D.DeptNo
;
GO

SELECT * FROM VWorkingEmployeesWithSkills;
GO

