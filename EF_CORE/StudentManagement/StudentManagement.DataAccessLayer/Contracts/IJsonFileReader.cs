﻿namespace StudentManagement.DataAccessLayer.Contracts
{
    internal interface IJsonFileReader<T>
    {
        Task<List<T>> ReadArrayJsonAsync(string fileName);
        Task WriteArrayJsonAsync(string fileName, List<T> items);
    }
}