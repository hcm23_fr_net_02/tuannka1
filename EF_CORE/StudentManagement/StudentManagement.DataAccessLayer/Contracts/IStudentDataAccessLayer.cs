﻿using StudentManagement.Entities;

namespace StudentManagement.DataAccessLayer.Contracts
{
    public interface IStudentDataAccessLayer
    {
        Task<int> AddStudentAsync(Student student);
        Task<List<Student>> GetStudentsPagingAsync();
        Task<List<Student>> GetStudentsByConditionAsync(Func<Student, bool> predicate);
        Task<bool> DeleteStudentAsync(int studentId);
    }
}