﻿using StudentManagement.DataAccessLayer.Contracts;
using StudentManagement.Entities;
using StudentManagement.Entities.Exceptions;

namespace StudentManagement.DataAccessLayer
{

    public class StudentDataAccessLayer : IStudentDataAccessLayer
    {
        #region Private fields
        private readonly IJsonFileReader<Student> _jsonFileReader;
        private const string _jsonFileName = "Data/students.json";
        #endregion

        #region Constructor
        public StudentDataAccessLayer()
        {
            _jsonFileReader = new JsonFileReader<Student>();
        }
        #endregion

        #region Public Methods
        public async Task<int> AddStudentAsync(Student student)
        {
            try
            {
                List<Student> studentsList = await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);

                studentsList.Add(student);

                await _jsonFileReader.WriteArrayJsonAsync(_jsonFileName, studentsList);
                return student.Id;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<Student>> GetStudentsPagingAsync()
        {
            try
            {
                return await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<Student>> GetStudentsByConditionAsync(Func<Student, bool> predicate)
        {
            try
            {
                List<Student> studentsList = await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);
                return studentsList.Where(predicate).ToList();
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> DeleteStudentAsync(int studentId)
        {
            try
            {
                List<Student> studentsList = await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);

                if (studentsList.RemoveAll(item => item.Id == studentId) == 0)
                    return false;

                await _jsonFileReader.WriteArrayJsonAsync(_jsonFileName, studentsList);
                return true;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}