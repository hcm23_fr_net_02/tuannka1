﻿using StudentManagement.DataAccessLayer.Contracts;
using System.Text.Json;

namespace StudentManagement.DataAccessLayer
{

    internal class JsonFileReader<T> : IJsonFileReader<T>
    {
        public async Task<List<T>> ReadArrayJsonAsync(string fileName)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            string content;
            List<T> items;

            try
            {
                content = await File.ReadAllTextAsync(fileName);
                items = JsonSerializer.Deserialize<List<T>>(content, options) ?? new List<T>();
            }
            catch (Exception ex)
            {
                throw new Exception("Xảy ra lỗi khi đọc file.", ex);
            }

            return items;
        }

        public async Task WriteArrayJsonAsync(string fileName, List<T> items)
        {
            try
            {
                string json = JsonSerializer.Serialize(items);
                await File.WriteAllTextAsync(fileName, json);
            }
            catch (Exception ex)
            {
                throw new Exception("Xảy ra lỗi khi ghi file.", ex);
            }
        }
    }
}
