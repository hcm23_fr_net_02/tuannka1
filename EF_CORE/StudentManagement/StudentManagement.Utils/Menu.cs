﻿namespace StudentManagement.Utils
{
    /// <summary>
    /// Utils static class for create menu automation
    /// </summary>
    public static class Menu
    {
        /// <summary>
        /// Create menu method with menuName and options
        /// </summary>
        /// <param name="menuName">Name of Menu</param>
        /// <param name="options">Name and action respectively for each choice in menu</param>
        /// <returns></returns>
        public static async Task CreateMenuAsync(string menuName, Dictionary<string, Func<Task>> options)
        {
            //declare menuChoice
            var menuChoice = -1;

            do
            {
                //star display for format menu name more beautiful
                var starDisplay = new string('*', 9);

                //display menu name
                Console.WriteLine($"\n{starDisplay} {menuName} {starDisplay}");

                //display menu choice
                ///note: with each option, we create index respectively with orther in dictionary
                ///     choice = index + 1 (bc index start from 0)
                var optionsWithIndex = options.Select((option, index) => (option, index));
                foreach (var (option, index) in optionsWithIndex)
                    Console.WriteLine($"{index + 1}. {option.Key}");

                // Option for exit
                Console.WriteLine("0. Thoát");

                //require enter again until user enter correctly choice
                do
                {
                    Console.Write("Lựa chọn của bạn: ");
                } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                var choiceOption = optionsWithIndex.FirstOrDefault(opt => opt.index + 1 == menuChoice).option;
                Func<Task>? choiceAction = choiceOption.Value;

                //if user's choice not in range of menu choice => no thing occur
                if (choiceAction is not null)
                    await choiceAction.Invoke();

                //Console.Clear();
            } while (menuChoice != 0);
        }
    }
}