﻿using StudentManagement.ConsoleApplication;
using StudentManagement.Utils;

//configure Unicode encoding for Vietnames Languages
Console.OutputEncoding = System.Text.Encoding.UTF8;

var applicationName = "Ứng dụng quản lý thông tin sinh viên";

Console.WriteLine($":::{applicationName}:::");

//show main menu
var mainMenuName = "Menu chính";

var mainNameOptions = new Dictionary<string, Func<Task>>
{
    { "Thêm sinh viên", StudentPresentation.AddStudentAsync },
    { "Xem danh sách sinh viên", StudentPresentation.ViewStudentsAsync },
    { "Tìm sinh viên", SearchAsync },
    { "Xóa sinh viên", StudentPresentation.DeleteStudentAsync }
};

await Menu.CreateMenuAsync(mainMenuName, mainNameOptions);

//Exit 
Console.WriteLine("\nHẹn gặp lại");

async Task SearchAsync()
{
    var menuName = "Tìm sinh viên";
    var nameOptions = new Dictionary<string, Func<Task>>
    {
        { "Tìm kiếm theo mã số sinh viên", StudentPresentation.SearchStudentIdAsync },
        { "Tìm kiếm theo tên", StudentPresentation.SearchStudentFullNameAsync },
    };

    await Menu.CreateMenuAsync(menuName, nameOptions);
}