﻿using StudentManagement.DataAccessLayer;
using StudentManagement.DataAccessLayer.Contracts;
using StudentManagement.Entities;

namespace StudentManagement.ConsoleApplication
{
    internal static class StudentPresentation
    {
        internal static async Task AddStudentAsync()
        {
            try
            {
                Student student = new Student();

                Console.WriteLine("\n:::Thêm sinh viên:::");

                //user enter fullname
                Console.Write("Mời nhập họ và tên: ");
                student.FullName = Console.ReadLine();

                //user enter age
                int age;
                Console.Write("Mời nhập tuổi: ");
                while (!int.TryParse(Console.ReadLine(), out age))
                {
                    Console.Write("Dữ liệu không hợp lệ, mời nhập lại: ");
                }
                student.Age = age;

                //user enter gpa
                float gpa;
                Console.Write("Mời nhập GPA: ");
                while (!float.TryParse(Console.ReadLine(), out gpa))
                {
                    Console.Write("Dữ liệu không hợp lệ, mời nhập lại: ");
                }
                student.GPA = gpa;

                //generate student Id
                student.Id = Student.GenerateId();

                IStudentDataAccessLayer studentsDataAccessLayer = new StudentDataAccessLayer();

                int id = await studentsDataAccessLayer.AddStudentAsync(student);

                List<Student> newStudent = await studentsDataAccessLayer
                                                    .GetStudentsByConditionAsync(item => item.Id == id);

                if (newStudent.Count == 1)
                {
                    Console.WriteLine("Sinh viên mới có ID: " + newStudent.Single().Id);
                }
                else
                {
                    Console.WriteLine("Thêm mới sinh viên thất bại.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        internal static async Task ViewStudentsAsync()
        {
            try
            {
                IStudentDataAccessLayer studentsDataAccessLayer = new StudentDataAccessLayer();

                List<Student> allStudents = await studentsDataAccessLayer.GetStudentsPagingAsync();
                if (allStudents.Count == 0)
                {
                    Console.WriteLine("Không có sinh viên\n");
                    return;
                }

                //read all students
                Console.WriteLine("\n***Danh sách sinh viên***");
                foreach (var student in allStudents)
                {
                    Console.WriteLine(student);

                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        internal static async Task SearchStudentIdAsync()
        {
            try
            {
                IStudentDataAccessLayer studentsDataAccessLayer = new StudentDataAccessLayer();

                Console.WriteLine("\n:::Tìm sinh viên:::");

                //user enter id need to search
                int studentIdToSeach;
                Console.Write("Nhập mã số sinh viên của sinh viên cần tìm: ");
                while (!int.TryParse(Console.ReadLine(), out studentIdToSeach))
                {
                    Console.Write("Dữ liệu không hợp lệ, mời nhập lại: ");
                }

                var existingStudent = (await studentsDataAccessLayer
                                        .GetStudentsByConditionAsync(temp => temp.Id == studentIdToSeach))
                                        .FirstOrDefault();

                if (existingStudent == null)
                {
                    Console.WriteLine("Mã số sinh viên không hợp lệ.\n");
                    return;
                }

                Console.WriteLine(existingStudent);

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        internal static async Task SearchStudentFullNameAsync()
        {
            try
            {
                IStudentDataAccessLayer studentsDataAccessLayer = new StudentDataAccessLayer();

                Console.WriteLine("\n:::Tìm sinh viên:::");

                //user enter id need to search
                string studentName;

                studentName = Console.ReadLine();
             
                var existingStudent = (await studentsDataAccessLayer
                                        .GetStudentsByConditionAsync(temp => temp.FullName.ToLower().Contains(studentName.ToLower())))
                                        .FirstOrDefault();

                if (existingStudent == null)
                {
                    Console.WriteLine("Tên không hợp lệ.\n");
                    return;
                }

                Console.WriteLine(existingStudent);

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }


        internal static async Task DeleteStudentAsync()
        {
            try
            {
                IStudentDataAccessLayer studentsDataAccessLayer = new StudentDataAccessLayer();

                //user enter id need to deleted
                int studentIdToDelete;
                Console.Write("Nhập mã số sinh viên của sinh viên cần xóa: ");
                while (!int.TryParse(Console.ReadLine(), out studentIdToDelete))
                {
                    Console.Write("Dữ liệu không hợp lệ, mời nhập lại: ");
                }

                var existingStudent = (await studentsDataAccessLayer
                                        .GetStudentsByConditionAsync(s => s.Id == studentIdToDelete))
                                        .FirstOrDefault();

                if (existingStudent == null)
                {
                    Console.WriteLine($"Không có sinh viên tồn tại với Id: {studentIdToDelete}.\n");
                    return;
                }

                bool isDeleted = await studentsDataAccessLayer.DeleteStudentAsync(existingStudent.Id);

                if (isDeleted)
                {
                    Console.WriteLine("Đã xóa thành công.\n");
                }
                else
                {
                    Console.WriteLine("Xóa sinh viên thất bại.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }
    }
}
