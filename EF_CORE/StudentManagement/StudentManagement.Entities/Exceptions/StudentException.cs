﻿namespace StudentManagement.Entities.Exceptions
{
    /// <summary>
    /// represents error of Student entity
    /// </summary>
    public class StudentException : Exception
    {
        public StudentException(string message) : base(message) { }
    }
}