﻿using StudentManagement.Entities.Exceptions;
using System.Text;

namespace StudentManagement.Entities
{
    public class Student
    {
        #region Private fields
        private int _id;
        private string _fullName;
        private int _age;
        private float _gPA;
        private static int _baseId = 10000;
        #endregion

        #region Public Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string FullName
        {
            get => _fullName;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new StudentException("Họ và tên không thể có giá trị null hoặc khoảng trắng.");
                _fullName = value;
            }
        }

        public int Age
        {
            get => _age;
            set
            {
                if (value < 0)
                    throw new StudentException("Tuổi không thể là số âm.");
                _age = value;
            }
        }

        public float GPA
        {
            get => _gPA;
            set
            {
                if (value < 0 || value > 10)
                    throw new StudentException("GPA chỉ có thể nằm trong khoảng 0 đến 10.");
                _gPA = value;
            }
        }
        #endregion

        #region Public Methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append($"Mã số sinh viên: {Id}\n");
            stringBuilder.Append($"Họ và tên: {FullName}\n");
            stringBuilder.Append($"Tuổi: {Age}\n");
            stringBuilder.Append($"GPA: {GPA} \n\n");

            return stringBuilder.ToString();
        }

        public static int GenerateId()
        {
            return _baseId++;
        }
        #endregion
    }
}