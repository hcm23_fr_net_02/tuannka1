﻿using Learn_EF_Model.Models;
using Microsoft.EntityFrameworkCore;

namespace Learn_EF_DataAccess.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Genre> Genres { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer("Server=(LocalDb)\\MSSQLLocalDB;Database=Learn_EF;TrustServerCertificate=True;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Book>()
                .HasData(
                    new Book
                    {
                        Id = 1,
                        Title = "Harry Potter",
                        ISBN = "001",
                        Price = 100
                    }
                );
        }

    }
}