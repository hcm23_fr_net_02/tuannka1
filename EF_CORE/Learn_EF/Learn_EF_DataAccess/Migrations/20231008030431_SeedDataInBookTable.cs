﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Learn_EF_DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class SeedDataInBookTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "ISBN", "Price", "Title" },
                values: new object[] { 1, "001", 100m, "Harry Potter" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
