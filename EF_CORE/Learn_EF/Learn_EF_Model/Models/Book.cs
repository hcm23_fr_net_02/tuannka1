﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Learn_EF_Model.Models
{
    public class Book
    {
        private float _price;
        public int Id { get; set; }
        [MaxLength(100)]
        public string? Title { get; set; }
        [MaxLength(20)]
        public string? ISBN { get; set; }
        public float Price
        {
            get => _price;
            set
            {
                _price = value;
            }
        } 
        [NotMapped]
        public int PriceMax { get; set; }
    }
}
