﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    public class DateTimeTests
    {
        [Fact]
        public void CreateDatetime_ShouldCreated_WhenStringCorrectFormat()
        {
            //Arrange
            var param = "2025-12-31 23:59:59.999 PM";
            var expectedHour = 23; // correct, default remove PM if not necessary

            //Action
            var actual = DateTime.Parse(param);

            //Assert
            Assert.Equal(expectedHour, actual.Hour);
        }

        [Theory]
        [InlineData("2025-12-31 23:59:59.999 AM")]
        [InlineData("2025-12-31 24:59:59.999")]
        [InlineData("2025-12-32 21:59:59.999")]
        public void CreateDatetime_ShouldThrowException_WhenParamIsWrongFormat(string param)
        {
            //Action & Assert
            Assert.Throws<FormatException>(() => DateTime.Parse(param));
        }

        [Fact]
        public void DateOfWeek_ShouldReturnWednesday_WhenParamIs20230410()
        {
            //Arrange
            var param = DateTime.Parse("2023-10-04");
            var expected = DayOfWeek.Wednesday;

            //Action
            var actual = param.DayOfWeek;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddDay_ShouldReturnAddedDay_WhenParamIsDay()
        {
            //Arrange
            var param = DateTime.Parse("2023-10-04");
            var expected = DateTime.Parse("2023-10-01");

            //Action
            var actual = param.AddDays(-3);

            //Assert
            Assert.Equal(expected, actual); // DateTime is struct => value type => Equal compare value in variable
        }

        [Fact]
        public void ConvertTimeToUtc_ShouldReturnUTCTime_WhenParamIsNow()
        {
            //Arrange
            var now = DateTime.Now;
            var expected = now.ToUniversalTime();

            //Action
            var actual = TimeZoneInfo.ConvertTimeToUtc(now);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("China Standard Time", (+8)-(+7))]
        [InlineData("Pacific Standard Time", (-8)-(+7) + 1)]
        public void ConvertTimeTo_ShouldReturnTimeZoneCorrect_WhenParamIsTimeZoneAndOffset(string timezone, int offset)
        {
            //Arrange
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timezone);

            var now = DateTime.Now;
            var expected = now.AddHours(offset);

            //Action
            var actual = TimeZoneInfo.ConvertTime(now, timeZone);

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
