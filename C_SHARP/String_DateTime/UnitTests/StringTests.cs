namespace UnitTests
{
    public class paramingTests
    {
        [Fact]
        public void ToUpper_ShouldReturnNAME_WhenParamIsname()
        {
            //Arrange
            var param = "name";
            var expected = "NAME";

            //Action
            var actual = param.ToUpper();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ToLower_ShouldReturnname_WhenParamIsNAme()
        {
            //Arrange
            var param = "NAme";
            var expected = "name";

            //Action
            var actual = param.ToLower();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Substring_ShouldReturnSubstring_WhenParamIsString()
        {
            //Arrange
            var param = "Hello Word!";
            var startIndex = 6;
            var lenth = 5;
            var expected = "Word!";

            //Action
            var actual = param.Substring(startIndex, lenth);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Substring_ShouldThrowException_WhenLengthParamIsOver()
        {
            //Arrange
            var param = "Hello Word!";
            var startIndex = 6;
            var lenth = 6;

            //Action & Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => param.Substring(startIndex, lenth));
        }

        [Fact]
        public void Split_ShoudReturnArray_WhenParamIsString()
        {
            //Arrange
            var param = "Hello World!";
            var expectedCount = 2;
            var expectedLastElement = "World!";

            //Action
            var result = param.Split(" ");

            //Assert
            Assert.Equal(expectedCount, result.Count());
            Assert.Equal(expectedLastElement, result.Last());
        }
    }
}