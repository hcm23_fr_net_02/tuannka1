﻿using MathOperationLibrary;

namespace MathOperation
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Calculator calculator = new Calculator();

            while (true)
            {

                IOperation<int> intOperation = new IntOperation();
                //IOperation<double> doubleOperation = new DoubleOperation();

                Console.WriteLine("Select an operation:");
                Console.WriteLine("1. Addition");
                Console.WriteLine("2. Subtraction");
                Console.WriteLine("3. Multiplication");
                Console.WriteLine("4. Division");
                Console.WriteLine("5. Exit");

                Console.Write("Your choice: ");
                int.TryParse(Console.ReadLine(), out int choice);

                if (choice == 5)
                    break;

                Console.Write("Enter the first number: ");
                int.TryParse(Console.ReadLine(), out int num1);

                Console.Write("Enter the second number: ");
                int.TryParse(Console.ReadLine(), out int num2);

                int result = 0;

                switch (choice)
                {
                    case 1:
                        result = calculator.PerformOperation(num1, num2, intOperation.Add);
                        break;
                    case 2:
                        result = calculator.PerformOperation(num1, num2, intOperation.Subtract);
                        break;
                    case 3:
                        result = calculator.PerformOperation(num1, num2, intOperation.Multiply);
                        break;
                    case 4:
                        result = calculator.PerformOperation(num1, num2, intOperation.Divide);
                        break;
                    default:
                        Console.WriteLine("Invalid choice.");
                        break;
                }

                Console.WriteLine($"Result: {result}\n");
            }
        }
    }
}