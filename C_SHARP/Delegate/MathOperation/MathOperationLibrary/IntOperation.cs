﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathOperationLibrary
{
    public class IntOperation : IOperation<int>
    {
        public int Add(int a, int b) => a + b;

        public int Divide(int a, int b) => b != 0 ? a / b : throw new InvalidOperationException();

        public int Multiply(int a, int b) => a * b; 

        public int Subtract(int a, int b) => a - b;
    }
}
