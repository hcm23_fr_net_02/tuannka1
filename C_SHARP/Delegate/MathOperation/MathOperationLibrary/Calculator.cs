﻿namespace MathOperationLibrary
{
    public class Calculator
    {
        public T PerformOperation<T>(T param1, T param2, Func<T, T, T> operation)
        {
            return operation(param1, param2);
        }
    }
}