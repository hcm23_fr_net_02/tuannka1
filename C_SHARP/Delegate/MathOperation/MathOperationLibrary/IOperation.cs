﻿namespace MathOperationLibrary
{
    // Define an interface for operations
    public interface IOperation<T>
    {
        T Add(T a, T b);
        T Subtract(T a, T b);
        T Multiply(T a, T b);
        T Divide(T a, T b);
    }
}
