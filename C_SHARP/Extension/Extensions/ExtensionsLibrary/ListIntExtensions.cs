﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionsLibrary
{
    public static class ListIntExtensions
    {
        public static double GetAverage(this List<int> list)
        {
            if (list == null) throw new ArgumentNullException();
            if (list.Count == 0) return 0;
            return list.Sum(x => x) / list.Count;
        }
    }
}
