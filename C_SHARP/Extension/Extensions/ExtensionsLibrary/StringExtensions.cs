﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionsLibrary
{
    public static class StringExtensions
    {
        public static string CustomeUpper(this string str, int point = 0)
        {
            if (string.IsNullOrEmpty(str))
                throw new ArgumentException();

            if (point < 0 || point >= str.Length)
                throw new ArgumentOutOfRangeException();

            char[] arrayOfChar = str.ToCharArray();
            arrayOfChar[point] = char.ToUpper(arrayOfChar[point]);
            return new string(arrayOfChar);
        }

        public static string Reverse(this string str)
        {
            if (str is null)
                throw new NullReferenceException();

            char[] arrayOfChar = str.ToCharArray();
            Array.Reverse(arrayOfChar);
            return new string(arrayOfChar);
        }
        public static string RemoveDuplicates(this string str)
        {
            if (str is null)
                throw new NullReferenceException();

            var hashSet = new HashSet<char>();

            foreach (char c in str)
            {
                hashSet.Add(c);
            }

            return hashSet.Aggregate("", (acc, c) => acc + c);
        }
    }
}
