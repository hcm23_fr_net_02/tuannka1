﻿using ExtensionsLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensions.UnitTests
{
    public class ListIntExtensionsTests
    {
        [Fact]
        public void GetAverage_ShouldThrowException_WhenListIsNull()
        {
            //Arrange
            List<int> input = null;

            //Action & Assert
            Assert.Throws<ArgumentNullException>(() => input.GetAverage());
        }

        [Fact]
        public void GetAverage_ShouldReturn0_WhenListIsEmpty()
        {
            //Arrange
            var input = new List<int>();
            var expected = 0D;

            //Action
            var actual = input.GetAverage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetAverage_ShouldReturn2_WhenListIs123()
        {
            //Arrange
            var input = new List<int> { 1, 2, 3 };
            var expected = 2D;

            //Action
            var actual = input.GetAverage();

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
