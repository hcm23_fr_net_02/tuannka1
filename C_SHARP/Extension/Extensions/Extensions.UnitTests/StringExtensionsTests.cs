﻿using ExtensionsLibrary;

namespace Extensions.UnitTests
{
    public class StringExtensionsTests
    {
        [Theory]
        [InlineData(5)]
        [InlineData(-1)]
        public void CustomUpper_ShouldThrowOutOfRangeException_WhenPointOutOfRange(int point)
        {
            //Arrange
            var str = "hello";

            //Action & Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => { str.CustomeUpper(point); });
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void CustomUpper_ShouldThrowException_WhenStrIsEmptyOrNull(string input)
        {
            //Action & Assert
            Assert.Throws<ArgumentException>(() => { input.CustomeUpper(); });
        }

        [Fact]
        public void CustomUpper_ShouldReturnHello_WhenPointIsDefault()
        {
            //Arrange
            var str = "hello";
            var expected = "Hello";

            //Action
            var actual = str.CustomeUpper();

            //Action & Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CustomUpper_ShouldReturnheLlo_WhenPointIs2()
        {
            //Arrange
            var str = "hello";
            var expected = "heLlo";
            var point = 2;

            //Action
            var actual = str.CustomeUpper(point);

            //Action & Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Reverse_ShouldThrowException_WhenStrIsNull()
        {
            //Arrange
            string input = null;

            /// Note: Extension method, can be called by null object

            //Action & Assert
            Assert.Throws<NullReferenceException>(() => { input.Reverse(); });
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("Hello, world!", "!dlrow ,olleH")]
        public void Reverse_ShouldReturnReversedString_WhenStrIsCorrectFormat(string input, string expected)
        {
            //Action
            string actual = input.Reverse();

            //Action & Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveDuplicates_ShouldThrowException_WhenStrIsNull()
        {
            //Arrange
            string input = null;

            /// Note: Extension method, can be called by null object

            //Action & Assert
            Assert.Throws<NullReferenceException>(() => { input.RemoveDuplicates(); });
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("Hello World", "Helo Wrd")]
        public void RemoveDuplicates_ShouldReturnRemoveDuplicatesString_WhenStrIsCorrectFormat(string input, string expected)
        {
            //Action
            string actual = input.RemoveDuplicates();

            //Action & Assert
            Assert.Equal(expected, actual);
        }
    }
}
