using ExtensionsLibrary;

namespace Extensions.UnitTests
{
    public class IntExtensionsTests
    {
        [Fact]
        public void Square_Return4WhenParamIs2()
        {
            //Arrange
            var param = 2;
            var expected = 4;

            //Action
            var result = param.Square();

            //Assert
            Assert.Equal(expected, result);
        }
    }
}