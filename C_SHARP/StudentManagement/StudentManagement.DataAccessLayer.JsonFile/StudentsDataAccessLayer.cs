﻿using StudentManagement.DataAccessLayer.Contracts;
using StudentManagement.DataAccessLayer.JsonFile.Contracts;
using StudentManagement.Shared.Entities;
using StudentManagement.Shared.Exceptions;

namespace StudentManagement.DataAccessLayer.JsonFile
{
    /// <summary>
    /// Represents Data Access Layer for Student
    /// </summary>
    public class StudentsDataAccessLayer : IStudentsDataAccessLayer
    {
        #region Private fields
        private readonly IJsonFileReader<Student> _jsonFileReader;
        private const string _jsonFileName = "Data/students.json";
        #endregion

        #region Constructor
        /// <summary>
        /// Contructor that initializes Json File Reader
        /// </summary>
        public StudentsDataAccessLayer()
        {
            _jsonFileReader = new JsonFileReader<Student>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a new Student to the existing Students list
        /// </summary>
        /// <param name="student">The Student object to add</param>
        /// <returns>Returns StudentID of Student object just created</returns>
        public async Task<Guid> AddStudentAsync(Student student)
        {
            try
            {
                List<Student> studentsList = await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);

                studentsList.Add(student);

                await _jsonFileReader.WriteArrayJsonAsync(_jsonFileName, studentsList);
                return student.Id;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes an existing Student
        /// </summary>
        /// <param name="studentId">StudentId to delete</param>
        /// <returns>Returns true, that indicates the Student is deleted successfully; otherwise, returns false</returns>
        public async Task<bool> DeleteStudentAsync(Guid studentId)
        {
            try
            {
                List<Student> studentsList = await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);

                if (studentsList.RemoveAll(item => item.Id == studentId) == 0)
                    return false;

                await _jsonFileReader.WriteArrayJsonAsync(_jsonFileName, studentsList);
                return true;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns all existing Students
        /// </summary>
        /// <returns>The list of all existing Students</returns>
        public async Task<List<Student>> GetStudentsAsync()
        {
            try
            {
                return await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves a paginated list of students asynchronously.
        /// </summary>
        /// <param name="pageNumber">The page number to retrieve.</param>
        /// <param name="numberPerPage">The number of students per page.</param>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains a list of students for the specified page.
        /// </returns>
        public async Task<List<Student>> GetStudentsPagingAsync(int pageNumber = 0, int numberPerPage = 5)
        {
            try
            {
                return (await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName))
                              .Skip(pageNumber * numberPerPage)
                              .Take(numberPerPage)
                              .ToList();
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns a set of Students that matches with specified criteria
        /// </summary>
        /// <param name="predicate">Lamdba expression that contains condition to check</param>
        /// <returns>The list of matching Students</returns>
        public async Task<List<Student>> GetStudentsByConditionAsync(Func<Student, bool> predicate)
        {
            try
            {
                List<Student> studentsList = await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);
                return studentsList.Where(predicate).ToList();
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates an existing Student
        /// </summary>
        /// <param name="student">Student object that contains Student details to update</param>
        /// <returns>Returns true, that indicates the Student is updated successfully; otherwise, returns false</returns>
        public async Task<bool> UpdateStudentAsync(Student student)
        {
            try
            {
                List<Student> studentsList = await _jsonFileReader.ReadArrayJsonAsync(_jsonFileName);

                var existingStudent = studentsList.Find(s => s.Code == student.Code);

                if (existingStudent is null)
                    return false;

                existingStudent.Name = student.Name;
                existingStudent.BirthDate = student.BirthDate;
                existingStudent.Gender = student.Gender;
                existingStudent.PhoneNumber = student.PhoneNumber;

                await _jsonFileReader.WriteArrayJsonAsync(_jsonFileName, studentsList);
                return true;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
