﻿namespace StudentManagement.DataAccessLayer.JsonFile.Contracts
{
    /// <summary>
    /// Represents an interface for reading and writing JSON files.
    /// </summary>
    /// <typeparam name="T">The type of objects to read and write.</typeparam>
    internal interface IJsonFileReader<T>
    {
        /// <summary>
        /// Reads a single object from a JSON file.
        /// </summary>
        /// <param name="fileName">The name of the JSON file to read from.</param>
        /// <returns>The deserialized object from the JSON file.</returns>
        /// <exception cref="Exception">Thrown when an error occurs while reading the file.</exception>
        /// <exception cref="NullReferenceException">Thrown when the deserialized object is null.</exception>
        Task<T> ReadObjectJsonAsync(string fileName);

        /// <summary>
        /// Reads a list of objects from a JSON file.
        /// </summary>
        /// <param name="fileName">The name of the JSON file to read from.</param>
        /// <returns>A list of deserialized objects from the JSON file.</returns>
        /// <exception cref="Exception">Thrown when an error occurs while reading the file.</exception>
        Task<List<T>> ReadArrayJsonAsync(string fileName);

        /// <summary>
        /// Writes a list of objects to a JSON file.
        /// </summary>
        /// <param name="fileName">The name of the JSON file to write to.</param>
        /// <param name="items">The list of objects to serialize and write to the JSON file.</param>
        /// <exception cref="Exception">Thrown when an error occurs while writing to the file.</exception>
        Task WriteArrayJsonAsync(string fileName, List<T> items);
    }
}
