﻿using StudentManagement.DataAccessLayer.JsonFile.Contracts;
using System.Text.Json;

namespace StudentManagement.DataAccessLayer.JsonFile
{

    /// <summary>
    /// Provides functionality to read and write JSON files.
    /// </summary>
    /// <typeparam name="T">The type of objects to read and write.</typeparam>
    internal class JsonFileReader<T> : IJsonFileReader<T>
    {
        /// <summary>
        /// Reads a single object from a JSON file.
        /// </summary>
        /// <param name="fileName">The name of the JSON file to read from.</param>
        /// <returns>The deserialized object from the JSON file.</returns>
        /// <exception cref="Exception">Thrown when an error occurs while reading the file.</exception>
        /// <exception cref="NullReferenceException">Thrown when the deserialized object is null.</exception>
        public async Task<T> ReadObjectJsonAsync(string fileName)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            string content;
            T? item;
            try
            {
                content = await File.ReadAllTextAsync(fileName);
                item = JsonSerializer.Deserialize<T>(content, options);
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while reading the file.", ex);
            }

            if (item is null)
                throw new NullReferenceException("The deserialized object is null.");

            return item;
        }

        /// <summary>
        /// Reads a list of objects from a JSON file.
        /// </summary>
        /// <param name="fileName">The name of the JSON file to read from.</param>
        /// <returns>A list of deserialized objects from the JSON file.</returns>
        /// <exception cref="Exception">Thrown when an error occurs while reading the file.</exception>
        public async Task<List<T>> ReadArrayJsonAsync(string fileName)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            string content;
            List<T> items;

            try
            {
                content = await File.ReadAllTextAsync(fileName);
                items = JsonSerializer.Deserialize<List<T>>(content, options) ?? new List<T>();
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while reading the file.", ex);
            }

            return items;
        }

        /// <summary>
        /// Writes a list of objects to a JSON file.
        /// </summary>
        /// <param name="fileName">The name of the JSON file to write to.</param>
        /// <param name="items">The list of objects to serialize and write to the JSON file.</param>
        /// <exception cref="Exception">Thrown when an error occurs while writing to the file.</exception>
        public async Task WriteArrayJsonAsync(string fileName, List<T> items)
        {
            try
            {
                string json = JsonSerializer.Serialize(items);
                await File.WriteAllTextAsync(fileName, json);
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while writing to the file.", ex);
            }
        }
    }
}
