﻿using StudentManagement.Shared.Exceptions;
using System.Text;

namespace StudentManagement.Shared.Entities
{
    public class ScoreStudent
    {
        #region Private fields
        private Guid _id;
        private Guid _studentId;
        private Guid _subjectId;
        private float _score;
        #endregion

        #region Public properties
        public Guid Id
        {
            get => _id;
            set => _id = value;
        }

        public Guid SubjectId
        {
            get => _subjectId;
            set => _subjectId = value;
        }

        public Guid StudentId
        {
            get => _studentId;
            set => _studentId = value;
        }

        public float Score
        {
            get => _score;
            set
            {
                if (value > 10 || value < 0)
                    throw new ScoreStudentException("Score should be in range from 0 to 10");
                _score = value;
            }
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"Student Id: {Id}")
                .AppendLine($"Subject Id: {SubjectId}")
                .AppendLine($"Score: {Score}");
            return stringBuilder.ToString();
        }
        #endregion
    }
}
