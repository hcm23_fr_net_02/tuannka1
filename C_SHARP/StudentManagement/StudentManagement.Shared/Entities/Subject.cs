﻿using StudentManagement.Shared.Entities.Contracts;
using StudentManagement.Shared.Exceptions;
using System.Text;

namespace StudentManagement.Shared.Entities
{
    /// <summary>
    /// Represents information of Subject
    /// </summary>
    public class Subject : ISubject
    {
        #region Private fields
        private Guid _id;
        private string _name;
        #endregion

        #region Public properties
        public Guid Id
        {
            get => _id;
            set => _id = value;
        }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > 60)
                    throw new SubjectException("Subject name should not be null, empty or greater than 60 characters long.");
                _name = value;
            }
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"Subject Id: {Id}")
                .AppendLine($"Subject Name: {Name}");

            return stringBuilder.ToString();
        }
        #endregion
    }
}
