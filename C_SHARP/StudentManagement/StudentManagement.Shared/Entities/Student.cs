﻿using StudentManagement.Shared.Entities.Contracts;
using StudentManagement.Shared.Exceptions;
using System.Text;

namespace StudentManagement.Shared.Entities
{
    /// <summary>
    /// Represents information of Student
    /// </summary>
    public class Student : IStudent
    {
        #region Private fields
        private Guid _id;
        private long _code;
        private string _name;
        private DateTime _birthDate;
        private bool _gender;
        private string _phoneNumber;
        private List<ScoreStudent> _scores;
        #endregion

        #region Public properties
        public Guid Id
        {
            get => _id;
            set => _id = value;
        }
        public long Code
        {
            get => _code;
            set
            {
                if (value <= 0)
                    throw new StudentException("Student code should be positive.");
                _code = value;
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > 60)
                    throw new StudentException("Student name should not be null, empty or greater than 60 characters long.");
                _name = value;
            }
        }

        public DateTime BirthDate
        {
            get => _birthDate;
            set
            {
                if (value.CompareTo(DateTime.Now) >= 0)
                    throw new StudentException("Birthdate should be earlier than today.");
                _birthDate = value;
            }
        }

        public bool Gender
        {
            get => _gender;
            set => _gender = value;
        }

        public string PhoneNumber
        {
            get => _phoneNumber;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new StudentException("Phone number should not be null, empty or whitespace");
                _phoneNumber = value;
            }
        }

        public List<ScoreStudent>? Scores
        {
            get => _scores;
            set => _scores = value;
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tStudent Id: {Id}")
                .AppendLine($"\tStudent Code: {Code}")
                .AppendLine($"\tStudent Name: {Name}")
                .AppendLine($"\tBirthdate: {BirthDate.ToShortDateString()}")
                .AppendLine($"\tGender: {(Gender ? "male" : "female")}")
                .AppendLine($"\tPhone number: {PhoneNumber}");

            return stringBuilder.ToString();
        }
        #endregion
    }

}
