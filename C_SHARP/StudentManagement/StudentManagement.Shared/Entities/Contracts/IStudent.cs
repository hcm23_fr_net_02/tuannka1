﻿namespace StudentManagement.Shared.Entities.Contracts
{
    /// <summary>
    /// Represents the interface of Student class
    /// </summary>
    public class IStudent
    {
        #region Properties
        Guid Id { get; set; }
        long Code { get; set; }
        string Name { get; set; }
        DateTime BirthDate { get; set; }
        bool Gender { get; set; }
        string PhoneNumber { get; set; }
        #endregion
    }
}
