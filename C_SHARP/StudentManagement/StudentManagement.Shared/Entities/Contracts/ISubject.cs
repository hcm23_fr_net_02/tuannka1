﻿namespace StudentManagement.Shared.Entities.Contracts
{
    /// <summary>
    /// Represents the interface of Subject class
    /// </summary>
    public class ISubject
    {
        #region Properties
        Guid Id { get; set; }
        string Name { get; set; }
        #endregion
    }
}
