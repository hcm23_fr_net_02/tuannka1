﻿namespace StudentManagement.Shared.Configuration
{
    public static class Settings
    {
        /// <summary>
        /// Student code starts from 1000001; incremented by 1
        /// </summary>
        public static long BaseStudentCode { get; set; } = 1000000;
    }
}
