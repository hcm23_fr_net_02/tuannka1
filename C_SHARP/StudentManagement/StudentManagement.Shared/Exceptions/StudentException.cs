﻿namespace StudentManagement.Shared.Exceptions
{
    /// <summary>
    /// Represents error raised in Student class
    /// </summary>
    public class StudentException : Exception
    {
        /// <summary>
        /// Constructor that initializes exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public StudentException(string message) : base(message) { }

        /// <summary>
        /// Constructor that initializes exception message and inner exception
        /// </summary>
        /// <param name="message">exception message</param>
        /// <param name="innerException">inner exception</param>
        public StudentException(string message, Exception innerException) : base(message, innerException) { }
    }
}
