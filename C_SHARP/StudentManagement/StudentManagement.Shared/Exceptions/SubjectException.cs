﻿namespace StudentManagement.Shared.Exceptions
{
    /// <summary>
    /// Represents error raised in subject class
    /// </summary>
    public class SubjectException : Exception
    {
        /// <summary>
        /// Constructor that initializes exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public SubjectException(string message) : base(message) { }

        /// <summary>
        /// Constructor that initializes exception message and inner exception
        /// </summary>
        /// <param name="message">exception message</param>
        /// <param name="innerException">inner exception</param>
        public SubjectException(string message, Exception innerException) : base(message, innerException) { }
    }
}
