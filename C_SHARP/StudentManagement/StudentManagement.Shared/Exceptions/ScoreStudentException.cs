﻿namespace StudentManagement.Shared.Exceptions
{
    /// <summary>
    /// Represents error raised in Score Student class
    /// </summary>
    public class ScoreStudentException : Exception
    {
        /// <summary>
        /// Constructor that initializes exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public ScoreStudentException(string message) : base(message) { }

        /// <summary>
        /// Constructor that initializes exception message and inner exception
        /// </summary>
        /// <param name="message">exception message</param>
        /// <param name="innerException">inner exception</param>
        public ScoreStudentException(string message, Exception innerException) : base(message, innerException) { }
    }
}
