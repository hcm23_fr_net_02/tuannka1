﻿using Microsoft.AspNetCore.Mvc;
using StudentManagement.DataAccessLayer.Database.Data;
using StudentManagement.Shared.Entities;

namespace StudentManagement.PresentationLayer.WebAppMVC.Controllers
{
    public class StudentsController : Controller
    {
        #region Private fields
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constructor
        public StudentsController(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            List<Student> students = _context.Students.ToList();
            return View(students);
        }

        public async Task<IActionResult> InsertUpdateAsync(Guid? id)
        {
            ///note: always pass student into View InsertUpdate 
            ///if Insert: student default created
            Student? student = new Student();
            if (id is null)
            {
                //create request
                return View(student);
            }
            //update request
            student = await _context.Students.FindAsync(id);
            if (student is null)
            {
                return NotFound();
            }
            return View(student);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> InsertUpdate(Student student)
        {
            ///if there are any modelstate validations on Student object => trigger validations
            if (ModelState.IsValid)
            {
                if (student.Id == new Guid())
                {
                    //create request
                    await _context.AddAsync(student);
                }
                else
                {
                    //update request
                    _context.Update(student);
                }
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }


        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var student = _context.Students.Find(id);
            if (student is null)
            {
                return NotFound();
            }
            _context.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        #endregion
    }
}
