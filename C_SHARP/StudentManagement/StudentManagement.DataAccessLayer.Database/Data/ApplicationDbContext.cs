﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StudentManagement.DataAccessLayer.Database.TypeConfiguration;
using StudentManagement.Shared.Entities;

namespace StudentManagement.DataAccessLayer.Database.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public ApplicationDbContext() { }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /// Log all query to Console with WriteLine method
            /// only get command into log
            
            //optionsBuilder
            //    .UseSqlServer("Server=(LocalDb)\\MSSQLLocalDB;Database=StudentManagement;TrustServerCertificate=True;Trusted_Connection=True;")
            //    .LogTo(
            //        Console.WriteLine,
            //        new[] { DbLoggerCategory.Database.Command.Name },
            //        LogLevel.Information
            //    );
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new StudentConfiguration());
            modelBuilder.ApplyConfiguration(new SubjectConfiguration());
            modelBuilder.ApplyConfiguration(new ScoreStudentConfiguration());
        }
    }
}
