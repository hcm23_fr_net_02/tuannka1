﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.Shared.Entities;

namespace StudentManagement.DataAccessLayer.Database.TypeConfiguration
{
    internal class SubjectConfiguration : IEntityTypeConfiguration<Subject>
    {
        public void Configure(EntityTypeBuilder<Subject> modelBuilder)
        {
            modelBuilder.Property(s => s.Name).HasMaxLength(60);

            modelBuilder.HasData(
                new Subject
                {
                    Id = Guid.NewGuid(),
                    Name = "Maths"
                }
            );
        }
    }
}
