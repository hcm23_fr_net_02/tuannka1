﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.Shared.Entities;

namespace StudentManagement.DataAccessLayer.Database.TypeConfiguration
{
    internal class ScoreStudentConfiguration : IEntityTypeConfiguration<ScoreStudent>
    {
        public void Configure(EntityTypeBuilder<ScoreStudent> modelBuilder)
        {
            modelBuilder
                .HasOne<Student>()
                .WithMany(s => s.Scores)
                .HasForeignKey(scoreStudent => scoreStudent.StudentId);

            modelBuilder
                .HasOne<Subject>()
                .WithMany()
                .HasForeignKey(scoreStudent => scoreStudent.SubjectId);
        }
    }
}
