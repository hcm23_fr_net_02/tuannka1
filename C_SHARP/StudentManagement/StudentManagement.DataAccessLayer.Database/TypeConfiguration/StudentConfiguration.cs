﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.Shared.Entities;
using System.Reflection.Emit;

namespace StudentManagement.DataAccessLayer.Database.TypeConfiguration
{
    internal class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> modelBuilder)
        {
            modelBuilder.Property(s => s.Name).HasMaxLength(60);
            modelBuilder.Property(s => s.PhoneNumber).HasMaxLength(10);

            modelBuilder.HasData(
                new Student
                {
                    Id = Guid.NewGuid(),
                    Code = 1000001,
                    BirthDate = new DateTime(2000, 12, 23),
                    Gender= true,
                    Name = "Tuan",
                    PhoneNumber = "0916709047"
                }
            );
        }
    }
}
