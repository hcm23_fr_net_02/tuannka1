﻿using Microsoft.EntityFrameworkCore;
using StudentManagement.DataAccessLayer.Contracts;
using StudentManagement.DataAccessLayer.Database.Data;
using StudentManagement.Shared.Entities;
using StudentManagement.Shared.Exceptions;
using System;

namespace StudentManagement.DataAccessLayer.Database
{
    /// <summary>
    /// Represents Data Access Layer for Student
    /// </summary>
    public class StudentsDataAccessLayer : IStudentsDataAccessLayer
    {
        #region Methods
        /// <summary>
        /// Adds a new Student to the existing Students list
        /// </summary>
        /// <param name="student">The Student object to add</param>
        /// <returns>Returns StudentID of Student object just created</returns>
        public async Task<Guid> AddStudentAsync(Student student)
        {
            /// data stored in database only when call save change method
            try
            {
                using var context = new ApplicationDbContext();
                var queryStudents = context.Students;
                var studentEntity = await queryStudents.AddAsync(student);
                await context.SaveChangesAsync();
                return studentEntity.Entity.Id;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes an existing Student
        /// </summary>
        /// <param name="studentId">StudentId to delete</param>
        /// <returns>Returns true, that indicates the Student is deleted successfully; otherwise, returns false</returns>
        public async Task<bool> DeleteStudentAsync(Guid studentId)
        {
            try
            {
                using var context = new ApplicationDbContext();
                var queryStudents = context.Students;

                var removeStudent = await queryStudents.FindAsync(studentId);

                if (removeStudent is null)
                    return false;

                queryStudents.Remove(removeStudent);

                await context.SaveChangesAsync();
                return true;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns all existing Students
        /// </summary>
        /// <returns>The list of all existing Students</returns>
        public async Task<List<Student>> GetStudentsAsync()
        {
            try
            {
                using var context = new ApplicationDbContext();
                var queryStudents = await context.Students.ToListAsync();
                return queryStudents;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves a paginated list of students asynchronously.
        /// </summary>
        /// <param name="pageNumber">The page number to retrieve.</param>
        /// <param name="numberPerPage">The number of students per page.</param>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains a list of students for the specified page.
        /// </returns>
        public async Task<List<Student>> GetStudentsPagingAsync(int pageNumber = 0, int numberPerPage = 5)
        {
            try
            {
                using var context = new ApplicationDbContext();
                var queryStudents = await context.Students
                                        .Skip(pageNumber * numberPerPage)
                                        .Take(numberPerPage)
                                        .ToListAsync();
                return queryStudents;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns a set of Students that matches with specified criteria
        /// </summary>
        /// <param name="predicate">Lamdba expression that contains condition to check</param>
        /// <returns>The list of matching Students</returns>
        public async Task<List<Student>> GetStudentsByConditionAsync(Func<Student, bool> predicate)
        {
            try
            {
                using var context = new ApplicationDbContext();
                var queryStudents = (await context.Students.ToListAsync()).Where(predicate);
                return queryStudents.ToList();
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates an existing Student
        /// </summary>
        /// <param name="student">Student object that contains Student details to update</param>
        /// <returns>Returns true, that indicates the Student is updated successfully; otherwise, returns false</returns>
        public async Task<bool> UpdateStudentAsync(Student student)
        {
            try
            {
                using var context = new ApplicationDbContext();
                var existingStudent = await context.Students.FindAsync(student.Id);

                if (existingStudent is null)
                    return false;

                existingStudent.Name = student.Name;
                existingStudent.BirthDate = student.BirthDate;
                existingStudent.Gender = student.Gender;
                existingStudent.PhoneNumber = student.PhoneNumber;

                context.SaveChanges();
                return true;
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
