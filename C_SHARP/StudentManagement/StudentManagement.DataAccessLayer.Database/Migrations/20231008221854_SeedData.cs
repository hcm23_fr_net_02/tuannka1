﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace StudentManagement.DataAccessLayer.Database.Migrations
{
    /// <inheritdoc />
    public partial class SeedData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "BirthDate", "Code", "Gender", "Name", "PhoneNumber" },
                values: new object[] { new Guid("89e90a0e-964a-43c8-8a9b-f77d093b052c"), new DateTime(2000, 12, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), 1000001L, true, "Tuan", "0916709047" });

            migrationBuilder.InsertData(
                table: "Subjects",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("31a298f0-66b5-489e-acc3-7b188d6d1012"), "Maths" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "Id",
                keyValue: new Guid("89e90a0e-964a-43c8-8a9b-f77d093b052c"));

            migrationBuilder.DeleteData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: new Guid("31a298f0-66b5-489e-acc3-7b188d6d1012"));
        }
    }
}
