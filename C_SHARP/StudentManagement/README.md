Write a console application to manage students. The application will manage student and subject information. Besides, it also stores the subjects that students have studied

Each student will have the following information stored:
- StudentId: ID unique of each student
- StudentCode: Must be positive. Note, student code is automatically generated with the rule, if no student has been saved then student code = student code base + 1 (with student code base = 1000000). If a student already exists in the data, the student code of the newly added student must be equal to the largest student code in the data source + 1.
- StudentName: student's name, cannot be null and cannot be larger than 60 characters
- BirthDate: student's date of birth, date of birth cannot be later than today. Student's age must be greater than or equal to 18 years old
- Gender: true is male, false is female
- PhoneNumber: must be a string of 10 characters, and the string must be all numbers

Each subject will have the following information stored:
- SubjectId: ID unique of each subject
- SubjectName: 

Requirement: 
- Using file.json as data source and database
- Using async method in order to implement application