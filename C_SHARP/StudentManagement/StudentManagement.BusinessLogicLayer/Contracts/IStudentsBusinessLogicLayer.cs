﻿using StudentManagement.Shared.Entities;

namespace StudentManagement.BusinessLogicLayer.Contracts
{
    /// <summary>
    /// Represents interface of student business logic layer
    /// </summary>
    public interface IStudentsBusinessLogicLayer
    {
        /// <summary>
        /// Returns all existing Students
        /// </summary>
        /// <returns>The list of all existing Students</returns>
        Task<List<Student>> GetStudentsAsync();

        /// <summary>
        /// Retrieves a paginated list of students asynchronously.
        /// </summary>
        /// <param name="pageNumber">The page number to retrieve.</param>
        /// <param name="numberPerPage">The number of students per page.</param>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains a list of students for the specified page.
        /// </returns>
        Task<List<Student>> GetStudentsPagingAsync(int pageNumber = 0, int numberPerPage = 5);

        /// <summary>
        /// Returns a set of Students that matches with specified criteria
        /// </summary>
        /// <param name="predicate">Lamdba expression that contains condition to check</param>
        /// <returns>The list of matching Students</returns>
        Task<List<Student>> GetStudentsByConditionAsync(Func<Student, bool> predicate);

        /// <summary>
        /// Adds a new Student to the existing Students list
        /// </summary>
        /// <param name="student">The Student object to add</param>
        /// <returns>Returns StudentId of Student object just created</returns>
        Task<Guid> AddStudentAsync(Student student);

        /// <summary>
        /// Updates an existing Student
        /// </summary>
        /// <param name="student">Student object that contains Student details to update</param>
        /// <returns>Returns true, that indicates the Student is updated successfully; otherwise, returns false</returns>
        Task<bool> UpdateStudentAsync(Student student);

        /// <summary>
        /// Deletes an existing Student
        /// </summary>
        /// <param name="studentId">StudentId to delete</param>
        /// <returns>Returns true, that indicates the Student is deleted successfully; otherwise, returns false</returns>
        Task<bool> DeleteStudentAsync(Guid studentId);
    }
}
