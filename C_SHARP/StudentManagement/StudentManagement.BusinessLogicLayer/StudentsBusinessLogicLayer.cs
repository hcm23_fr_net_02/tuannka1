﻿using StudentManagement.BusinessLogicLayer.Contracts;
using StudentManagement.DataAccessLayer.Contracts;
using StudentManagement.DataAccessLayer.Database;
using StudentManagement.Shared.Configuration;
using StudentManagement.Shared.Entities;
using StudentManagement.Shared.Exceptions;

namespace StudentManagement.BusinessLogicLayer
{
    /// <summary>
    /// Represents Business Logic Layer for Student
    /// </summary>
    public class StudentsBusinessLogicLayer : IStudentsBusinessLogicLayer
    {
        #region Private Fields
        private IStudentsDataAccessLayer _studentsDataAccessLayer;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor that initializes StudentsDataAccessLayer
        /// </summary>
        public StudentsBusinessLogicLayer()
        {
            _studentsDataAccessLayer = new StudentsDataAccessLayer();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Private property that represents reference of StudentsDataAccessLayer
        /// </summary>
        private IStudentsDataAccessLayer StudentsDataAccessLayer
        {
            set => _studentsDataAccessLayer = value;
            get => _studentsDataAccessLayer;
        }
        #endregion

        #region Methods
        // <summary>
        /// Returns all existing Students
        /// </summary>
        /// <returns>List of Students</returns>
        public async Task<List<Student>> GetStudentsAsync()
        {
            try
            {
                //invoke DAL
                return await StudentsDataAccessLayer.GetStudentsAsync();
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves a paginated list of students asynchronously.
        /// </summary>
        /// <param name="pageNumber">The page number to retrieve.</param>
        /// <param name="numberPerPage">The number of students per page.</param>
        /// <returns>
        /// A task that represents the asynchronous operation.
        /// The task result contains a list of students for the specified page.
        /// </returns>
        public async Task<List<Student>> GetStudentsPagingAsync(int pageNumber = 0, int numberPerPage = 5)
        {
            try
            {
                //invoke DAL
                return await StudentsDataAccessLayer.GetStudentsPagingAsync(pageNumber, numberPerPage);
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns a set of Students that matches with specified criteria
        /// </summary>
        /// <param name="predicate">Lamdba expression that contains condition to check</param>
        /// <returns>The list of matching Students</returns>
        public async Task<List<Student>> GetStudentsByConditionAsync(Func<Student, bool> predicate)
        {
            try
            {
                //invoke DAL
                return await StudentsDataAccessLayer.GetStudentsByConditionAsync(predicate);
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Adds a new Student to the existing Students list
        /// </summary>
        /// <param name="student">The Student object to add</param>
        /// <returns>Returns StudentId, that indicates the Student is added successfully
        /// </returns>
        public async Task<Guid> AddStudentAsync(Student student)
        {
            try
            {
                //get all Students
                List<Student> allStudents = await StudentsDataAccessLayer.GetStudentsAsync();

                /// Business Rule
                /// 1. If there are no Students yet, then StudentCode will be equal to StudentCodeBase + 1
                /// 2. If yes, then the next StudentCode will be equal to the largest StudentCode among the existing Students + 1
                long maxCustCode = 0;
                foreach (var item in allStudents)
                {
                    if (item.Code > maxCustCode)
                    {
                        maxCustCode = item.Code;
                    }
                }

                //generate new StudentCode
                if (allStudents.Count >= 1)
                {
                    student.Code = maxCustCode + 1;
                }
                else
                {
                    student.Code = Settings.BaseStudentCode + 1;
                }

                //generate new StudentId
                student.Id = Guid.NewGuid();

                //check business rules
                CheckStudentBusinessRule(student);

                //invoke DAL
                return await StudentsDataAccessLayer.AddStudentAsync(student);
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates an existing Student
        /// </summary>
        /// <param name="student">Student object that contains Student details to update</param>
        /// <returns>Returns true, that indicates the Student is updated successfully</returns>
        public async Task<bool> UpdateStudentAsync(Student student)
        {
            try
            {
                //check business rule
                CheckStudentBusinessRule(student);

                //invoke DAL
                return await StudentsDataAccessLayer.UpdateStudentAsync(student);
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes an existing Student
        /// </summary>
        /// <param name="studentId">StudentId to delete</param>
        /// <returns>Returns true, that indicates the Student is deleted successfully</returns>
        public async Task<bool> DeleteStudentAsync(Guid studentId)
        {
            try
            {
                //invoke DAL
                return await StudentsDataAccessLayer.DeleteStudentAsync(studentId);
            }
            catch (StudentException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Check business rule of Student object
        /// </summary>
        /// <param name="student">Student object need to checked</param>
        private void CheckStudentBusinessRule(Student student)
        {
            /// Business Rule
            /// 1. Age of Student should be greater than or equal to 18
            /// 2. Phone number should be a 10-digit number
            int age = DateTime.Today.Year
                        - student.BirthDate.Year
                        - (student.BirthDate > DateTime.Today.AddYears(-1) ? 1 : 0);
            if (age < 18)
                throw new StudentException("Age of Student shouble be greater than or equal to 18.");

            if (student.PhoneNumber.Length != 10 || student.PhoneNumber.Any(c => !char.IsDigit(c)))
                throw new StudentException("Phone number should be a 10-character number.");
        }
        #endregion
    }
}
