﻿using StudentManagement.BusinessLogicLayer;
using StudentManagement.BusinessLogicLayer.Contracts;
using StudentManagement.PresentationLayer.ConsoleApp.Utils;
using StudentManagement.Shared.Entities;
using StudentManagement.Shared.Entities.Contracts;
using System.Collections.Generic;
using System.Reflection;

namespace StudentManagement.PresentationLayer.ConsoleApp
{
    internal static class StudentsPresentation
    {
        internal static async Task AddStudentAsync()
        {
            try
            {
                //create an object of Student
                Student student = new Student();

                //read all details from the user
                Console.WriteLine("\n***Add Student***");
                Console.Write("Student Name: ");
                student.Name = Console.ReadLine();

                DateTime birthDate;
                do
                {
                    Console.Write("Birthdate (yyyy-mm-dd): ");
                } while (!DateTime.TryParse(Console.ReadLine(), out birthDate));
                student.BirthDate = birthDate;

                bool gender;
                do
                {
                    Console.Write("Gender (male: true; female: false): ");
                } while (!bool.TryParse(Console.ReadLine(), out gender));
                student.Gender = gender;

                Console.Write("Phone number: ");
                student.PhoneNumber = Console.ReadLine();

                //Create BL object
                IStudentsBusinessLogicLayer studentsBusinessLogicLayer = new StudentsBusinessLogicLayer();
                Guid newId = await studentsBusinessLogicLayer.AddStudentAsync(student);

                List<Student> matchingStudents = await studentsBusinessLogicLayer
                                                    .GetStudentsByConditionAsync(item => item.Id == newId);

                if (matchingStudents.Count >= 1)
                {
                    Console.WriteLine("New Student Id: " + matchingStudents[0].Id);
                    Console.WriteLine("Student Added.\n");
                }
                else
                {
                    Console.WriteLine("Student Not added");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        internal static async Task ViewStudentsAsync()
        {
            try
            {
                Console.WriteLine("\n***View Student***");
                //Create BL object
                IStudentsBusinessLogicLayer studentsBusinessLogicLayer = new StudentsBusinessLogicLayer();

                var currentPage = 0;
                var numberPerPage = 2;

                int menuChoice = -1;

                do
                {
                    //Console.Clear();
                    Console.WriteLine($"Current Page: {currentPage + 1}");
                    var students = await studentsBusinessLogicLayer.GetStudentsPagingAsync(currentPage, numberPerPage);

                    foreach (var student in students)
                    {
                        Console.WriteLine(student);

                        Console.WriteLine();
                    }

                    Console.WriteLine("Your choice: ");

                    Console.WriteLine("1. Previous Page");
                    Console.WriteLine("2. Next Page");

                    // Option for exit
                    Console.WriteLine("0. Exit");

                    do
                    {
                        Console.Write("Your choice: ");
                    } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                    if (menuChoice == 2 && students.Count  < numberPerPage)
                    {
                        Console.Clear();
                        Console.WriteLine("This is the last page\n");
                        continue;
                    }

                    if (menuChoice == 1 && currentPage == 0)
                    {
                        Console.Clear();
                        Console.WriteLine("Can not go back, this is already the first page\n");
                        continue;
                    }

                    switch (menuChoice)
                    {
                        case 1:
                            currentPage--;
                            break;
                        case 2:
                            currentPage++;
                            break;
                    }

                } while (menuChoice != 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        internal static async Task UpdateStudentAsync()
        {
            try
            {
                //Create BL object
                IStudentsBusinessLogicLayer studentsBusinessLogicLayer = new StudentsBusinessLogicLayer();

                if ((await studentsBusinessLogicLayer.GetStudentsAsync()).Count <= 0)
                {
                    Console.WriteLine("No students exist");
                    return;
                }

                //display existing students
                Console.WriteLine("\n***Edit Student***");
                await ViewStudentsAsync();

                //read all details from the user
                Guid studentIdToEdit;
                do
                {
                    Console.Write("Enter the Student Id that you want to edit: ");
                } while (!Guid.TryParse(Console.ReadLine(), out studentIdToEdit));

                var existingStudent = (await studentsBusinessLogicLayer
                                        .GetStudentsByConditionAsync(temp => temp.Id == studentIdToEdit))
                                        .FirstOrDefault();

                if (existingStudent == null)
                {
                    Console.WriteLine("Invalid Student Code.\n");
                    return;
                }

                Console.WriteLine("New Student details:");

                Console.Write("Student Name: ");
                existingStudent.Name = Console.ReadLine();

                DateTime birthDate;
                do
                {
                    Console.Write("Birthdate (yyyy-mm-dd): ");
                } while (!DateTime.TryParse(Console.ReadLine(), out birthDate));
                existingStudent.BirthDate = birthDate;

                bool gender;
                do
                {
                    Console.Write("Gender (male: true; female: false): ");
                } while (!bool.TryParse(Console.ReadLine(), out gender));
                existingStudent.Gender = gender;

                Console.Write("Phone number: ");
                existingStudent.PhoneNumber = Console.ReadLine();

                bool isUpdated = await studentsBusinessLogicLayer.UpdateStudentAsync(existingStudent);

                if (isUpdated)
                {
                    Console.WriteLine("Student Updated.\n");
                }
                else
                {
                    Console.WriteLine("Student not updated");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        internal static async Task SearchStudentAsync()
        {
            try
            {
                //Create BL object
                IStudentsBusinessLogicLayer studentsBusinessLogicLayer = new StudentsBusinessLogicLayer();

                if ((await studentsBusinessLogicLayer.GetStudentsAsync()).Count <= 0)
                {
                    Console.WriteLine("No students exist");
                    return;
                }

                //display existing students
                Console.WriteLine("\n***Search Student***");
                await ViewStudentsAsync();

                //read all details from the user
                Guid studentIdToEdit;

                do
                {
                    Console.Write("Enter the Student Id that you want to get: ");
                } while (!Guid.TryParse(Console.ReadLine(), out studentIdToEdit));

                var existingStudent = (await studentsBusinessLogicLayer
                                        .GetStudentsByConditionAsync(temp => temp.Id == studentIdToEdit))
                                        .FirstOrDefault();

                if (existingStudent == null)
                {
                    Console.WriteLine("Invalid Student Code.\n");
                    return;
                }

                Console.WriteLine(existingStudent);

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        internal static async Task DeleteStudentAsync()
        {
            try
            {
                //Create BL object
                IStudentsBusinessLogicLayer studentsBusinessLogicLayer = new StudentsBusinessLogicLayer();

                if ((await studentsBusinessLogicLayer.GetStudentsAsync()).Count <= 0)
                {
                    Console.WriteLine("No students exist");
                    return;
                }

                //display existing students
                Console.WriteLine("\n***Delete Student***");
                await ViewStudentsAsync();

                //read all details from the user
                Guid studentIdToEdit;

                do
                {
                    Console.Write("Enter the Student Id that you want to delete: ");
                } while (!Guid.TryParse(Console.ReadLine(), out studentIdToEdit));

                var existingStudent = (await studentsBusinessLogicLayer
                                        .GetStudentsByConditionAsync(temp => temp.Id == studentIdToEdit))
                                        .FirstOrDefault();

                if (existingStudent == null)
                {
                    Console.WriteLine("Invalid Student Id.\n");
                    return;
                }

                bool isDeleted = await studentsBusinessLogicLayer.DeleteStudentAsync(existingStudent.Id);

                if (isDeleted)
                {
                    Console.WriteLine("Student Deleted.\n");
                }
                else
                {
                    Console.WriteLine("Student not deleted");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

    }
}
