﻿using StudentManagement.PresentationLayer.ConsoleApp;
using StudentManagement.PresentationLayer.ConsoleApp.Utils;

var applicationName = "Product Management Application";

//display application name

int A = 5;
Console.WriteLine(A);

Console.WriteLine($"***{applicationName}***");

var menuName = "Main Menu";
var options = new Dictionary<string, Func<Task>>()
{
    { "Add student", StudentsPresentation.AddStudentAsync },
    { "Delete student", StudentsPresentation.DeleteStudentAsync },
    { "Update student", StudentsPresentation.UpdateStudentAsync },
    { "Search student", StudentsPresentation.SearchStudentAsync },
    { "View student", StudentsPresentation.ViewStudentsAsync },
};

await Menu.ShowMenuAsync(menuName, options);

//Exit 
Console.WriteLine("\nSee you again");