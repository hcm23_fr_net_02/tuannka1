﻿//var count1 = CountDown(10); //At here, countdown start but not await 
//var count2 = CountDown(5);
//// tao dong thoi
//await count1;
//await count2;
async Task CountDown(int seconds)
{
    for (int i = seconds; i >= 0; i--)
    {
        Console.WriteLine($"Remaining time: {i} seconds"); await Task.Delay(1000); // chờ bất đồng bộ
    }
}

//var task1 = LongRunningTaskAsync();
//var task2 = LongRunningTaskAsync();
//var task3 = LongRunningTaskAsync();
//Task allTasks = Task.WhenAll(task1, task2, task3);
//await allTasks; // Đợi cho đến khi tất cả các task hoànthành

// Tiếp tục xử lý sau khi tất cả các task hoàn thành

async Task LongRunningTaskAsync()
{
    Console.WriteLine("Long running task started.");// Giả lập một tác vụ tốn thời gian
    await Task.Delay(5000);
    Console.WriteLine("Long running task completed.");
}

var countWhen1 = CountDown(10);
var countWhen2 = CountDown(5);
var countWhen3 = CountDown(15);
Task allWhenTasks = Task.WhenAll(countWhen1, countWhen1, countWhen1); 
await allWhenTasks; // Đợi cho đến khi tất cả các task hoànthành