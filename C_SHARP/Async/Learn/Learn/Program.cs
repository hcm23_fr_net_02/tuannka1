﻿using System.Diagnostics;

Stopwatch stopWatch;

//Console.WriteLine("*************Test Async G***********");

//stopWatch = Stopwatch.StartNew();
//stopWatch.Start();
//var intAsync1 = G();
//var intAsync2 = G();
//Console.WriteLine(await intAsync1);
//Console.WriteLine(await intAsync2);
//stopWatch.Stop();

//Console.WriteLine($"Time Async G: {stopWatch.ElapsedMilliseconds}");

Console.WriteLine("*************Async***********");

stopWatch = Stopwatch.StartNew();
stopWatch.Start();
var taskAsync1 = LongRunningTaskAsync();
var taskAsync2 = LongRunningTaskAsync();
Thread.Sleep(3000);
Console.WriteLine("Rick");
await taskAsync1;
await taskAsync2;
stopWatch.Stop();

Console.WriteLine($"Time Async: {stopWatch.ElapsedMilliseconds}");

//Console.WriteLine("*************Async Test 2***********");

//stopWatch = Stopwatch.StartNew();
//stopWatch.Start();
//await LongRunningTaskAsync();
//await LongRunningTaskAsync();

//stopWatch.Stop();

//Console.WriteLine($"Time Sync Test 2: {stopWatch.ElapsedMilliseconds}");

//Console.WriteLine("*************Async Test 3: When dont call await***********");

//stopWatch = Stopwatch.StartNew();
//stopWatch.Start();
//LongRunningTaskAsync();
//LongRunningTaskAsync();

//stopWatch.Stop();

//Console.WriteLine($"Time Sync Test 3: {stopWatch.ElapsedMilliseconds}");

//Console.WriteLine("*************Sync***********");
//stopWatch = Stopwatch.StartNew();
//stopWatch.Start();
//LongRunningTask();
//LongRunningTask();
//stopWatch.Stop();

//Console.WriteLine($"Time Sync: {stopWatch.ElapsedMilliseconds}");

async Task LongRunningTaskAsync()
{
    Console.WriteLine("Long running task started.");
    await Task.Delay(5000);
    Console.WriteLine("Long running task completed.");
}

void LongRunningTask()
{
    // Giả lập một tác vụ tốn thời gian
    Thread.Sleep(5000);
    Console.WriteLine("Long running task completed.");
}

async Task<int> G()
{
    // simulate network call
    await Task.Delay(5000);
    return 42;
}