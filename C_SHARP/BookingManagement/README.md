SRS
- Write a booking management program
- There are functions: login and account registration
- There are two types of accounts: admin and customer

- Customers can view detailed information of each villa
- Customers can search for villas based on personal needs, check-in date, and how long they will stay. The system will display available villas for users to choose.
- Customers can perform booking functions: choose booking place, choose date,...
- Customers can make payment via credit card
- Customers can manage their booking

- Admin can view reports about bookings, such as revenue,...
- Admin can manage information of villas
- Admin can manage booking information of all customers