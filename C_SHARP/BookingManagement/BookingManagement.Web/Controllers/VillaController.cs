﻿using BookingManagement.Domain.Entities;
using BookingManagement.Infrastructure.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;

namespace BookingManagement.Web.Controllers
{
    public class VillaController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        public VillaController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IActionResult> Index()
        {
            var villas = await _dbContext.Villas.ToListAsync();
            return View(villas);
        }

        public async Task<IActionResult> Create()
        {
            return await Task.FromResult(View());
        }

        [HttpPost]
        public async Task<IActionResult> Create(Villa obj)
        {
            if (!ModelState.IsValid)
                return View(obj);

            _dbContext.Villas.Add(obj);
            await _dbContext.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
