﻿using BookingManagement.Domain.Entities;
using BookingManagement.Infrastructure.TypeConfiguration;
using Microsoft.EntityFrameworkCore;

namespace BookingManagement.Infrastructure.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Villa> Villas { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new VillaConfiguration());
        }
    }
}
