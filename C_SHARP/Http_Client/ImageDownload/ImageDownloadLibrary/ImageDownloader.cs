﻿using System;

namespace ImageDownloaderLibrary
{
    public class ImageDownloader : IImageDownloader
    {
        public async Task DownloadImagesAsync(List<string> imageUrls, string outputFolder)
        {
            List<Task> downloadTasks = new List<Task>();

            foreach (string imageUrl in imageUrls)
            {
                string fileName = Path.GetFileName(imageUrl);
                string outputPath = Path.Combine(outputFolder, fileName);
                downloadTasks.Add(DownloadImageAsync(imageUrl, outputPath));
            }

            await Task.WhenAll(downloadTasks);
        }

        private async Task DownloadImageAsync(string imageUrl, string outputPath)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    byte[] imageBytes = await httpClient.GetByteArrayAsync(imageUrl);

                    // Create the output directory if it doesn't exist
                    Directory.CreateDirectory(Path.GetDirectoryName(outputPath) ?? "Default");

                    // Save the image to the specified file path
                    File.WriteAllBytes(outputPath, imageBytes);

                    Console.WriteLine($"Downloaded: {imageUrl}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error downloading {imageUrl}: {ex.Message}");
                }
            }
        }
    }

}