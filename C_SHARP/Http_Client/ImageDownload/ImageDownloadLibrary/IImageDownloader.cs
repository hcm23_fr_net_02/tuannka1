﻿namespace ImageDownloaderLibrary
{
    public interface IImageDownloader
    {
        Task DownloadImagesAsync(List<string> imageUrls, string outputFolder);
    }
}
