﻿using ImageDownloaderLibrary;

var imageUrls = new List<string>
            {
                "https://static01.nyt.com/images/2023/09/29/science/space-photos-september-05/space-photos-september-05-mediumThreeByTwo252-v2.jpg",
                "https://static01.nyt.com/images/2023/10/03/multimedia/03ukraine-briefing-biden-vfpq/03ukraine-briefing-biden-vfpq-mediumThreeByTwo252.jpg"
            };
string outputFolder = "DownloadedImages";
IImageDownloader imageDownloader = new ImageDownloader();

await imageDownloader.DownloadImagesAsync(imageUrls, outputFolder);