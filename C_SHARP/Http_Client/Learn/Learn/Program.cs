﻿namespace Learn
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            HttpClient httpClient = new HttpClient();
            string url = "https://jsonplaceholder.typicode.com/todos/1";
            HttpResponseMessage response = await httpClient.GetAsync(url);
            string content = await response.Content.ReadAsStringAsync();
            Console.WriteLine(content);
            httpClient.Dispose();
        }
    }
}