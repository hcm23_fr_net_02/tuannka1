﻿using TodoApiClientLibrary;

namespace TodoApp
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            ITodoApiClient apiClient = new TodoApiClient();

            while (true)
            {
                Console.WriteLine("Todo App Menu:");
                Console.WriteLine("1. Display todos");
                Console.WriteLine("2. Add a new todo");
                Console.WriteLine("3. Delete a todo");
                Console.WriteLine("4. Mark a todo as completed");
                Console.WriteLine("5. Raise priority");
                Console.WriteLine("6. Exit");

                Console.Write("Enter your choice: ");
                string choice = Console.ReadLine() ?? string.Empty;

                switch (choice)
                {
                    case "1":
                        // Display todos by creation date

                        // Implement logic to display todos by creation date

                        Console.WriteLine();
                        Console.WriteLine("1. Display todos by creation date");
                        Console.WriteLine("2. Display todos by priority");

                        bool isValid = DisplayFormat.TryParse(Console.ReadLine(), out DisplayFormat displayChoice);

                        if (!isValid)
                        {
                            Console.WriteLine("Invalid Input");
                            continue;
                        }

                        try
                        {
                            var todos = await apiClient.GetTodosAsync(displayChoice);
                            DisplayTodos(todos);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Cannot display todoList!");
                        }
                        break;

                    case "2":
                        // Add a new todo
                        // Implement logic to add a new todo

                        Console.Write("Enter title: ");
                        string newTitle = Console.ReadLine() ?? string.Empty;

                        Console.Write("Priority: ");
                        int newPriority = int.Parse(Console.ReadLine() ?? "0");

                        var todo = new Todo(newTitle, newPriority);

                        // Call the AddTodoAsync method to add the new todo
                        try
                        {
                            Todo addedTodo = await apiClient.AddTodoAsync(todo);
                            Console.WriteLine($"Todo added: {addedTodo.Id}");
                        }
                        catch (Exception)
                        {
                            Console.WriteLine($"Cannot add Todo item with info: title = {newTitle}; priority = {newPriority}");
                        }
                        break;

                    case "3":
                        // Delete a todo
                        // Implement logic to delete a todo
                        Console.Write("Enter remove id: ");
                        string removeId = Console.ReadLine() ?? string.Empty;

                        try
                        {
                            await apiClient.DeleteTodoAsync(removeId);
                            Console.WriteLine($"Todo with ID {removeId} has been deleted.");

                        }
                        catch (Exception)
                        {
                            Console.WriteLine($"Failed to delete todo with ID {removeId}.");
                        }
                        break;

                    case "4":
                        // Mark a todo as completed
                        Console.Write("Enter the ID of the todo to mark as completed: ");
                        string todoIdToMark = Console.ReadLine() ?? string.Empty;

                        // Call the MarkTodoAsCompletedAsync method to mark the todo as completed
                        try
                        {
                            await apiClient.MarkTodoAsCompletedAsync(todoIdToMark);
                            Console.WriteLine($"Todo with ID {todoIdToMark} has been marked as completed.");

                        }
                        catch (Exception)
                        {
                            Console.WriteLine($"Failed to mark todo with ID {todoIdToMark} as completed.");
                        }
                        break;

                    case "5":
                        // Raise priority
                        Console.Write("Enter the ID of the todo to raise priority: ");
                        string todoIdToRaise = Console.ReadLine() ?? string.Empty;

                        Console.Write("Enter the number of priority levels to raise: ");

                        bool isValidLevels = int.TryParse(Console.ReadLine(), out int levelsToRaise);
                        if (!isValidLevels)
                        {
                            Console.WriteLine("Invalid input");
                            continue;
                        }

                        // Call the RaisePriorityAsync method to raise the priority of the todo
                        try
                        {
                            await apiClient.RaisePriorityAsync(todoIdToRaise, levelsToRaise);
                            Console.WriteLine($"Priority of todo with ID {todoIdToRaise} has been raised by {levelsToRaise} levels.");
                        }
                        catch (Exception)
                        {
                            Console.WriteLine($"Failed to raise priority of todo with ID {todoIdToRaise}.");
                        }
                        break;

                    case "6":
                        // Exit the application
                        Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine("Invalid choice. Please try again.");
                        break;
                }
                Console.WriteLine();
            }
        }

        static void DisplayTodos(List<Todo> todos)
        {
            Console.WriteLine("Todo List:");
            foreach (var todo in todos)
                Console.WriteLine(todo);
        }
    }

}