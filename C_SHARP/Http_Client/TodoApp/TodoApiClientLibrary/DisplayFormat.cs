﻿namespace TodoApiClientLibrary
{
    public enum DisplayFormat
    {
        ByDate = 1,
        ByPriority = 2
    }
}
