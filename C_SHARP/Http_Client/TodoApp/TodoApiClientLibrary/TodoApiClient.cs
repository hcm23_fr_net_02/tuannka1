﻿using System.Text;
using System.Text.Json;
using System.Web;

namespace TodoApiClientLibrary
{
    public class TodoApiClient : ITodoApiClient
    {
        private readonly HttpClient _httpClient;
        private readonly string _baseUrl = "https://651e456c44a3a8aa4767f2b8.mockapi.io/todos"; // Replace with your API endpoint URL

        public TodoApiClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<Todo> AddTodoAsync(Todo item)
        {
            var json = JsonSerializer.Serialize(item, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _httpClient.PostAsync($"{_baseUrl}", content);

            // Check status code
            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException("Error when call HTTP request");

            string responseContent = await response.Content.ReadAsStringAsync();

            var todo = JsonSerializer.Deserialize<Todo>(
                responseContent, 
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }
            );

            if (todo is null)
                throw new JsonException("the JSON text is not compatible with the type of a property on an object");
            return todo;
        }

        /// <summary>
        /// Get List Todo items
        /// </summary>
        /// <param name="display">Display Format: 1 - ByDate, 2 - ByPriority</param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException"></exception>
        public async Task<List<Todo>> GetTodosAsync(DisplayFormat display)
        {
            // Create url with parameters
            var builder = new UriBuilder(_baseUrl);
            var query = HttpUtility.ParseQueryString(builder.Query);

            var (sortBy, order) = display switch
            {
                DisplayFormat.ByPriority
                    => ("priority", "asc"),
                _
                    => ("createdAt", "desc")
            };

            query[nameof(sortBy)] = sortBy;
            query[nameof(order)] = order;

            builder.Query = query.ToString();
            string url = builder.ToString();

            // Action Request
            var response = await _httpClient.GetAsync(url);

            // Check status code
            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException("Error when call HTTP request");

            string contentString = await response.Content.ReadAsStringAsync();

            // CaseInsensive When Deserialize
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            var todoLists = JsonSerializer.Deserialize<List<Todo>>(contentString, options);

            if (todoLists is null)
                throw new JsonException("the JSON text is not compatible with the type of a property on an object");
            return todoLists;
        }

        public async Task DeleteTodoAsync(string todoId)
        {
            HttpResponseMessage response = await _httpClient.DeleteAsync($"{_baseUrl}/{todoId}");

            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException("Error when call HTTP request");
        }

        public async Task MarkTodoAsCompletedAsync(string todoId)
        {
            var updateData = new
            {
                IsComplete = true
            };

            var json = JsonSerializer.Serialize(updateData, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _httpClient.PutAsync($"{_baseUrl}/{todoId}", content);

            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException("Error when call HTTP request");
        }

        public async Task RaisePriorityAsync(string todoId, int priority)
        {
            var todo = await GetAsync(todoId);

            var updateData = new
            {
                Priority = todo.Priority - priority
            };

            var json = JsonSerializer.Serialize(updateData, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _httpClient.PutAsync($"{_baseUrl}/{todoId}", content);

            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException();
        }

        /// <summary>
        /// Get Todo item
        /// </summary>
        /// <param name="todoId"></param>
        /// <returns></returns>
        public async Task<Todo> GetAsync(string todoId)
        {
            // Create url
            var builder = new UriBuilder(_baseUrl);
            var query = HttpUtility.ParseQueryString(builder.Query);

            builder.Query = query.ToString();
            string url = builder.ToString();

            // Perform Request
            var response = await _httpClient.GetAsync($"{url}/{todoId}");

            if (response.IsSuccessStatusCode)
                throw new HttpRequestException("Error when call HTTP request");

            string contentString = await response.Content.ReadAsStringAsync();

            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            var item = JsonSerializer.Deserialize<Todo>(contentString, options);

            if (item is null)
                throw new Exception("Cannot deserialize to Todo item");
            return item;
        }
    }

}