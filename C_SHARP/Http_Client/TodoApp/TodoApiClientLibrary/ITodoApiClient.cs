﻿namespace TodoApiClientLibrary
{
    public interface ITodoApiClient
    {
        Task<List<Todo>> GetTodosAsync(DisplayFormat display);
        Task<Todo> AddTodoAsync(Todo item);
        Task DeleteTodoAsync(string todoId);
        Task MarkTodoAsCompletedAsync(string todoId);
        Task RaisePriorityAsync(string todoId, int levelsToRaise);
        Task<Todo> GetAsync(string todoId);
    }
}