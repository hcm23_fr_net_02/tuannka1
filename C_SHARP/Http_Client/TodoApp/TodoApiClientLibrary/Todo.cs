﻿namespace TodoApiClientLibrary
{
    public class Todo
    {
        public DateTime CreatedAt { get; set; }
        public string TaskTitle { get; set; }
        public bool IsComplete { get; set; }
        public int Priority { get; set; }
        public string Id { get; set; }

        public Todo(string taskTitle, int priority)
        {
            TaskTitle = taskTitle;
            Priority = priority;
            CreatedAt = DateTime.Now;
            IsComplete = false;
        }

        public override string ToString()
        {
            return $"Id: {Id},\tTaskTitle: {TaskTitle},\tIsCompete: {IsComplete},\tCreateAt: {CreatedAt},\tPriority: {Priority}";
        }
    }
}