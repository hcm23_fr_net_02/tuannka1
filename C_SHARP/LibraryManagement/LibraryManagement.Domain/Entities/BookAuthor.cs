﻿using LibraryManagement.Domain.Shared.Constants;
using LibraryManagement.Domain.Shared.Exceptions;
using System.Text;

namespace LibraryManagement.Domain.Entities
{
    public class BookAuthor
    {
        #region Private fields
        private int _id;
        private int _bookId;
        private int _authorId;
        private Book? _book;
        private Author? _author;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public int BookId
        {
            get => _bookId;
            set => _bookId = value;
        }

        public int AuthorId
        {
            get => _authorId;
            set => _authorId = value;
        }

        public Book? Book
        {
            get => _book;
            set => _book = value;
        }

        public Author? Author
        {
            get => _author;
            set => _author = value;
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tBook Id: {Id}");

            return stringBuilder.ToString();
        }
        #endregion
    }
}