﻿using LibraryManagement.Domain.Shared.Constants;
using LibraryManagement.Domain.Shared.Exceptions;

namespace LibraryManagement.Domain.Entities
{
    public class Review
    {
        #region Private fields
        private int _id;
        private string _title;
        private string _content;
        private byte _rating;
        private int _bookId;
        private Book? _book;
        #endregion

        #region Public Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Title
        {
            get => _title;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > ReviewConstant.MaxLengthTitle)
                    throw new ReviewException($"Review Title should not be null, whitespace or greater than {ReviewConstant.MaxLengthTitle} characters long.");
                _title = value;
            }
        }

        public string Content
        {
            get => _content;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > ReviewConstant.MaxLengthContent)
                    throw new ReviewException($"Review Content should not be null, whitespace or greater than {ReviewConstant.MaxLengthContent} characters long.");
                _content = value;
            }
        }

        public byte Rating
        {
            get => _rating;
            set
            {
                if (value < ReviewConstant.MinValueRating || value > ReviewConstant.MaxValueRating)
                    throw new ReviewException($"Rating should be in range ({ReviewConstant.MinValueRating}, {ReviewConstant.MaxValueRating}).");
                _rating = value;
            }
        }

        public int BookId
        {
            get => _bookId;
            set => _bookId = value;
        }

        public Book? Book
        {
            get => _book;
            set => _book = value;
        }
        #endregion
    }
}