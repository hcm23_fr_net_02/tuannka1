﻿namespace LibraryManagement.Domain.Entities
{
    public class BorrowDetail
    {
        #region Private fields
        private int _id;
        private DateTime _borrowedDate;
        private DateTime _returnDate;
        private DateTime? _actualReturnDate;
        private int _bookId;
        private int _readerId;
        private Book? _book;
        private Reader? _reader;
        #endregion

        #region Public Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public DateTime BorrowedDate
        {
            get => _borrowedDate;
            set => _borrowedDate = value;
        }

        public DateTime ReturnDate
        {
            get => _returnDate;
            set => _returnDate = value;
        }

        public DateTime? ActualReturnDate
        {
            get => _actualReturnDate;
            set => _actualReturnDate = value;
        }

        public int BookId
        {
            get => _bookId;
            set => _bookId = value;
        }

        public int ReaderId
        {
            get => _readerId;
            set => _readerId = value;
        }

        public Book? Book
        {
            get => _book;
            set => _book = value;
        }

        public Reader? Reader
        {
            get => _reader;
            set => _reader = value;
        }
        #endregion
    }
}