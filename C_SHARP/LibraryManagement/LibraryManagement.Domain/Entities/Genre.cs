﻿using LibraryManagement.Domain.Shared.Constants;
using LibraryManagement.Domain.Shared.Exceptions;

namespace LibraryManagement.Domain.Entities
{
    public class Genre
    {
        #region Private fields
        private int _id;
        private string _name;
        private IEnumerable<BookGenre>? _bookGenres;
        #endregion

        #region Public Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > GenreConstant.MaxLengthName)
                    throw new GenreException($"Genre name should not be null, whitespace or greater than {GenreConstant.MaxLengthName} characters long.");
                _name = value;
            }
        }

        public IEnumerable<BookGenre>? BookGenres
        {
            get => _bookGenres;
            set => _bookGenres = value;
        }
        #endregion
    }
}