﻿using LibraryManagement.Domain.Shared.Constants;
using LibraryManagement.Domain.Shared.Exceptions;
using System.Text;

namespace LibraryManagement.Domain.Entities
{
    public class Book
    {
        #region Private fields
        private int _id;
        private string _title;
        private short _publicationYear;
        private ushort _quantity;
        private string? _description;
        private IEnumerable<BookAuthor>? _bookAuthors;
        private IEnumerable<Genre>? _genres;
        private IEnumerable<Review>? _reviews;
        private IEnumerable<BorrowDetail>? _borrowDetail;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Title
        {
            get => _title;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > BookConstant.MaxLengthTitle)
                    throw new BookException($"Book title should not be null, whitespace or greater than {BookConstant.MaxLengthTitle} characters long.");
                _title = value;
            }
        }

        public short PublicationYear
        {
            get => _publicationYear;
            set
            {
                if (value < BookConstant.MinPublicationYear || value > DateTime.Now.Year)
                    throw new BookException($"Publication Year should not be less than 4000BC or greater than Year now: {DateTime.Now.Year}.");
                _publicationYear = value;
            }
        }

        public ushort Quantity
        {
            get => _quantity;
            set
            {
                if (value < BookConstant.MinQuantity)
                    throw new BookException($"Quantity should be greater than or equal to {BookConstant.MinQuantity}");
                _quantity = value;
            }
        }

        public string? Description
        {
            get => _description;
            set => _description = value;
        }

        public IEnumerable<BookAuthor>? BookAuthors
        {
            get => _bookAuthors;
            set => _bookAuthors = value;
        }

        public IEnumerable<Genre>? Genres
        {
            get => _genres;
            set => _genres = value;
        }

        public IEnumerable<Review>? Review
        {
            get => _reviews;
            set => _reviews = value;
        }

        public IEnumerable<BorrowDetail>? BrowDetail
        {
            get => _borrowDetail;
            set => _borrowDetail = value;
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tBook Id: {Id}");

            return stringBuilder.ToString();
        }
        #endregion
    }
}