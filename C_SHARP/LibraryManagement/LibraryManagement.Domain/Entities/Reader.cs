﻿using LibraryManagement.Domain.Shared.Constants;
using LibraryManagement.Domain.Shared.Exceptions;

namespace LibraryManagement.Domain.Entities
{
    public class Reader
    {
        #region Private fields
        private int _id;
        private string _name;
        private IEnumerable<BorrowDetail>? _borrowDetails;
        #endregion

        #region Public Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > ReaderConstant.MaxLengthName)
                    throw new ReaderException($"Reader name should not be null, whitespace or greater than {ReaderConstant.MaxLengthName} characters long.");
                _name = value;
            }
        }

        public IEnumerable<BorrowDetail>? BorrowDetails
        {
            get => _borrowDetails;
            set => _borrowDetails = value;
        }
        #endregion
    }
}