﻿using LibraryManagement.Domain.Shared.Constants;
using LibraryManagement.Domain.Shared.Exceptions;

namespace LibraryManagement.Domain.Entities
{
    public class Author
    {
        #region Private fields
        private int _id;
        private string _name;
        private IEnumerable<BookAuthor>? _bookAuthors;
        #endregion

        #region Public Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > AuthorConstant.MaxLengthName)
                    throw new AuthorException($"Author name should not be null, whitespace or greater than {AuthorConstant.MaxLengthName} characters long.");
                _name = value;
            }
        }

        public IEnumerable<BookAuthor>? BookAuthors
        {
            get => _bookAuthors;
            set => _bookAuthors = value;
        }
        #endregion
    }
}