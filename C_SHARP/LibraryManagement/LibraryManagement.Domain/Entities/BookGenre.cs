﻿using System.Text;

namespace LibraryManagement.Domain.Entities
{
    public class BookGenre
    {
        #region Private fields
        private int _id;
        private int _bookId;
        private int _genreId;
        private Book? _book;
        private Genre? _genre;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public int BookId
        {
            get => _bookId;
            set => _bookId = value;
        }

        public int GenreId
        {
            get => _genreId;
            set => _genreId = value;
        }

        public Book? Book
        {
            get => _book;
            set => _book = value;
        }

        public Genre? Genre
        {
            get => _genre;
            set => _genre = value;
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tBook Id: {Id}");

            return stringBuilder.ToString();
        }
        #endregion
    }
}