﻿namespace LibraryManagement.Domain.Shared.Exceptions
{
    /// <summary>
    /// Represents error raised in BorrowDetail class
    /// </summary>
    public class BorrowDetailException : Exception
    {
        /// <summary>
        /// Constructor that initializes exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public BorrowDetailException(string message) : base(message) { }
    }
}