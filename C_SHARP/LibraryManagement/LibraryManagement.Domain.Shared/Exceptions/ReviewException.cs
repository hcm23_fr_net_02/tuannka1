﻿namespace LibraryManagement.Domain.Shared.Exceptions
{
    /// <summary>
    /// Represents error raised in Review class
    /// </summary>
    public class ReviewException : Exception
    {
        /// <summary>
        /// Constructor that initializes exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public ReviewException(string message) : base(message) { }
    }
}