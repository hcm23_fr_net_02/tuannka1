﻿namespace LibraryManagement.Domain.Shared.Exceptions
{
    /// <summary>
    /// Represents error raised in Author class
    /// </summary>
    public class AuthorException : Exception
    {
        /// <summary>
        /// Constructor that initializes exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public AuthorException(string message) : base(message) { }
    }
}
