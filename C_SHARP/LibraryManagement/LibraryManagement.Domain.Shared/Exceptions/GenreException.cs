﻿namespace LibraryManagement.Domain.Shared.Exceptions
{
    /// <summary>
    /// Represents error raised in Genre class
    /// </summary>
    public class GenreException : Exception
    {
        /// <summary>
        /// Constructor that initializes exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public GenreException(string message) : base(message) { }
    }
}