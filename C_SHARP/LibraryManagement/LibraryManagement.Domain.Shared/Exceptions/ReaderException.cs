﻿namespace LibraryManagement.Domain.Shared.Exceptions
{
    /// <summary>
    /// Represents error raised in Reader class
    /// </summary>
    public class ReaderException : Exception
    {
        /// <summary>
        /// Constructor that initializes exception message
        /// </summary>
        /// <param name="message">exception message</param>
        public ReaderException(string message) : base(message) { }
    }
}