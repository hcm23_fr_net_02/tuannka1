﻿namespace LibraryManagement.Domain.Shared.Constants
{
    public class ReviewConstant
    {
        public const byte MaxLengthTitle = 50;
        public const short MaxLengthContent = 3000;
        public const byte MinValueRating = 1;
        public const byte MaxValueRating = 5;
    }
}