﻿namespace LibraryManagement.Domain.Shared.Constants
{
    public static class BookConstant
    {
        public const byte MaxLengthTitle = 255;
        public const short MinPublicationYear = -4000;
        public const sbyte MinQuantity = 0;
    }
}