﻿using LibraryManagement.Application.Contracts.IService;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Service
{
    public class BorrowDetailService : IBorrowDetailService
    {
        public Task CreateAsync(BorrowDetail borrowDetail)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<BorrowDetail>> GetUnreturnedBooksPagingAsync()
        {
            throw new NotImplementedException();
        }

        public Task ReturnAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}