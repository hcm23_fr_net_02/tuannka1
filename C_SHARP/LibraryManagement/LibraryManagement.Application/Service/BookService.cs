﻿using LibraryManagement.Application.Contracts.IRepository;
using LibraryManagement.Application.Contracts.IService;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Service
{
    /// <summary>
    /// Service for managing books.
    /// </summary>
    public class BookService : IBookService
    {
        #region Private Fields
        /// <summary>
        /// The repository for accessing book data.
        /// </summary>
        private readonly IBookRepository _bookRepository;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the BookService class.
        /// </summary>
        /// <param name="bookRepository">The repository for accessing book data.</param>
        public BookService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Create a new book.
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public async Task CreateAsync(Book book)
        {
            try
            {
                await _bookRepository.AddAsync(book);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                Book? objFromRepo = await _bookRepository.GetAsync(u => u.Id == id);
                if (objFromRepo is null)
                    return false;

                await _bookRepository.RemoveAsync(objFromRepo);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Book> GetDetailByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Book>> GetPagingAsync()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Book>> GetPagingByAuthorNameAsync()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Book>> GetPagingByGenreNameAsync()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Book>> GetPagingByPublicationYearAsync()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Book>> GetPagingByTitleAsync()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public Task UpdateAsync(Book book)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}