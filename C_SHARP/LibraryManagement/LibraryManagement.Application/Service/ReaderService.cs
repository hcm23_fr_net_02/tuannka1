﻿using LibraryManagement.Application.Contracts.IService;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Service
{
    public class ReaderService : IReaderService
    {
        public Task CreateAsync(Reader Reader)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Reader>> GetPagingAsync()
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Reader Reader)
        {
            throw new NotImplementedException();
        }
    }
}