﻿using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Contracts.IService
{
    internal interface IReaderService
    {
        Task<IEnumerable<Reader>> GetPagingAsync();
        Task CreateAsync(Reader Reader);
        Task UpdateAsync(Reader Reader);
        Task<bool> DeleteAsync(int id);
    }
}