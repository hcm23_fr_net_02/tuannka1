﻿using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Contracts.IService
{
    public interface IReviewService
    {
        Task<IEnumerable<Review>> GetByBookIdAsync();
        Task CreateAsync(Review review);
    }
}