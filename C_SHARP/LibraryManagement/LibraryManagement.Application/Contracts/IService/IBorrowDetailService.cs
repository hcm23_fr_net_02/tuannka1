﻿using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Contracts.IService
{
    public interface IBorrowDetailService
    {
        Task<IEnumerable<BorrowDetail>> GetUnreturnedBooksPagingAsync();
        Task CreateAsync(BorrowDetail borrowDetail);
        Task ReturnAsync(int id);
    }
}