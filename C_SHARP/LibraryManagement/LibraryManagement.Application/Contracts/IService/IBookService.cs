﻿using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Contracts.IService
{
    public interface IBookService
    {
        Task<IEnumerable<Book>> GetPagingAsync();
        Task<IEnumerable<Book>> GetPagingByTitleAsync();
        Task<IEnumerable<Book>> GetPagingByAuthorNameAsync();
        Task<IEnumerable<Book>> GetPagingByGenreNameAsync();
        Task<IEnumerable<Book>> GetPagingByPublicationYearAsync();
        Task<Book> GetDetailByIdAsync(int id);
        Task CreateAsync(Book book);
        Task UpdateAsync(Book book);
        Task<bool> DeleteAsync(int id);
    }
}