﻿using LibraryManagement.Domain.Entities;
using System.Linq.Expressions;

namespace LibraryManagement.Application.Contracts.IRepository
{
    public interface IBookRepository : IRepository<Book>
    {
        Task<Book?> GetDetailByIdAsync(int id);
    }
}