﻿using LibraryManagement.Domain.Utils.Shared;
using System.Linq.Expressions;

namespace LibraryManagement.Application.Contracts.IRepository
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetPagingAsync(Expression<Func<T, bool>>? filter = null, string? includeProperties = null, bool tracked = false, PagingEntity paging = default);
        Task<T?> GetAsync(Expression<Func<T, bool>> filter, string? includeProperties = null, bool tracked = false);
        Task AddAsync(T entity);
        Task RemoveAsync(T entity);
        Task Update(T entity);
        Task<bool> AnyAsync(Expression<Func<T, bool>> filter);
    }
}