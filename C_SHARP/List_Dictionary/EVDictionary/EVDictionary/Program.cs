﻿using EVDictionaryLibrary;
using System.Text;

namespace EVDictionary
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IEVDictionary dictionary = new EVDictionaryLibrary.EVDictionary();

            while (true)
            {
                Console.WriteLine("English-Vietnamese Dictionary");
                Console.WriteLine("1. Add new word");
                Console.WriteLine("2. Search for a word");
                Console.WriteLine("3. Delete a word");
                Console.WriteLine("4. Display all words");
                Console.WriteLine("5. Exit");

                Console.Write("Your choice: ");
                int.TryParse(Console.ReadLine(), out int choice);

                switch (choice)
                {
                    case 1:
                        Console.Write("Enter the English word: ");
                        string englishWord = Console.ReadLine() ?? string.Empty;
                        Console.Write("Enter the Vietnamese meaning: ");
                        string vietnameseMeaning = Console.ReadLine() ?? string.Empty;

                        try
                        {
                            dictionary.AddWord(englishWord, vietnameseMeaning);
                        }
                        catch (InvalidOperationException)
                        {
                            Console.WriteLine("Word already exists in the dictionary.");
                        }
                        break;

                    case 2:
                        Console.Write("Enter word to search: ");
                        string searchWord = Console.ReadLine() ?? string.Empty;
                        try
                        {
                            dictionary.SearchWord(searchWord);
                        }
                        catch (InvalidOperationException)
                        {
                            Console.WriteLine($"No words containing '{searchWord}' found in the dictionary.");
                        }
                        break;

                    case 3:
                        Console.Write("Enter the word to delete: ");
                        string deleteWord = Console.ReadLine() ?? string.Empty;

                        try
                        {
                            dictionary.DeleteWord(deleteWord);
                        }
                        catch (InvalidOperationException)
                        {
                            Console.WriteLine($"'{deleteWord}' does not exist in the dictionary.");
                        }
                        break;

                    case 4:
                        Console.WriteLine();
                        Console.WriteLine("1. Display all words");
                        Console.WriteLine("2. Display only English words");
                        Console.WriteLine("3. Display only Vietnamese meanings");

                        bool isValid = DisplayFormat.TryParse(Console.ReadLine(), out DisplayFormat displayChoice);

                        if (isValid)
                            dictionary.DisplayAllWords(displayChoice);
                       
                        break;

                    case 5:
                        Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine("Invalid choice. Please enter a valid option.");
                        break;
                }

                Console.WriteLine();
            }
        }
    }
}