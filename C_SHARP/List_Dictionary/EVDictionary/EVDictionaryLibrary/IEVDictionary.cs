﻿namespace EVDictionaryLibrary
{
    public interface IEVDictionary
    {
        void AddWord(string englishWord, string vietnameseMeaning);

        void SearchWord(string searchWord);

        void DeleteWord(string deleteWord);

        void DisplayAllWords(DisplayFormat format);
    }
}