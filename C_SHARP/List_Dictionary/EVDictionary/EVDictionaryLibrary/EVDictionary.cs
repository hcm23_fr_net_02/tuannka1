﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVDictionaryLibrary
{
    public class EVDictionary : IEVDictionary
    {
        private Dictionary<string, string> _dictionary = new Dictionary<string, string>();

        public void AddWord(string englishWord, string vietnameseMeaning)
        {
            if (!_dictionary.ContainsKey(englishWord))
            {
                _dictionary.Add(englishWord, vietnameseMeaning);
                Console.WriteLine("Word added successfully.");
            }
            else
            {
                throw new InvalidOperationException($"Word '{englishWord}' already exists in the dictionary.");
            }
        }

        public void DeleteWord(string deleteWord)
        {
            if (_dictionary.Remove(deleteWord))
            {
                Console.WriteLine($"'{deleteWord}' has been deleted from the dictionary.");
            }
            else
            {
                throw new InvalidOperationException($"'{deleteWord}' does not exist in the dictionary.");
            }
        }

        public void DisplayAllWords(DisplayFormat format)
        {
            foreach (var pair in _dictionary)
            {
                switch (format)
                {
                    case DisplayFormat.Both:
                        Console.WriteLine($"English: {pair.Key}, Vietnamese: {pair.Value}");
                        break;
                    case DisplayFormat.English:
                        Console.WriteLine($"English: {pair.Key}");
                        break;
                    default:
                        Console.WriteLine($"Vietnamese: {pair.Value}");
                        break;
                }
            }
        }

        public void SearchWord(string searchWord)
        {
            var foundWords = _dictionary.Keys.Where(key => key.Contains(searchWord, StringComparison.OrdinalIgnoreCase)).ToList();

            if (foundWords.Count > 0)
            {
                Console.WriteLine($"Words containing '{searchWord}':");
                foreach (var word in foundWords)
                {
                    Console.WriteLine($"English: {word}, Vietnamese: {_dictionary[word]}");
                }
            }
            else
            {
                Console.WriteLine($"No words containing '{searchWord}' found in the dictionary.");
            }
        }
    }
}
