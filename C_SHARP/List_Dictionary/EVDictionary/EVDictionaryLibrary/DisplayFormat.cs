﻿namespace EVDictionaryLibrary
{
    public enum DisplayFormat
    {
        Both = 1,
        English = 2,
        Vietnamese = 3
    }
}