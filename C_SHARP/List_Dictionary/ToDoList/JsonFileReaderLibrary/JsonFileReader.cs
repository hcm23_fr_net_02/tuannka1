﻿using System.Text.Json;

namespace JsonFileReaderLibrary
{
    public class JsonFileReader<T> : IJsonFileReader<T>
    {
        public T ReadObjectJson(string fileName)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            string content;
            T? item;
            try
            {
                content = File.ReadAllText(fileName);
                item = JsonSerializer.Deserialize<T>(content, options);
            }
            catch (Exception)
            {
                throw;
            }

            if (item is null)
                throw new NullReferenceException();
            return item;
        }

        public List<T> ReadArrayJson(string fileName)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            string content;
            List<T> items;

            try
            {
                content = File.ReadAllText(fileName);
                items = JsonSerializer.Deserialize<List<T>>(content, options) ?? new List<T>();
            }
            catch (Exception)
            {
                throw;
            }
            return items;
        }

        public void WriteArrayJson(string fileName, List<T> items)
        {
            string json = JsonSerializer.Serialize(items);
            File.WriteAllText(fileName, json);
        }
    }
}
