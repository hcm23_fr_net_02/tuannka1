﻿namespace JsonFileReaderLibrary
{
    public interface IJsonFileReader<T>
    {
        public T ReadObjectJson(string fileName);
        public List<T> ReadArrayJson(string fileName);
        void WriteArrayJson(string fileName, List<T> items);
    }
}