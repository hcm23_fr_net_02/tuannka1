﻿using JsonFileReaderLibrary;
using System.Text.Json;
using TodoManagerLibrary;

namespace ToDoList
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var jsonFileReader = new JsonFileReader<Todo>();
            var fileName = "list-item.json";

            TodoManager todoManager = new TodoManager(jsonFileReader, fileName);

            while (true)
            {
                Console.WriteLine("Todo List Application");
                Console.WriteLine("1. Display todos");
                Console.WriteLine("2. Add a new todo");
                Console.WriteLine("3. Delete a todo");
                Console.WriteLine("4. Mark a todo as completed");
                Console.WriteLine("5. Exit");
                Console.Write("Enter your choice: ");

                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            todoManager.DisplayTodos();
                            break;
                        case 2:
                            Console.Write("Enter Task Title: ");
                            string taskTitle = Console.ReadLine() ?? string.Empty;
                            var item = new Todo(taskTitle);
                            todoManager.AddTodo(item);
                            break;
                        case 3:
                            Console.Write("Enter Task ID: ");
                            string removeTaskId = Console.ReadLine() ?? string.Empty;
                            try
                            {
                                todoManager.DeleteTodo(removeTaskId);
                            }
                            catch (InvalidOperationException)
                            {
                                Console.WriteLine($"Task {removeTaskId} does not exist.");
                            }
                            break;
                        case 4:
                            Console.Write("Enter Task ID: ");
                            string updateTaskId = Console.ReadLine() ?? string.Empty;
                            try
                            {
                                todoManager.MarkAsCompleted(updateTaskId);
                            }
                            catch (InvalidOperationException)
                            {
                                Console.WriteLine($"Task {updateTaskId} does not exist.");
                            }
                            break;
                        case 5:
                            todoManager.SaveData();
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Invalid choice. Please try again.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input. Please enter a valid choice.");
                }
                Console.WriteLine();
            }
        }
    }
}