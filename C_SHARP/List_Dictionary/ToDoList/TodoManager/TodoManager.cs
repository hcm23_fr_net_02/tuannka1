﻿using JsonFileReaderLibrary;

namespace TodoManagerLibrary
{
    public class TodoManager
    {
        private List<Todo> _todos = new List<Todo>();
        private readonly IJsonFileReader<Todo> _reader;
        private string _fileName;

        public TodoManager(IJsonFileReader<Todo> jsonFileReader, string fileName)
        {
            _reader = jsonFileReader;
            _fileName = fileName;

            LoadData();
        }

        public void DisplayTodos()
        {
            Console.WriteLine("Todo List:");
            foreach (var todo in _todos)
            {
                Console.WriteLine($"ID: {todo.Id}, Title: {todo.TaskTitle}, Completed: {todo.IsComplete}");
            }
        }

        public void AddTodo(Todo item)
        {
            _todos.Add(item);
            Console.WriteLine("New task added successfully.");
        }

        public void DeleteTodo(string id)
        {
            Todo? todoToDelete = _todos.Find(t => t.Id == id);
            if (todoToDelete is null)
            {
                throw new InvalidOperationException();
            }

            _todos.Remove(todoToDelete);
            Console.WriteLine($"Task {id} deleted successfully.");

        }

        public void MarkAsCompleted(string id)
        {
            Todo? todoToMark = _todos.Find(t => t.Id == id);
            if (todoToMark is null)
            {
                throw new InvalidOperationException();
            }

            todoToMark.IsComplete = true;
            Console.WriteLine($"Task {id} marked as completed.");
        }


        public void ReLoadDataFromAnotherFileName(string fileName)
        {
            _fileName = fileName;
            LoadData();
        }

        public void SaveData()
        {
            _reader.WriteArrayJson(_fileName, _todos);
        }

        private void LoadData()
        {
            _todos = _reader.ReadArrayJson(_fileName);
        }
    }
}
