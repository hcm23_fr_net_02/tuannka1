﻿namespace TodoManagerLibrary
{
    public class Todo
    {
        public DateTime CreatedAt { get; set; }
        public string TaskTitle { get; set; }
        public bool IsComplete { get; set; }
        public string Id { get; set; }

        public override string ToString() => TaskTitle;

        public Todo(string taskTitle)
        {
            Id = Guid.NewGuid().ToString();
            TaskTitle = taskTitle;
            CreatedAt = DateTime.Now;
            IsComplete = false;
        }
    }
}