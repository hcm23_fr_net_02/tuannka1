﻿namespace StudentManagerLibrary
{
    public class StudentManager : IStudentManager
    {
        private List<Student> _students;

        public StudentManager()
        {
            _students = new List<Student>();
        }

        public int Count() => _students.Count;

        public void AddStudent(Student student)
        {
            _students.Add(student);
            Console.WriteLine($"Added {student.Name} to the student list.");
        }

        public void RemoveStudent(string name)
        {
            var studentToRemove = _students.Find(student => student.Name == name);

            if (studentToRemove is not null)
            {
                _students.Remove(studentToRemove!);
                Console.WriteLine($"Removed {studentToRemove!.Name} from the student list.");
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void DisplayStudents()
        {
            Console.WriteLine("Student List:");
            foreach (var student in _students)
            {
                Console.WriteLine($"Name: {student.Name}, Age: {student.Age}, GPA: {student.GPA}");
            }
        }

        public Student FindStudentByName(string name)
        {
            var student = _students.Find(student => student.Name == name);
            if (student is null)
                throw new InvalidOperationException();
            return student!;

        }
    }
}
