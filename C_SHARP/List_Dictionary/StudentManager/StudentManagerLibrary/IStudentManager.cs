﻿namespace StudentManagerLibrary
{
    public interface IStudentManager
    {
        int Count();
        void AddStudent(Student student);
        void RemoveStudent(string name);
        void DisplayStudents();
        Student FindStudentByName(string name);
    }
}