﻿using StudentManagerLibrary;

StudentManager studentManager = new StudentManager();

while (true)
{
    Console.WriteLine("\nStudent Management System");
    Console.WriteLine("1. Add Student");
    Console.WriteLine("2. Remove Student");
    Console.WriteLine("3. Display Students");
    Console.WriteLine("4. Find Student by Name");
    Console.WriteLine("5. Exit");

    var choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            Console.Write("Enter student name: ");
            string name = Console.ReadLine() ?? string.Empty;
            Console.Write("Enter student age: ");
            int.TryParse(Console.ReadLine(), out int age);
            Console.Write("Enter student GPA: ");
            double.TryParse(Console.ReadLine(), out double gpa);

            Student student = new Student { Name = name, Age= age, GPA= gpa };
            studentManager.AddStudent(student);
            break;
        case "2":
            Console.Write("Enter the name of the student to remove: ");
            string studentNameToRemove = Console.ReadLine() ?? string.Empty;
            studentManager.RemoveStudent(studentNameToRemove);
            break;
        case "3":
            studentManager.DisplayStudents();
            break;
        case "4":
            Console.Write("Enter the name to search for: ");
            string searchName = Console.ReadLine() ?? string.Empty;
            Student foundStudent = studentManager.FindStudentByName(searchName);

            Console.WriteLine($"Found student with the name {searchName}:");
            try
            {
                Console.WriteLine($"Name: {foundStudent.Name}, Age: {foundStudent.Age}, GPA: {foundStudent.GPA}");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine($"No student found with the name {searchName}.");
            }
            break;
        case "5":
            return;
        default:
            Console.WriteLine("Invalid choice. Please enter a valid option.");
            break;
    }
}