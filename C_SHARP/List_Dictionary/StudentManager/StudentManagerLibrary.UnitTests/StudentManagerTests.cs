namespace StudentManagerLibrary.UnitTests
{
    public class StudentManagerTests
    {
        [Fact]
        public void Count_ShouldReturn4_WhenOnlyAdd3Student()
        {
            //Arrange
            IStudentManager studentManager = new StudentManager();
            var student1 = new Student { Name = "Rick", Age = 22, GPA = 3.6 };
            var student2 = new Student { Name = "Bill", Age = 21, GPA = 3.1 };
            var student3 = new Student { Name = "Hillary", Age = 20, GPA = 3.2 };
            var expected = 3;

            studentManager.AddStudent(student1);
            studentManager.AddStudent(student2);
            studentManager.AddStudent(student3);

            //Action
            var result = studentManager.Count();


            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void AddStudent_ShouldAddIntoList_WhenParamValid()
        {
            //Arrange
            IStudentManager studentManager = new StudentManager();
            var param = new Student { Name = "Rick", Age = 22, GPA = 3.6 };
            var expected = 1;

            //Action
            studentManager.AddStudent(param);

            //Assert
            Assert.Equal(expected, studentManager.Count());
        }

        [Fact]
        public void RemoveStudent_ShouldThrowException_WhenRemoveStudentNotExist()
        {
            //Arrange
            IStudentManager studentManager = new StudentManager();
            var student1 = new Student { Name = "Rick", Age = 22, GPA = 3.6 };
            var student2 = new Student { Name = "Bill", Age = 21, GPA = 3.1 };
            var student3 = new Student { Name = "Hillary", Age = 20, GPA = 3.2 };

            var name = "Linda";

            studentManager.AddStudent(student1);
            studentManager.AddStudent(student2);
            studentManager.AddStudent(student3);

            //Action & Assert
            Assert.Throws<InvalidOperationException>(() => studentManager.RemoveStudent(name));
        }

        [Fact]
        public void RemoveStudent_ShouldCountReduceBy1_WhenRemoveStudentSuccess()
        {
            //Arrange
            IStudentManager studentManager = new StudentManager();
            var student1 = new Student { Name = "Rick", Age = 22, GPA = 3.6 };
            var student2 = new Student { Name = "Bill", Age = 21, GPA = 3.1 };
            var student3 = new Student { Name = "Hillary", Age = 20, GPA = 3.2 };
            var expected = 2;

            var name = "Bill";

            studentManager.AddStudent(student1);
            studentManager.AddStudent(student2);
            studentManager.AddStudent(student3);

            studentManager.RemoveStudent(name);

            //Action
            var result = studentManager.Count();


            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void DisplayStudents_ShouldWriteOnConsole_WhenCall()
        {
            //Arrange
            IStudentManager studentManager = new StudentManager();
            var student = new Student { Name = "Rick", Age = 22, GPA = 3.6 };
            var expectedSubstring = "Student List:";

            studentManager.AddStudent(student);

            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            //Action
            studentManager.DisplayStudents();

            //Assert
            Assert.Contains(expectedSubstring, stringWriter.ToString());

        }

        [Fact]
        public void FindStudent_ShouldThrowException_WhenFindStudentNotExist()
        {
            //Arrange
            IStudentManager studentManager = new StudentManager();
            var student1 = new Student { Name = "Rick", Age = 22, GPA = 3.6 };
            var student2 = new Student { Name = "Bill", Age = 21, GPA = 3.1 };
            var student3 = new Student { Name = "Hillary", Age = 20, GPA = 3.2 };

            var name = "Linda";

            studentManager.AddStudent(student1);
            studentManager.AddStudent(student2);
            studentManager.AddStudent(student3);

            //Action & Assert
            Assert.Throws<InvalidOperationException>(() => studentManager.FindStudentByName(name));
        }


        [Fact]
        public void FindStudent_ShouldReturnStudent_WhenFindStudentExist()
        {
            //Arrange
            IStudentManager studentManager = new StudentManager();
            var student1 = new Student { Name = "Rick", Age = 22, GPA = 3.6 };
            var student2 = new Student { Name = "Bill", Age = 21, GPA = 3.1 };
            var student3 = new Student { Name = "Hillary", Age = 20, GPA = 3.2 };

            var name = "Rick";

            studentManager.AddStudent(student1);
            studentManager.AddStudent(student2);
            studentManager.AddStudent(student3);

            //Action
            var result = studentManager.FindStudentByName(name);

            //Assert
            Assert.Equal(student1, result);
        }
    }
}