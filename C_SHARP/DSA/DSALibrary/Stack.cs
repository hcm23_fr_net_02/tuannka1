﻿namespace DSALibrary
{
    public class Stack<T> : IStack<T>
    {
        private const int _defaultCapacity = 5;

        private T[] _array;
        private int _size;

        public int Count => _size;

        public Stack()
        {
            _array = new T[_defaultCapacity];
            _size = 0;
        }

        public T Peek()
        {
            if (_size == 0)
            {
                throw new InvalidOperationException("Stack Underflow");
            }
            return _array[_size - 1];
        }

        public T Pop()
        {
            if (_size == 0)
            {
                throw new InvalidOperationException("Stack Underflow");
            }
            return _array[--_size];
        }

        public void Push(T item)
        {
            if (_size == _array.Length)
            {
                T[] newArray = new T[_size * 2];
                Array.Copy(_array, newArray, _size);
                _array = newArray;
            }
            _array[_size++] = item;
        }
    }
}