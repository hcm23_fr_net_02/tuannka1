﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSALibrary
{
    public class Queue<T> : IQueue<T>
    {
        private const int _defaultCapacity = 5;

        private T[] _array;
        private int _front;
        private int _rear;
        private int _size;

        public int Count => _size;
        public Queue()
        {
            _array = new T[_defaultCapacity];
            _front = 0;
            _rear = -1;
            _size = 0;
        }
        public T Dequeue()
        {
            if (_size == 0)
            {
                throw new OutOfRangeException()
                throw new InvalidOperationException("Underflow");
            }
            T item = _array[_front];
            _front = (_front + 1) % _array.Length; //incase _front is at the last position in the array
            _size--;
            return item;
        }

        public void Enqueue(T item)
        {
            if (_size == _array.Length)
            {
                ResizeQueue();
            }
            _rear = (_rear + 1) % _array.Length; //incase _rear is at the last position in the array
            _array[_rear] = item;
            _size++;
        }

        private void ResizeQueue()
        {
            T[] newArray = new T[_size * 2];
            if (_front <= _rear)
            {
                Array.Copy(_array, _front, newArray, 0, _size);
            }
            else
            {
                Array.Copy(_array, _front, newArray, 0, _size - _front);
                Array.Copy(_array, 0, newArray, _size - _front, _rear + 1);
            }
            _array = newArray;
            _front = 0;
            _rear = _size - 1;
        }
    }
}
