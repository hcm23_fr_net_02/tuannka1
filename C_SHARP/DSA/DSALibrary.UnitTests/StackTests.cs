using DSALibrary;

namespace DSALibrary.UnitTests
{
    [TestFixture]
    public class StackTests
    {
        [Test]
        public void Pop_EmptyStack_ThrowInvalidOperationException()
        {
            // Arrange
            var stack = new Stack<int>();

            // Act and Assert
            Assert.That(() => stack.Pop(), Throws.InvalidOperationException);
        }

        [Test]
        public void Pop_StackWithObjects_ReturnObjectOnTheTop()
        {
            // Arrange 
            var stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            // Act
            var result = stack.Pop();

            // Assert
            Assert.That(result, Is.EqualTo(3));
        }

        [Test]
        public void Pop_StackWithObjects_RemoveObjectOnTheTop()
        {
            // Arrange 
            var stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            // Act
            stack.Pop();

            // Assert
            Assert.That(stack.Count, Is.EqualTo(2));
        }

        [Test]
        public void Peek_EmptyStack_ThrowInvalidOperationException()
        {
            var stack = new Stack<int>();

            Assert.That(() => stack.Peek(), Throws.InvalidOperationException);
        }

        [Test]
        public void Peek_StackWithObjects_ReturnObjectOnTopOfTheStack()
        {
            // Arrange 
            var stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            // Act
            var result = stack.Peek();

            // Assert
            Assert.That(result, Is.EqualTo(3));
        }

        [Test]
        public void Peek_StackWithObjects_DoesNotRemoveTheObjectOnTopOfTheStack()
        {
            // Arrange 
            var stack = new Stack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            // Act
            stack.Peek();

            // Assert
            Assert.That(stack.Count, Is.EqualTo(3));
        }
    }
}