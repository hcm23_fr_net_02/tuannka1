﻿namespace DSALibrary.UnitTests
{
    [TestFixture]
    public class QueueTests
    {
        [Test]
        public void Enqueue_EmptyQueue_InsertedIntoQueue()
        {
            // Arrange
            var queue = new Queue<int>();

            // Act
            queue.Enqueue(15);

            // Assert
            Assert.That(queue.Count, Is.EqualTo(1));
            Assert.That(queue.Dequeue(), Is.EqualTo(15));
        }

        [Test]
        public void Enqueue_QueueButNotFull_ResizeQueueAndInsert()
        {
            // Arrange
            var queue = new Queue<int>();
            queue.Enqueue(15);

            // Act
            queue.Enqueue(20);

            // Assert
            Assert.That(queue.Count, Is.EqualTo(2));
        }

        [Test]
        public void Dequeue_EmptyQueue_ThrowInvalidOperationException()
        {
            // Arrange
            var queue = new Queue<int>();

            // Act and Assert
            Assert.Throws<InvalidOperationException>(() => queue.Dequeue());
        }

        [Test]
        public void Dequeue_QueueWithObjects_ReturnObjectAtFront()
        {
            // Arrange
            var queue = new Queue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            // Act
            var result = queue.Dequeue();

            // Assert
            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        public void Dequeue_QueueWithObjects_RemoveObjectOnTheTop()
        {
            // Arrange
            var queue = new Queue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            // Act
            queue.Dequeue();

            // Assert
            Assert.That(queue.Count, Is.EqualTo(2));
        }
    }
}
