﻿using LibraryManagement.Application.Contracts.IRepositories;
using LibraryManagement.Application.Contracts.IServices;
using LibraryManagement.Application.Services;
using LibraryManagement.ConsoleApplication;
using LibraryManagement.ConsoleApplication.Contracts;
using LibraryManagement.ConsoleApplication.Presentations;
using LibraryManagement.Infrastructure;
using LibraryManagement.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

IHostBuilder hostBuider = Host.CreateDefaultBuilder();

hostBuider.ConfigureServices((hostContext, services) =>
{
    services.AddDbContext<ApplicationDbContext>(options =>
        options
            .UseSqlServer(hostContext.Configuration.GetConnectionString("DefaultConnection"))
            .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole().SetMinimumLevel(LogLevel.None)))
    );

    services.AddScoped<Application>();

    services.AddScoped<IUnitOfWork, UnitOfWork>();

    services.AddScoped<IBookService, BookService>();
    services.AddScoped<ICustomerService, CustomerService>();
    services.AddScoped<ILoanService, LoanService>();

    services.AddScoped<IBookPresentation, BookPresentation>();
    services.AddScoped<ILoanPresentation, LoanPresentation>();
});

IHost host = hostBuider.Build();

var app = host.Services.GetRequiredService<Application>();

await app.RunAsync();