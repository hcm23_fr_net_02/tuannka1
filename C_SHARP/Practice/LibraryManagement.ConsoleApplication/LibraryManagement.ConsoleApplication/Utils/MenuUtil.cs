﻿namespace LibraryManagement.ConsoleApplication.Utils
{
    internal class MenuUtil
    {
        internal static async Task ShowMenuAsync(string menuName, Dictionary<string, Func<Task>> options)
        {
            int menuChoice = -1;

            do
            {
                //show menu
                Console.WriteLine($"\n:::{menuName}:::");

                var optionsWithIndex = options.Select((option, index) => (option, index));
                foreach (var (option, index) in optionsWithIndex)
                {
                    Console.WriteLine($"{index + 1}. {option.Key}");
                }

                // Option for exit
                Console.WriteLine("0. Exit");

                do
                {
                    Console.Write("Your choice: ");
                } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                var choiceOption = optionsWithIndex.FirstOrDefault(opt => opt.index + 1 == menuChoice).option;
                Func<Task>? choiceAction = choiceOption.Value;

                if (choiceAction is not null)
                {
                    await choiceAction.Invoke();
                }
            } while (menuChoice != 0);
        }
    }
}