﻿using LibraryManagement.ConsoleApplication.Presentations;

namespace LibraryManagement.ConsoleApplication.Contracts
{
    public interface IBookPresentation
    {
        Task AddBookAsync();
        Task ViewBooksAsync();
        Task SearchBookByIdAsync();
        Task DeleteBookAsync();
    }
}