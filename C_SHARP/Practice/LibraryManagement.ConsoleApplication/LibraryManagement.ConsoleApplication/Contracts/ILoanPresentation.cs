﻿using LibraryManagement.ConsoleApplication.Presentations;

namespace LibraryManagement.ConsoleApplication.Contracts
{
    public interface ILoanPresentation
    {
        Task AddLoanAsync();
        Task ViewLoansAsync();
    }
}