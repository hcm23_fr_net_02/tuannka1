﻿using Azure;
using LibraryManagement.Application.Contracts.IServices;
using LibraryManagement.ConsoleApplication.Contracts;
using LibraryManagement.ConsoleApplication.Utils;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Shared.Common;
using Microsoft.Extensions.Options;

namespace LibraryManagement.ConsoleApplication.Presentations
{
    public class BookPresentation : IBookPresentation
    {
        private readonly IBookService _bookService;

        public BookPresentation(IBookService bookService)
        {
            _bookService = bookService;
        }

        public async Task AddBookAsync()
        {
            try
            {
                //create an object of Book
                Book book = new Book();

                //read all details from the user
                Console.WriteLine("\n***Add Book***");

                book.Title = ConsoleUtil.ReadString("Title: ");
                book.AuthorName = ConsoleUtil.ReadString("Author Name: ");
                book.Quantity = ConsoleUtil.ReadUShort("Quantity: ");
                book.Price = ConsoleUtil.ReadFloat("Price: ");

                await _bookService.CreateAsync(book);

                Console.WriteLine("Book added.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Book not added.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task DeleteBookAsync()
        {
            try
            {
                Console.WriteLine("\n***Delete Book***");

                int bookIdToDelete = ConsoleUtil.ReadInt("Book Id: ");

                bool isExist = await _bookService.DeleteAsync(bookIdToDelete);

                if (!isExist)
                {
                    Console.WriteLine($"Book id: {bookIdToDelete} not existed");
                    return;
                }

                Console.WriteLine("Book deleted.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Book not deleted.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task SearchBookByIdAsync()
        {
            try
            {
                Console.WriteLine("\n***Search Book by Id***");

                int bookIdToSearch = ConsoleUtil.ReadInt("Book Id: ");

                Book? book = await _bookService.GetByIdAsync(bookIdToSearch);

                if (book is null)
                {
                    Console.WriteLine($"Book id: {bookIdToSearch} not existed");
                    return;
                }

                Console.WriteLine(book);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Book not deleted.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task ViewBooksAsync()
        {
            try
            {
                var menuName = "\n***View Books***";
                var menuChoice = -1;
                var paging = new Paging { NumberPerPage = 2, PageNumber = 0 };
                var lastPage = -1;

                do
                {
                    //show menu
                    Console.WriteLine($"\n:::{menuName}:::");

                    Console.WriteLine($"Current Page: {paging.PageNumber}");

                    IEnumerable<Book> books = await _bookService.GetPagingAsync(paging);

                    foreach(var book in books)
                    {
                        Console.WriteLine(book);
                    }

                    if (books.Count() == 0)
                    {
                        lastPage = paging.PageNumber;
                        Console.WriteLine("This is final page.");
                    }

                    Console.WriteLine("1. Back");
                    Console.WriteLine("2. Next");

                    // Option for exit
                    Console.WriteLine("0. Exit");

                    do
                    {
                        Console.Write("Your choice: ");
                    } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                    switch (menuChoice)
                    {
                        case 1:
                            if (lastPage == 0)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber--;
                            break;
                        case 2:
                            if (lastPage == 0 || lastPage == paging.PageNumber)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber++;
                            break;
                    }
                } while (menuChoice != 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }
    }
}