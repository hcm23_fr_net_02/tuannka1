﻿using LibraryManagement.Application.Contracts.IServices;
using LibraryManagement.Application.Services;
using LibraryManagement.ConsoleApplication.Contracts;
using LibraryManagement.ConsoleApplication.Utils;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Shared.Common;

namespace LibraryManagement.ConsoleApplication.Presentations
{
    public class LoanPresentation : ILoanPresentation
    {
        private readonly ILoanService _loanService;

        public LoanPresentation(ILoanService loanService)
        {
            _loanService = loanService;
        }

        public async Task AddLoanAsync()
        {
            try
            {
                //create an object of Loan
                Loan loan = new Loan();

                //read all details from the user
                Console.WriteLine("\n***Add Loan***");

                loan.BookId = ConsoleUtil.ReadInt("Book Id: ");
                loan.CustomerId = ConsoleUtil.ReadInt("Customer Id: ");
                loan.BorrowedDate = DateTime.Now;
                Console.WriteLine($"Borrowed Date: {loan.BorrowedDate.ToShortDateString()}");
                loan.ReturnDate = ConsoleUtil.ReadDateTime("Return Date (mm/dd/yyyy): ");

                await _loanService.CreateAsync(loan);

                Console.WriteLine("Loan added.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Loan not added.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task ViewLoansAsync()
        {
            try
            {
                var menuName = "\n***View Loans***";
                var menuChoice = -1;
                var paging = new Paging { NumberPerPage = 2, PageNumber = 0 };
                var lastPage = -1;

                do
                {
                    //show menu
                    Console.WriteLine($"\n:::{menuName}:::");

                    Console.WriteLine($"Current Page: {paging.PageNumber}");

                    IEnumerable<Loan> loans = await _loanService.GetPagingAsync(paging);

                    foreach (var loan in loans)
                    {
                        Console.WriteLine(loan);
                    }

                    if (loans.Count() == 0)
                    {
                        lastPage = paging.PageNumber;
                        //paging.PageNumber--;
                        Console.WriteLine("This is final page.");
                    }

                    Console.WriteLine("1. Back");
                    Console.WriteLine("2. Next");

                    // Option for exit
                    Console.WriteLine("0. Exit");

                    do
                    {
                        Console.Write("Your choice: ");
                    } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                    switch (menuChoice)
                    {
                        case 1:
                            if (lastPage == 0)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber--;
                            break;
                        case 2:
                            if (lastPage == 0 || lastPage == paging.PageNumber)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber++;
                            break;
                    }
                } while (menuChoice != 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }
    }
}