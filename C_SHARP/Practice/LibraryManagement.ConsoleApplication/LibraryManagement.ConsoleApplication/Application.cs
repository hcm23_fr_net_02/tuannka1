﻿using LibraryManagement.Application.Contracts.IRepositories;
using LibraryManagement.ConsoleApplication.Contracts;
using LibraryManagement.ConsoleApplication.Presentations;
using LibraryManagement.ConsoleApplication.Utils;

namespace LibraryManagement.ConsoleApplication
{
    public class Application
    {
        private readonly IBookPresentation _bookPresentation;
        private readonly ILoanPresentation _loanPresentation;

        public Application(IBookPresentation bookPresentation, ILoanPresentation loanPresentation)
        {
            _bookPresentation = bookPresentation;
            _loanPresentation = loanPresentation;
        }

        public async Task RunAsync()
        {
            var applicationName = "Library Management Application";
            Console.WriteLine($"***{applicationName}***");

            var menuName = "Main Menu";
            var options = new Dictionary<string, Func<Task>>()
            {
                { "Add book", _bookPresentation.AddBookAsync },
                { "Delete book", _bookPresentation.DeleteBookAsync },
                { "View books", _bookPresentation.ViewBooksAsync },
                { "Add Loan", _loanPresentation.AddLoanAsync },
                { "View loans", _loanPresentation.ViewLoansAsync },
                { "Search book by Id", _bookPresentation.SearchBookByIdAsync },
            };

            await MenuUtil.ShowMenuAsync(menuName, options);

            //Exit 
            Console.WriteLine("\nSee you again");
        }
    }
}