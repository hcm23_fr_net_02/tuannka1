﻿using System.Text;
using System.Xml.Linq;

namespace LibraryManagement.Domain.Entities
{
    public class Book
    {
        #region Private fields
        private int _id;
        private string _title;
        private string _authorName;
        private ushort _quantity;
        private float _price;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Title
        {
            get => _title;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new Exception($"Book title should not be null, whitespace or greater than 255 characters long.");
                }
                _title = value;
            }
        }

        public string AuthorName
        {
            get => _authorName;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new Exception($"Author Name should not be null, whitespace or greater than 255 characters long.");
                }
                _authorName = value;
            }
        }

        public ushort Quantity
        {
            get => _quantity;
            set
            {
                if (value < 0)
                {
                    throw new Exception($"Quantity should be greater than or equal to 0.");
                }
                _quantity = value;
            }
        }

        public float Price
        {
            get => _price;
            set
            {
                if (value <= 0)
                    throw new Exception($"Price should be greater than 0.");
                _price = value;
            }
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tBook Id: {Id}")
                .AppendLine($"\tTitle: {Title}")
                .AppendLine($"\tAuthor name: {AuthorName}")
                .AppendLine($"\tQuantity: {Quantity}")
                .AppendLine($"\tPrice: {Price}");

            return stringBuilder.ToString();
        }
        #endregion
    }
}