﻿using System.Diagnostics;
using System.Reflection.PortableExecutable;
using System.Text;

namespace LibraryManagement.Domain.Entities
{
    public class Loan
    {
        #region Private fields
        private int _id;
        private DateTime _borrowedDate;
        private DateTime _returnDate;
        private int _bookId;
        private int _customerId;
        private Book? _book;
        private Customer? _customer;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }
        public DateTime BorrowedDate
        {
            get => _borrowedDate;
            set => _borrowedDate = value;
        }

        public DateTime ReturnDate
        {
            get => _returnDate;
            set => _returnDate = value;
        }

        public int BookId
        {
            get => _bookId;
            set => _bookId = value;
        }

        public int CustomerId
        {
            get => _customerId;
            set => _customerId = value;
        }

        public Book? Book
        {
            get => _book;
            set => _book = value;
        }

        public Customer? Customer
        {
            get => _customer;
            set => _customer = value;
        }
        #endregion


        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tLoan Id: {Id}")
                .AppendLine($"\tBook Id: {BookId}")
                .AppendLine($"\tCustomer Id: {CustomerId}")
                .AppendLine($"\tBorrowed Date: {BorrowedDate}")
                .AppendLine($"\tReturn Date: {ReturnDate}");

            if (Customer is not null)
            {
                stringBuilder
                    .AppendLine($"\tCustomer Name: {Customer.Name}");
            }

            return stringBuilder.ToString();
        }
        #endregion
    }
}