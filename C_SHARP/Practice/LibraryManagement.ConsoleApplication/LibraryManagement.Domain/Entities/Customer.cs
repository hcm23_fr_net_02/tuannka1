﻿using System.Text;

namespace LibraryManagement.Domain.Entities
{
    public class Customer
    {
        #region Private fields
        private int _id;
        private string _name;
        private string? _address;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new Exception($"Customer name should not be null, whitespace or greater than 255 characters long.");
                }
                _name = value;
            }
        }

        public string? Address
        {
            get => _address;
            set => _address = value;
        }
        #endregion
    }
}