﻿using LibraryManagement.Application.Contracts.IRepositories;
using LibraryManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.Infrastructure.Repositories
{
    public class BookRepository : Repository<Book>, IBookRepository
    {
        private readonly ApplicationDbContext _db;

        public BookRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public async Task<Book?> GetDetailByIdAsync(int id, bool tracked = false)
        {
            IQueryable<Book> query;
            DbSet<Book> dbSet = _db.Set<Book>();

            if (tracked)
            {
                query = dbSet;
            }
            else
            {
                query = dbSet.AsNoTracking();
            }

            return await query.FirstOrDefaultAsync();
        }
    }
}