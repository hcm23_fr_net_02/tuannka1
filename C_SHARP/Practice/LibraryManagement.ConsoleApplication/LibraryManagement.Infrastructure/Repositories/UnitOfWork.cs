﻿using LibraryManagement.Application.Contracts.IRepositories;

namespace LibraryManagement.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _db;
        public IBookRepository Book { get; private set; }

        public ICustomerRepository Customer { get; private set; }

        public ILoanRepository Loan { get; private set; }

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            Book = new BookRepository(_db);
            Customer = new CustomerRepository(_db);
            Loan = new LoanRepository(_db);
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}