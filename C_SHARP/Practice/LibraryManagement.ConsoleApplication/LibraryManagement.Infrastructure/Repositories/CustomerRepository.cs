﻿using LibraryManagement.Application.Contracts.IRepositories;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Infrastructure.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        private readonly ApplicationDbContext _db;

        public CustomerRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}