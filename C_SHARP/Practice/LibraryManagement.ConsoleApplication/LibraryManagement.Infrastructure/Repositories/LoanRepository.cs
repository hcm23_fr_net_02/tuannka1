﻿using LibraryManagement.Application.Contracts.IRepositories;
using LibraryManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Linq.Expressions;

namespace LibraryManagement.Infrastructure.Repositories
{
    public class LoanRepository : Repository<Loan>, ILoanRepository
    {
        private readonly ApplicationDbContext _db;

        public LoanRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public async Task<int> CountAsync(Expression<Func<Loan, bool>> filter)
        {
            IQueryable<Loan> query = _db.Set<Loan>();

            return await query.Where(filter).CountAsync();
        }
    }
}