﻿using LibraryManagement.Domain.Entities;
using LibraryManagement.Infrastructure.TypeConfiguration;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.Infrastructure
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BookConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new LoanConfiguration());
        }
    }
}