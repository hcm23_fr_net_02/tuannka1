﻿using LibraryManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LibraryManagement.Infrastructure.TypeConfiguration
{
    internal class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> modelBuilder)
        {
            modelBuilder.Property(b => b.Title).HasMaxLength(255);

            modelBuilder.Property(b => b.AuthorName).HasMaxLength(255);

            modelBuilder.HasData
            (
                new Book
                {
                    Id = 1,
                    Title = "Harry Potter",
                    AuthorName = "J.K. Rowling",
                    Price = 100,
                    Quantity = 4
                }
            );
        }
    }
}