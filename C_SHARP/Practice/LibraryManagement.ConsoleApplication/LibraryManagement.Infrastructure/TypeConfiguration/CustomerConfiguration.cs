﻿using LibraryManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LibraryManagement.Infrastructure.TypeConfiguration
{
    internal class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> modelBuilder)
        {
            modelBuilder.Property(b => b.Name).HasMaxLength(255);

            modelBuilder.HasData
            (
                new Customer
                {
                    Id = 1,
                    Name = "Rick",
                    Address = "UK",
                }
            );
        }
    }
}