﻿using LibraryManagement.Application.Contracts.IRepositories;
using LibraryManagement.Application.Contracts.IServices;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Shared.Common;

namespace LibraryManagement.Application.Services
{
    public class BookService : IBookService
    {
        #region Private Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public BookService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Create a new book.
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public async Task CreateAsync(Book book)
        {
            try
            {
                await _unitOfWork.Book.AddAsync(book);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                Book? objFromRepo = await _unitOfWork.Book.GetAsync(u => u.Id == id);
                if (objFromRepo is null)
                {
                    return false;
                }

                await _unitOfWork.Book.RemoveAsync(objFromRepo);
                await _unitOfWork.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Book?> GetByIdAsync(int id)
        {
            try
            {
                return await _unitOfWork.Book.GetAsync(b => b.Id == id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Book>> GetPagingAsync(Paging paging)
        {
            return await _unitOfWork.Book.GetPagingAsync(paging: paging);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public Task UpdateAsync(Book book)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

}
