﻿using LibraryManagement.Application.Contracts.IRepositories;
using LibraryManagement.Application.Contracts.IServices;

namespace LibraryManagement.Application.Services
{
    public class CustomerService : ICustomerService
    {
        #region Private Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
