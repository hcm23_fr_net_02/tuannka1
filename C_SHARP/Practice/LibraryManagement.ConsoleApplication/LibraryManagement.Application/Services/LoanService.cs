﻿using LibraryManagement.Application.Common;
using LibraryManagement.Application.Contracts.IRepositories;
using LibraryManagement.Application.Contracts.IServices;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Shared.Common;

namespace LibraryManagement.Application.Services
{
    public class LoanService : ILoanService
    {
        #region Private Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public LoanService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Public Methods
        public async Task CreateAsync(Loan loan)
        {
            try
            {
                Book? book = await _unitOfWork.Book.GetAsync(b => b.Id == loan.BookId);

                if (book is null)
                {
                    throw new BusinessException($"Book Id: {loan.BookId} not exist.");
                }

                Customer? customer = await _unitOfWork.Customer.GetAsync(c => c.Id == loan.CustomerId);

                if (customer is null)
                {
                    throw new BusinessException($"Customer Id: {loan.CustomerId} not exist.");
                }

                int totalBorrowedBook = await _unitOfWork.Loan.CountAsync(l => l.BookId == loan.BookId);

                if (book.Quantity <= totalBorrowedBook)
                {
                    throw new BusinessException($"Book Id: {loan.BookId} not available.");
                }

                await _unitOfWork.Loan.AddAsync(loan);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Loan>> GetPagingAsync(Paging paging)
        {
            return await _unitOfWork.Loan.GetPagingAsync(paging: paging, includeProperties: nameof(Loan.Customer));
        }
        #endregion
    }
}
