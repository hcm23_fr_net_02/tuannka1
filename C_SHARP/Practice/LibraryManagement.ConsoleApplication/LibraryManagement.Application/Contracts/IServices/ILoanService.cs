﻿using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Shared.Common;

namespace LibraryManagement.Application.Contracts.IServices
{
    public interface ILoanService
    {
        Task<IEnumerable<Loan>> GetPagingAsync(Paging paging);
        Task CreateAsync(Loan loan);
    }
}