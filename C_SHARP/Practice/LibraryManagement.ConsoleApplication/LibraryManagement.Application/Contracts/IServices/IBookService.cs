﻿using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Shared.Common;

namespace LibraryManagement.Application.Contracts.IServices
{
    public interface IBookService
    {
        Task<IEnumerable<Book>> GetPagingAsync(Paging paging);
        Task<Book?> GetByIdAsync(int id);
        Task CreateAsync(Book book);
        Task UpdateAsync(Book book);
        Task<bool> DeleteAsync(int id);
    }
}