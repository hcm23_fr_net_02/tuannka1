﻿using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Contracts.IRepositories
{
    public interface IBookRepository : IRepository<Book>
    {
        Task<Book?> GetDetailByIdAsync(int id, bool tracked = false);
    }
}