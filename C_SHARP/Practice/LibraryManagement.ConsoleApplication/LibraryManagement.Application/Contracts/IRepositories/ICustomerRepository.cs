﻿using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Contracts.IRepositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}