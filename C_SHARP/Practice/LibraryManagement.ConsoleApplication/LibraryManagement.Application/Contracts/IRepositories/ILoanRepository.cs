﻿using LibraryManagement.Domain.Entities;
using System.Linq.Expressions;

namespace LibraryManagement.Application.Contracts.IRepositories
{
    public interface ILoanRepository : IRepository<Loan>
    {
        Task<int> CountAsync(Expression<Func<Loan, bool>> filter);
    }
}