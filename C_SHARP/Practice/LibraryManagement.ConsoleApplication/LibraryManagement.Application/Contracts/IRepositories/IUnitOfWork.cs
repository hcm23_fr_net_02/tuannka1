﻿namespace LibraryManagement.Application.Contracts.IRepositories
{
    public interface IUnitOfWork
    {
        IBookRepository Book { get; }
        ICustomerRepository Customer { get; }
        ILoanRepository Loan { get; }
        Task SaveAsync();
    }
}