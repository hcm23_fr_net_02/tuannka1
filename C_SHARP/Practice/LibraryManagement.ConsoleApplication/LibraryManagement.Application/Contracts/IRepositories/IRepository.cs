﻿using LibraryManagement.Domain.Shared.Common;
using System.Linq.Expressions;

namespace LibraryManagement.Application.Contracts.IRepositories
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetPagingAsync(Expression<Func<T, bool>>? filter = null, string? includeProperties = null, bool tracked = false, Paging paging = default);
        Task<T?> GetAsync(Expression<Func<T, bool>> filter, string? includeProperties = null, bool tracked = false);
        Task AddAsync(T entity);
        Task RemoveAsync(T entity);
        Task UpdateAsync(T entity);
        Task<bool> AnyAsync(Expression<Func<T, bool>> filter);
    }
}