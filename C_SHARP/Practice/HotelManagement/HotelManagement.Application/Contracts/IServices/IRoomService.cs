﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Shared.Common;

namespace HotelManagement.Application.Contracts.IServices
{
    public interface IRoomService
    {
        Task<IEnumerable<Room>> GetPagingAsync(Paging paging);
        Task CreateAsync(Room Room);
        Task<bool> DeleteAsync(int id);
    }
}
