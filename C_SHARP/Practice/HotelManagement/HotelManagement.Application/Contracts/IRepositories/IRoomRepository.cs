﻿using HotelManagement.Domain.Entities;

namespace HotelManagement.Application.Contracts.IRepositories
{
    public interface IRoomRepository : IRepository<Room>
    {
    }
}
