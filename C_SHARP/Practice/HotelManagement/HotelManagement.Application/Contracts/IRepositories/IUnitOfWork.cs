﻿namespace HotelManagement.Application.Contracts.IRepositories
{
    public interface IUnitOfWork
    {
        IRoomRepository Room { get; }
        Task SaveAsync();
    }
}
