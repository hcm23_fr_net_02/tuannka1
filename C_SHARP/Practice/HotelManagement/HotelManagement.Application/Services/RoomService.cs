﻿using HotelManagement.Application.Contracts.IRepositories;
using HotelManagement.Application.Contracts.IServices;
using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Shared.Common;

namespace HotelManagement.Application.Services
{
    public class RoomService : IRoomService
    {
        #region Private Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public RoomService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Public Methods
        public async Task CreateAsync(Room room)
        {
            try
            {
                await _unitOfWork.Room.AddAsync(room);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                Room? objFromRepo = await _unitOfWork.Room.GetAsync(u => u.Id == id);
                if (objFromRepo is null)
                {
                    return false;
                }

                await _unitOfWork.Room.RemoveAsync(objFromRepo);
                await _unitOfWork.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Room>> GetPagingAsync(Paging paging)
        {
            return await _unitOfWork.Room.GetPagingAsync(paging: paging);
        }
        #endregion
    }
}
