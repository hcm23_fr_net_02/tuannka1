﻿using HotelManagement.Application.Contracts.IRepositories;

namespace HotelManagement.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _db;
        public IRoomRepository Room { get; private set; }

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            Room = new RoomRepository(_db);
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
