﻿using HotelManagement.Application.Contracts.IRepositories;
using HotelManagement.Domain.Entities;

namespace HotelManagement.Infrastructure.Repositories
{
    public class RoomRepository : Repository<Room>, IRoomRepository
    {
        private readonly ApplicationDbContext _db;

        public RoomRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
