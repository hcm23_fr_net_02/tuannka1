﻿using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Shared.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HotelManagement.Infrastructure.TypeConfiguration
{
    internal class RoomConfiguration : IEntityTypeConfiguration<Room>
    {
        public void Configure(EntityTypeBuilder<Room> modelBuilder)
        {
            modelBuilder.Property(r => r.RoomType).HasColumnType("tinyint");

            modelBuilder.Property(r => r.Status).HasColumnType("tinyint");

            modelBuilder.HasData
            (
                new Room
                {
                    Id = 1,
                    Price = 1,
                    RoomType = RoomType.Family,
                    Status = Status.Booked
                }
            );
        }
    }
}
