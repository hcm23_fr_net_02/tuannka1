﻿namespace HotelManagement.Domain.Shared.Enums
{
    public enum Status
    {
        Available,
        Booked
    }
}
