﻿namespace HotelManagement.Domain.Shared.Enums
{
    public enum RoomType
    {
        Single,
        Double,
        Family
    }
}
