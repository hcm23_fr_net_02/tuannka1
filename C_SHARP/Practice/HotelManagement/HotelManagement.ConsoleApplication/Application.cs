﻿using HotelManagement.ConsoleApplication.Contracts;
using HotelManagement.ConsoleApplication.Utils;

namespace HotelManagement.ConsoleApplication
{
    public class Application
    {
        private readonly IRoomPresentation _roomPresentation;

        public Application(IRoomPresentation roomPresentation)
        {
            _roomPresentation = roomPresentation;
        }

        public async Task RunAsync()
        {
            var applicationName = "Hotel Management Application";
            Console.WriteLine($"***{applicationName}***");

            var menuName = "Main Menu";
            var options = new Dictionary<string, Func<Task>>()
            {
                { "Add Room", _roomPresentation.AddRoomAsync },
                { "Delete Room", _roomPresentation.DeleteRoomAsync },
                { "View Rooms", _roomPresentation.ViewRoomsAsync },
              
            };

            await MenuUtil.ShowMenuAsync(menuName, options);

            //Exit 
            Console.WriteLine("\nSee you again");
        }
    }

}
