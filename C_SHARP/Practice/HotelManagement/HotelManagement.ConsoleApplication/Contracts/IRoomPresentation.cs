﻿namespace HotelManagement.ConsoleApplication.Contracts
{
    public interface IRoomPresentation
    {
        Task AddRoomAsync();
        Task ViewRoomsAsync();
        Task DeleteRoomAsync();
    }
}
