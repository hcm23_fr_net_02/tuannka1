﻿using System;
using System.Text;

namespace HotelManagement.ConsoleApplication.Utils
{
    public static class EnumUtil
    {
        public static string GetRepresentEnum<TEnum>() where TEnum : struct
        {
            var stringBuilder = new StringBuilder();
            foreach (TEnum value in Enum.GetValues(typeof(TEnum)))
            {
                string? name = Enum.GetName(typeof(TEnum), value);
                Console.WriteLine($"Value: {Convert.ToInt32(value)}, Name: {name}");
            }
            return stringBuilder.ToString();
        }
    }
}
