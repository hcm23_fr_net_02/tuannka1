﻿namespace HotelManagement.ConsoleApplication.Utils
{
    internal class ConsoleUtil
    {
        internal static string ReadString(string prompt)
        {
            Console.Write(prompt);
            return Console.ReadLine() ?? string.Empty;
        }

        internal static int ReadInt(string prompt)
        {
            int result;
            do
            {
                Console.Write(prompt);
            } while (!int.TryParse(Console.ReadLine(), out result));
            return result;
        }

        internal static TEnum ReadEnum<TEnum>(string prompt) where TEnum : struct
        {
            TEnum result;
            do
            {
                Console.Write(prompt);
            } while (!Enum.TryParse(Console.ReadLine(), out result) || !Enum.IsDefined(typeof(TEnum), result));
            return result;
        }

        internal static ushort ReadUShort(string prompt)
        {
            ushort result;
            do
            {
                Console.Write(prompt);
            } while (!ushort.TryParse(Console.ReadLine(), out result));
            return result;
        }

        internal static float ReadFloat(string prompt)
        {
            float result;
            do
            {
                Console.Write(prompt);
            } while (!float.TryParse(Console.ReadLine(), out result));
            return result;
        }

        internal static DateTime ReadDateTime(string prompt)
        {
            DateTime result;
            do
            {
                Console.Write(prompt);
            } while (!DateTime.TryParse(Console.ReadLine(), out result));
            return result;
        }
    }
}
