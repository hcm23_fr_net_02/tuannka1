﻿using HotelManagement.Application.Contracts.IRepositories;
using HotelManagement.Application.Contracts.IServices;
using HotelManagement.Application.Services;
using HotelManagement.ConsoleApplication;
using HotelManagement.ConsoleApplication.Contracts;
using HotelManagement.ConsoleApplication.Presentations;
using HotelManagement.Infrastructure;
using HotelManagement.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

IHostBuilder hostBuider = Host.CreateDefaultBuilder();

hostBuider.ConfigureServices((hostContext, services) =>
{
    services.AddDbContext<ApplicationDbContext>(options =>
        options
            .UseSqlServer(hostContext.Configuration.GetConnectionString("DefaultConnection"))
            .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole().SetMinimumLevel(LogLevel.None)))
    );

    services.AddScoped<Application>();

    services.AddScoped<IUnitOfWork, UnitOfWork>();

    services.AddScoped<IRoomService, RoomService>();

    services.AddScoped<IRoomPresentation, RoomPresentation>();
});

IHost host = hostBuider.Build();

var app = host.Services.GetRequiredService<Application>();

await app.RunAsync();