﻿using HotelManagement.Application.Contracts.IServices;
using HotelManagement.ConsoleApplication.Contracts;
using HotelManagement.ConsoleApplication.Utils;
using HotelManagement.Domain.Entities;
using HotelManagement.Domain.Shared.Common;
using HotelManagement.Domain.Shared.Enums;
using System;

namespace HotelManagement.ConsoleApplication.Presentations
{
    public class RoomPresentation : IRoomPresentation
    {
        private readonly IRoomService _roomService;

        public RoomPresentation(IRoomService roomService)
        {
            _roomService = roomService;
        }
        public async Task AddRoomAsync()
        {
            try
            {
                //create an object of Room
                Room room = new Room();

                //read all details from the user
                Console.WriteLine("\n***Add Room***");

                room.Status = ConsoleUtil.ReadEnum<Status>($"{EnumUtil.GetRepresentEnum<Status>()}\nStatus: ");
                room.RoomType = ConsoleUtil.ReadEnum<RoomType>($"{EnumUtil.GetRepresentEnum<RoomType>()}\nRoomType: ");
                room.Price = ConsoleUtil.ReadFloat("Price: ");

                await _roomService.CreateAsync(room);

                Console.WriteLine("Room added.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Room not added.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task DeleteRoomAsync()
        {
            try
            {
                Console.WriteLine("\n***Delete Room***");

                int roomIdToDelete = ConsoleUtil.ReadInt("Room Id: ");

                bool isExist = await _roomService.DeleteAsync(roomIdToDelete);

                if (!isExist)
                {
                    Console.WriteLine($"Room id: {roomIdToDelete} not existed");
                    return;
                }

                Console.WriteLine("Room deleted.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Room not deleted.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task ViewRoomsAsync()
        {
            try
            {
                var menuName = "\n***View Rooms***";
                var menuChoice = -1;
                var paging = new Paging { NumberPerPage = 2, PageNumber = 0 };
                var lastPage = -1;

                do
                {
                    //show menu
                    Console.WriteLine($"\n:::{menuName}:::");

                    Console.WriteLine($"Current Page: {paging.PageNumber}");

                    IEnumerable<Room> rooms = await _roomService.GetPagingAsync(paging);

                    foreach (var room in rooms)
                    {
                        Console.WriteLine(room);
                    }

                    if (rooms.Count() == 0)
                    {
                        lastPage = paging.PageNumber;
                        Console.WriteLine("This is final page.");
                    }

                    Console.WriteLine("1. Back");
                    Console.WriteLine("2. Next");

                    // Option for exit
                    Console.WriteLine("0. Exit");

                    do
                    {
                        Console.Write("Your choice: ");
                    } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                    switch (menuChoice)
                    {
                        case 1:
                            if (lastPage == 0)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber--;
                            break;
                        case 2:
                            if (lastPage == 0 || lastPage == paging.PageNumber)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber++;
                            break;
                    }
                } while (menuChoice != 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }
    }
}
