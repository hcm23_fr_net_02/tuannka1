﻿using HotelManagement.Domain.Shared.Enums;
using System.Text;

namespace HotelManagement.Domain.Entities
{
    public class Room
    {
        #region Private fields
        private int _id;
        private Status _status;
        private RoomType _roomType;
        private float _price;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public RoomType RoomType
        {
            get => _roomType;
            set => _roomType = value;
        }
        
        public Status Status
        {
            get => _status;
            set => _status = value;
        }

        public float Price
        {
            get => _price;
            set
            {
                if (value <= 0)
                    throw new Exception($"Price should be greater than 0.");
                _price = value;
            }
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tRoom Id: {Id}")
                .AppendLine($"\tPrice: {Price}")
                .AppendLine($"\tRoom Type: {RoomType.ToString()}")
                .AppendLine($"\tRoom Type: {Status.ToString()}")
                .AppendLine($"\tPrice: {Price}");

            return stringBuilder.ToString();
        }
        #endregion
    }

}
