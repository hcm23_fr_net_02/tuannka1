﻿using MobileManagement.Domain.Entities;
using MobileManagement.Domain.Shared.Common;

namespace MobileManagement.Application.Contracts.IServices
{
    public interface IPurchaseService
    {
        Task<IEnumerable<Purchase>> GetPagingAsync(Paging paging);
        Task CreateAsync(Purchase purchase);
    }
}