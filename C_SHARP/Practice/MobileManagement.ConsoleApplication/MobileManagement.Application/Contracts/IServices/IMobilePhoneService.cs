﻿using MobileManagement.Domain.Entities;
using MobileManagement.Domain.Shared.Common;

namespace MobileManagement.Application.Contracts.IServices
{
    public interface IMobilePhoneService
    {
        Task<IEnumerable<MobilePhone>> GetPagingAsync(Paging paging);
        Task<MobilePhone?> GetByIdAsync(int id);
        Task CreateAsync(MobilePhone mobilePhone);
        Task UpdateAsync(MobilePhone mobilePhone);
        Task<bool> DeleteAsync(int id);
    }
}