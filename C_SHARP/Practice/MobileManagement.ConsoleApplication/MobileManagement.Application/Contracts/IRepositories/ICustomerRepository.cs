﻿using MobileManagement.Domain.Entities;

namespace MobileManagement.Application.Contracts.IRepositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}