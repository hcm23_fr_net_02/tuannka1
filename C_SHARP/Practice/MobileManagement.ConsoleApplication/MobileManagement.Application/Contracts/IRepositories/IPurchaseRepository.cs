﻿using MobileManagement.Domain.Entities;
using System.Linq.Expressions;

namespace MobileManagement.Application.Contracts.IRepositories
{
    public interface IPurchaseRepository : IRepository<Purchase>
    {
        Task<int> CountAsync(Expression<Func<Purchase, bool>> filter);
    }
}