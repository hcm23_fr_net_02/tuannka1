﻿
using MobileManagement.Domain.Entities;

namespace MobileManagement.Application.Contracts.IRepositories
{
    public interface IMobilePhoneRepository : IRepository<MobilePhone>
    {
        Task<MobilePhone?> GetDetailByIdAsync(int id, bool tracked = false);
    }
}