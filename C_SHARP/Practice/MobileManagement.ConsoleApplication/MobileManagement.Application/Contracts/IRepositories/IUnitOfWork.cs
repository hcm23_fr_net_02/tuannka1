﻿namespace MobileManagement.Application.Contracts.IRepositories
{
    public interface IUnitOfWork
    {
        IMobilePhoneRepository MobilePhone { get; }
        ICustomerRepository Customer { get; }
        IPurchaseRepository Purchase { get; }
        Task SaveAsync();
    }
}