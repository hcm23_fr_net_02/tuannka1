﻿using MobileManagement.Application.Contracts.IRepositories;
using MobileManagement.Application.Contracts.IServices;
using MobileManagement.Domain.Entities;
using MobileManagement.Domain.Shared.Common;

namespace MobileManagement.Application.Services
{
    public class MobilePhoneService : IMobilePhoneService
    {
        #region Private Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public MobilePhoneService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Create a new mobilePhone.
        /// </summary>
        /// <param name="mobilePhone"></param>
        /// <returns></returns>
        public async Task CreateAsync(MobilePhone mobilePhone)
        {
            try
            {
                await _unitOfWork.MobilePhone.AddAsync(mobilePhone);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                MobilePhone? objFromRepo = await _unitOfWork.MobilePhone.GetAsync(u => u.Id == id);
                if (objFromRepo is null)
                {
                    return false;
                }

                await _unitOfWork.MobilePhone.RemoveAsync(objFromRepo);
                await _unitOfWork.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<MobilePhone?> GetByIdAsync(int id)
        {
            try
            {
                return await _unitOfWork.MobilePhone.GetAsync(b => b.Id == id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<MobilePhone>> GetPagingAsync(Paging paging)
        {
            return await _unitOfWork.MobilePhone.GetPagingAsync(paging: paging);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobilePhone"></param>
        /// <returns></returns>
        public Task UpdateAsync(MobilePhone mobilePhone)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

}
