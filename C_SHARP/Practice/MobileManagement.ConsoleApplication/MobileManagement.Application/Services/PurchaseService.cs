﻿using MobileManagement.Application.Common;
using MobileManagement.Application.Contracts.IRepositories;
using MobileManagement.Application.Contracts.IServices;
using MobileManagement.Domain.Entities;
using MobileManagement.Domain.Shared.Common;

namespace MobileManagement.Application.Services
{
    public class PurchaseService : IPurchaseService
    {
        #region Private Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public PurchaseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Public Methods
        public async Task CreateAsync(Purchase purchase)
        {
            try
            {
                MobilePhone? mobilePhone = await _unitOfWork.MobilePhone.GetAsync(b => b.Id == purchase.MobilePhoneId);

                if (mobilePhone is null)
                {
                    throw new BusinessException($"MobilePhone Id: {purchase.MobilePhoneId} not exist.");
                }

                Customer? customer = await _unitOfWork.Customer.GetAsync(c => c.Id == purchase.CustomerId);

                if (customer is null)
                {
                    throw new BusinessException($"Customer Id: {purchase.CustomerId} not exist.");
                }

                int totalBorrowedMobilePhone = await _unitOfWork.Purchase.CountAsync(l => l.MobilePhoneId == purchase.MobilePhoneId);

                if (mobilePhone.Quantity <= totalBorrowedMobilePhone)
                {
                    throw new BusinessException($"MobilePhone Id: {purchase.MobilePhoneId} not available.");
                }

                await _unitOfWork.Purchase.AddAsync(purchase);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Purchase>> GetPagingAsync(Paging paging)
        {
            return await _unitOfWork.Purchase.GetPagingAsync(paging: paging, includeProperties: nameof(Purchase.Customer));
        }
        #endregion
    }
}
