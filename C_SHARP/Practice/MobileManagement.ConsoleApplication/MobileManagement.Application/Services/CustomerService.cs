﻿using MobileManagement.Application.Contracts.IRepositories;
using MobileManagement.Application.Contracts.IServices;

namespace MobileManagement.Application.Services
{
    public class CustomerService : ICustomerService
    {
        #region Private Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
