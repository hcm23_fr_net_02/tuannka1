﻿using MobileManagement.Infrastructure.TypeConfiguration;
using Microsoft.EntityFrameworkCore;
using MobileManagement.Domain.Entities;

namespace MobileManagement.Infrastructure
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<MobilePhone> MobilePhones { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MobilePhoneConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new PurchaseConfiguration());
        }
    }
}