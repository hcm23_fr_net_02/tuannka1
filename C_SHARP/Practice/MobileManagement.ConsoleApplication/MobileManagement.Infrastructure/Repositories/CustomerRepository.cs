﻿using MobileManagement.Application.Contracts.IRepositories;
using MobileManagement.Domain.Entities;

namespace MobileManagement.Infrastructure.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        private readonly ApplicationDbContext _db;

        public CustomerRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}