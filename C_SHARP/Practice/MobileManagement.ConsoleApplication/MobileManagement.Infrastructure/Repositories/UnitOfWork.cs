﻿using MobileManagement.Application.Contracts.IRepositories;

namespace MobileManagement.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _db;
        public IMobilePhoneRepository MobilePhone { get; private set; }

        public ICustomerRepository Customer { get; private set; }

        public IPurchaseRepository Purchase { get; private set; }

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            MobilePhone = new MobilePhoneRepository(_db);
            Customer = new CustomerRepository(_db);
            Purchase = new PurchaseRepository(_db);
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}