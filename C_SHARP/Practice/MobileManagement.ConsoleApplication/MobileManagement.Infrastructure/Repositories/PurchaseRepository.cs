﻿using MobileManagement.Application.Contracts.IRepositories;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Linq.Expressions;
using MobileManagement.Domain.Entities;

namespace MobileManagement.Infrastructure.Repositories
{
    public class PurchaseRepository : Repository<Purchase>, IPurchaseRepository
    {
        private readonly ApplicationDbContext _db;

        public PurchaseRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public async Task<int> CountAsync(Expression<Func<Purchase, bool>> filter)
        {
            IQueryable<Purchase> query = _db.Set<Purchase>();

            return await query.Where(filter).CountAsync();
        }
    }
}