﻿using MobileManagement.Application.Contracts.IRepositories;
using MobileManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace MobileManagement.Infrastructure.Repositories
{
    public class MobilePhoneRepository : Repository<MobilePhone>, IMobilePhoneRepository
    {
        private readonly ApplicationDbContext _db;

        public MobilePhoneRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public async Task<MobilePhone?> GetDetailByIdAsync(int id, bool tracked = false)
        {
            IQueryable<MobilePhone> query;
            DbSet<MobilePhone> dbSet = _db.Set<MobilePhone>();

            if (tracked)
            {
                query = dbSet;
            }
            else
            {
                query = dbSet.AsNoTracking();
            }

            return await query.FirstOrDefaultAsync();
        }
    }
}