﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MobileManagement.Domain.Entities;

namespace MobileManagement.Infrastructure.TypeConfiguration
{
    internal class PurchaseConfiguration : IEntityTypeConfiguration<Purchase>
    {
        public void Configure(EntityTypeBuilder<Purchase> modelBuilder)
        {
            modelBuilder
                .HasOne(l => l.Customer)
                .WithMany()
                .HasForeignKey(l => l.CustomerId)
                .IsRequired();
            
            modelBuilder
                .HasOne(l => l.MobilePhone)
                .WithMany()
                .HasForeignKey(l => l.MobilePhoneId)
                .IsRequired();
        }
    }
}