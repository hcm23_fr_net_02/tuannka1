﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MobileManagement.Domain.Entities;

namespace MobileManagement.Infrastructure.TypeConfiguration
{
    internal class MobilePhoneConfiguration : IEntityTypeConfiguration<MobilePhone>
    {
        public void Configure(EntityTypeBuilder<MobilePhone> modelBuilder)
        {
            modelBuilder.Property(b => b.Name).HasMaxLength(255);

            modelBuilder.Property(b => b.ManufacturerName).HasMaxLength(255);

            modelBuilder.HasData
            (
                new MobilePhone
                {
                    Id = 1,
                    Name = "Harry Potter",
                    ManufacturerName = "J.K. Rowling",
                    Price = 100,
                    Quantity = 4
                }
            );
        }
    }
}