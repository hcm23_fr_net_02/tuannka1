﻿using System.Text;
using System.Xml.Linq;

namespace MobileManagement.Domain.Entities
{
    public class MobilePhone
    {
        #region Private fields
        private int _id;
        private string _name;
        private string _manufacturerName;
        private ushort _quantity;
        private float _price;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new Exception($"MobilePhone name should not be null, whitespace or greater than 255 characters long.");
                }
                _name = value;
            }
        }

        public string ManufacturerName
        {
            get => _manufacturerName;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new Exception($"Manufacturer Name should not be null, whitespace or greater than 255 characters long.");
                }
                _manufacturerName = value;
            }
        }

        public ushort Quantity
        {
            get => _quantity;
            set
            {
                if (value < 0)
                {
                    throw new Exception($"Quantity should be greater than or equal to 0.");
                }
                _quantity = value;
            }
        }

        public float Price
        {
            get => _price;
            set
            {
                if (value <= 0)
                    throw new Exception($"Price should be greater than 0.");
                _price = value;
            }
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tMobilePhone Id: {Id}")
                .AppendLine($"\tName: {Name}")
                .AppendLine($"\tAuthor name: {ManufacturerName}")
                .AppendLine($"\tQuantity: {Quantity}")
                .AppendLine($"\tPrice: {Price}");

            return stringBuilder.ToString();
        }
        #endregion
    }
}