﻿using System.Diagnostics;
using System.Reflection.PortableExecutable;
using System.Text;

namespace MobileManagement.Domain.Entities
{
    public class Purchase
    {
        #region Private fields
        private int _id;
        private DateTime _buyDate;
        private int _mobilePhoneId;
        private int _customerId;
        private MobilePhone? _mobilePhone;
        private Customer? _customer;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }
        public DateTime BuyDate
        {
            get => _buyDate;
            set => _buyDate = value;
        }

        public int MobilePhoneId
        {
            get => _mobilePhoneId;
            set => _mobilePhoneId = value;
        }

        public int CustomerId
        {
            get => _customerId;
            set => _customerId = value;
        }

        public MobilePhone? MobilePhone
        {
            get => _mobilePhone;
            set => _mobilePhone = value;
        }

        public Customer? Customer
        {
            get => _customer;
            set => _customer = value;
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tPurchase Id: {Id}")
                .AppendLine($"\tMobilePhone Id: {MobilePhoneId}")
                .AppendLine($"\tCustomer Id: {CustomerId}")
                .AppendLine($"\tBuy Date: {BuyDate}");

            if (Customer is not null)
            {
                stringBuilder
                    .AppendLine($"\tCustomer Name: {Customer.Name}");
            }

            return stringBuilder.ToString();
        }
        #endregion
    }
}