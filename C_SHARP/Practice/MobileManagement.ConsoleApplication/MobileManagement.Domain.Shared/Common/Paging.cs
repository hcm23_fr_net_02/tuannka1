﻿namespace MobileManagement.Domain.Shared.Common
{
    public struct Paging
    {
        #region Private fields
        private int? _pageNumber;
        private int? _numberPerPage;

        #endregion

        #region Public Properties
        public int PageNumber
        {
            get => _pageNumber ?? 0;
            set
            {
                if (value < 0)
                {
                    throw new Exception($"PageNumber should be greater than or equal to 0.");
                }
                _pageNumber = value;
            }
        }

        public int NumberPerPage
        {
            get => _numberPerPage ?? 5;
            set
            {
                if (value <= 0)
                {
                    throw new Exception($"NumberPerPage should be greater than 0.");
                }
                _numberPerPage = value;
            }
        }
        #endregion
    }
}