﻿using MobileManagement.ConsoleApplication.Presentations;

namespace MobileManagement.ConsoleApplication.Contracts
{
    public interface IPurchasePresentation
    {
        Task AddPurchaseAsync();
        Task ViewPurchasesAsync();
    }
}