﻿using MobileManagement.ConsoleApplication.Presentations;

namespace MobileManagement.ConsoleApplication.Contracts
{
    public interface IMobilePhonePresentation
    {
        Task AddMobilePhoneAsync();
        Task ViewMobilePhonesAsync();
        Task SearchMobilePhoneByIdAsync();
        Task DeleteMobilePhoneAsync();
    }
}