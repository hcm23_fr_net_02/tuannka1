﻿using MobileManagement.Application.Contracts.IServices;
using MobileManagement.Application.Services;
using MobileManagement.ConsoleApplication.Contracts;
using MobileManagement.ConsoleApplication.Utils;
using MobileManagement.Domain.Entities;
using MobileManagement.Domain.Shared.Common;

namespace MobileManagement.ConsoleApplication.Presentations
{
    public class PurchasePresentation : IPurchasePresentation
    {
        private readonly IPurchaseService _purchaseService;

        public PurchasePresentation(IPurchaseService purchaseService)
        {
            _purchaseService = purchaseService;
        }

        public async Task AddPurchaseAsync()
        {
            try
            {
                //create an object of Purchase
                Purchase purchase = new Purchase();

                //read all details from the user
                Console.WriteLine("\n***Add Purchase***");

                purchase.MobilePhoneId = ConsoleUtil.ReadInt("MobilePhone Id: ");
                purchase.CustomerId = ConsoleUtil.ReadInt("Customer Id: ");
                purchase.BuyDate = DateTime.Now;
                Console.WriteLine($"Buy Date: {purchase.BuyDate.ToShortDateString()}");

                await _purchaseService.CreateAsync(purchase);

                Console.WriteLine("Purchase added.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Purchase not added.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task ViewPurchasesAsync()
        {
            try
            {
                var menuName = "\n***View Purchases***";
                var menuChoice = -1;
                var paging = new Paging { NumberPerPage = 2, PageNumber = 0 };
                var lastPage = -1;

                do
                {
                    //show menu
                    Console.WriteLine($"\n:::{menuName}:::");

                    Console.WriteLine($"Current Page: {paging.PageNumber}");

                    IEnumerable<Purchase> purchases = await _purchaseService.GetPagingAsync(paging);

                    foreach (var purchase in purchases)
                    {
                        Console.WriteLine(purchase);
                    }

                    if (purchases.Count() == 0)
                    {
                        lastPage = paging.PageNumber;
                        //paging.PageNumber--;
                        Console.WriteLine("This is final page.");
                    }

                    Console.WriteLine("1. Back");
                    Console.WriteLine("2. Next");

                    // Option for exit
                    Console.WriteLine("0. Exit");

                    do
                    {
                        Console.Write("Your choice: ");
                    } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                    switch (menuChoice)
                    {
                        case 1:
                            if (lastPage == 0)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber--;
                            break;
                        case 2:
                            if (lastPage == 0 || lastPage == paging.PageNumber)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber++;
                            break;
                    }
                } while (menuChoice != 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }
    }
}