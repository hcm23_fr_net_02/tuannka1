﻿using Azure;
using MobileManagement.ConsoleApplication.Contracts;
using MobileManagement.ConsoleApplication.Utils;
using MobileManagement.Domain.Entities;
using MobileManagement.Domain.Shared.Common;
using Microsoft.Extensions.Options;
using MobileManagement.Application.Contracts.IServices;

namespace MobileManagement.ConsoleApplication.Presentations
{
    public class MobilePhonePresentation : IMobilePhonePresentation
    {
        private readonly IMobilePhoneService _mobilePhoneService;

        public MobilePhonePresentation(IMobilePhoneService mobilePhoneService)
        {
            _mobilePhoneService = mobilePhoneService;
        }

        public async Task AddMobilePhoneAsync()
        {
            try
            {
                //create an object of MobilePhone
                MobilePhone mobilePhone = new MobilePhone();

                //read all details from the user
                Console.WriteLine("\n***Add MobilePhone***");

                mobilePhone.Name = ConsoleUtil.ReadString("Name: ");
                mobilePhone.ManufacturerName = ConsoleUtil.ReadString("Manufacturer Name: ");
                mobilePhone.Quantity = ConsoleUtil.ReadUShort("Quantity: ");
                mobilePhone.Price = ConsoleUtil.ReadFloat("Price: ");

                await _mobilePhoneService.CreateAsync(mobilePhone);

                Console.WriteLine("MobilePhone added.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("MobilePhone not added.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task DeleteMobilePhoneAsync()
        {
            try
            {
                Console.WriteLine("\n***Delete MobilePhone***");

                int mobilePhoneIdToDelete = ConsoleUtil.ReadInt("MobilePhone Id: ");

                bool isExist = await _mobilePhoneService.DeleteAsync(mobilePhoneIdToDelete);

                if (!isExist)
                {
                    Console.WriteLine($"MobilePhone id: {mobilePhoneIdToDelete} not existed");
                    return;
                }

                Console.WriteLine("MobilePhone deleted.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("MobilePhone not deleted.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task SearchMobilePhoneByIdAsync()
        {
            try
            {
                Console.WriteLine("\n***Search MobilePhone by Id***");

                int mobilePhoneIdToSearch = ConsoleUtil.ReadInt("MobilePhone Id: ");

                MobilePhone? mobilePhone = await _mobilePhoneService.GetByIdAsync(mobilePhoneIdToSearch);

                if (mobilePhone is null)
                {
                    Console.WriteLine($"MobilePhone id: {mobilePhoneIdToSearch} not existed");
                    return;
                }

                Console.WriteLine(mobilePhone);
            }
            catch (Exception ex)
            {
                Console.WriteLine("MobilePhone not deleted.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task ViewMobilePhonesAsync()
        {
            try
            {
                var menuName = "\n***View MobilePhones***";
                var menuChoice = -1;
                var paging = new Paging { NumberPerPage = 2, PageNumber = 0 };
                var lastPage = -1;

                do
                {
                    //show menu
                    Console.WriteLine($"\n:::{menuName}:::");

                    Console.WriteLine($"Current Page: {paging.PageNumber}");

                    IEnumerable<MobilePhone> mobilePhones = await _mobilePhoneService.GetPagingAsync(paging);

                    foreach(var mobilePhone in mobilePhones)
                    {
                        Console.WriteLine(mobilePhone);
                    }

                    if (mobilePhones.Count() == 0)
                    {
                        lastPage = paging.PageNumber;
                        //paging.PageNumber--;
                        Console.WriteLine("This is final page.");
                    }

                    Console.WriteLine("1. Back");
                    Console.WriteLine("2. Next");

                    // Option for exit
                    Console.WriteLine("0. Exit");

                    do
                    {
                        Console.Write("Your choice: ");
                    } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                    switch (menuChoice)
                    {
                        case 1:
                            if (lastPage == 0)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber--;
                            break;
                        case 2:
                            if (lastPage == 0 || lastPage == paging.PageNumber)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber++;
                            break;
                    }
                } while (menuChoice != 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }
    }
}