﻿using MobileManagement.Application.Contracts.IRepositories;
using MobileManagement.ConsoleApplication.Contracts;
using MobileManagement.ConsoleApplication.Presentations;
using MobileManagement.ConsoleApplication.Utils;

namespace MobileManagement.ConsoleApplication
{
    public class Application
    {
        private readonly IMobilePhonePresentation _mobilePhonePresentation;
        private readonly IPurchasePresentation _purchasePresentation;

        public Application(IMobilePhonePresentation mobilePhonePresentation, IPurchasePresentation purchasePresentation)
        {
            _mobilePhonePresentation = mobilePhonePresentation;
            _purchasePresentation = purchasePresentation;
        }

        public async Task RunAsync()
        {
            var applicationName = "Mobile Management Application";
            Console.WriteLine($"***{applicationName}***");

            var menuName = "Main Menu";
            var options = new Dictionary<string, Func<Task>>()
            {
                { "Add mobilePhone", _mobilePhonePresentation.AddMobilePhoneAsync },
                { "Delete mobilePhone", _mobilePhonePresentation.DeleteMobilePhoneAsync },
                { "View mobilePhones", _mobilePhonePresentation.ViewMobilePhonesAsync },
                { "Add Purchase", _purchasePresentation.AddPurchaseAsync },
                { "View purchases", _purchasePresentation.ViewPurchasesAsync },
                { "Search mobilePhone by Id", _mobilePhonePresentation.SearchMobilePhoneByIdAsync },
            };

            await MenuUtil.ShowMenuAsync(menuName, options);

            //Exit 
            Console.WriteLine("\nSee you again");
        }
    }
}