﻿using MobileManagement.Application.Contracts.IRepositories;
using MobileManagement.Application.Contracts.IServices;
using MobileManagement.Application.Services;
using MobileManagement.ConsoleApplication;
using MobileManagement.ConsoleApplication.Contracts;
using MobileManagement.ConsoleApplication.Presentations;
using MobileManagement.Infrastructure;
using MobileManagement.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

IHostBuilder hostBuider = Host.CreateDefaultBuilder();

hostBuider.ConfigureServices((hostContext, services) =>
{
    services.AddDbContext<ApplicationDbContext>(options =>
        options
            .UseSqlServer(hostContext.Configuration.GetConnectionString("DefaultConnection"))
            .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole().SetMinimumLevel(LogLevel.None)))
    );

    services.AddScoped<Application>();

    services.AddScoped<IUnitOfWork, UnitOfWork>();

    services.AddScoped<IMobilePhoneService, MobilePhoneService>();
    services.AddScoped<ICustomerService, CustomerService>();
    services.AddScoped<IPurchaseService, PurchaseService>();

    services.AddScoped<IMobilePhonePresentation, MobilePhonePresentation>();
    services.AddScoped<IPurchasePresentation, PurchasePresentation>();
});

IHost host = hostBuider.Build();

var app = host.Services.GetRequiredService<Application>();

await app.RunAsync();