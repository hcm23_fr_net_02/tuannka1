﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieManagement.Domain.Entities;

namespace MovieManagement.Infrastructure.TypeConfiguration
{
    internal class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> modelBuilder)
        {
            modelBuilder.Property(b => b.Title).HasMaxLength(255);

            modelBuilder.Property(b => b.CountryName).HasMaxLength(255);

            modelBuilder.Property(b => b.DicrectorName).HasMaxLength(255);

            modelBuilder
                .HasMany(m => m.Reviews)
                .WithOne(r => r.Movie)
                .HasForeignKey(r => r.MovieId)
                .IsRequired();

            modelBuilder.HasData
            (
                new Movie
                {
                    Id = 1,
                    Title = "Harry Potter",
                    CountryName = "US",
                    PublicationYear = 2001,
                    DicrectorName = "Dicrector"
                }
            );
        }
    }

}
