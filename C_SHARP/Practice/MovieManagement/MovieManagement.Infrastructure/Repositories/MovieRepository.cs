﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.Application.Contracts.IRepository;
using MovieManagement.Domain.Entities;

namespace MovieManagement.Infrastructure.Repositories
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        private readonly ApplicationDbContext _db;

        public MovieRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public async Task<Movie?> GetDetailByIdAsync(int id, bool tracked = false)
        {
            IQueryable<Movie> query;
            DbSet<Movie> dbSet = _db.Set<Movie>();

            if (tracked)
            {
                query = dbSet;
            }
            else
            {
                query = dbSet.AsNoTracking();
            }

            query = query.Include(m => m.Reviews);

            return await query.FirstOrDefaultAsync();
        }
    }
}
