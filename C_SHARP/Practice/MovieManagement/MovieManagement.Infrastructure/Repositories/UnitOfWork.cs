﻿using MovieManagement.Application.Contracts.IRepository;

namespace MovieManagement.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _db;
        public IMovieRepository Movie { get; private set; }

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            Movie = new MovieRepository(_db);
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
