﻿namespace MovieManagement.Domain.Entities
{
    public class Review
    {
        #region Private fields
        private int _id;
        private string _title;
        private string _content;
        private byte _rating;
        private int _movieId;
        private Movie? _movie;
        #endregion

        #region Public Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Title
        {
            get => _title;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                    throw new Exception($"Review Title should not be null, whitespace or greater than 255 characters long.");
                _title = value;
            }
        }

        public string Content
        {
            get => _content;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 3000)
                    throw new Exception($"Review Content should not be null, whitespace or greater than 3000 characters long.");
                _content = value;
            }
        }

        public byte Rating
        {
            get => _rating;
            set
            {
                if (value < 1 || value > 5)
                    throw new Exception($"Rating should be in range (1, 5).");
                _rating = value;
            }
        }

        public int MovieId
        {
            get => _movieId;
            set => _movieId = value;
        }

        public Movie? Movie
        {
            get => _movie;
            set => _movie = value;
        }
        #endregion
    }

}
