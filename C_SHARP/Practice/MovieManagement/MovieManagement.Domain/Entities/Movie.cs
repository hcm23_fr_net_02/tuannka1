﻿using System.Text;

namespace MovieManagement.Domain.Entities
{
    public class Movie
    {
        #region Private fields
        private int _id;
        private string _title;
        private short _publicationYear;
        private string _directorName;
        private string _countryName;
        private IEnumerable<Review>? _reviewers;
        #endregion

        #region Public properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Title
        {
            get => _title;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new Exception($"Movie title should not be null, whitespace or greater than 255 characters long.");
                }
                _title = value;
            }
        }

        public string DicrectorName
        {
            get => _directorName;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new Exception($"Dicrector Name should not be null, whitespace or greater than 255 characters long.");
                }
                _directorName = value;
            }
        }

        public string CountryName
        {
            get => _countryName;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new Exception($"Country Name should not be null, whitespace or greater than 255 characters long.");
                }
                _countryName = value;
            }
        }

        public short PublicationYear
        {
            get => _publicationYear;
            set
            {
                if (value < -4000 || value > DateTime.Now.Year)
                    throw new Exception($"Publication Year should not be less than 4000BC or greater than Year now: {DateTime.Now.Year}.");
                _publicationYear = value;
            }
        }

        public IEnumerable<Review>? Reviews
        {
            get => _reviewers;
            set => _reviewers = value;
        }
        #endregion

        #region Public methods
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder
                .AppendLine($"\tMovie Id: {Id}")
                .AppendLine($"\tTitle: {Title}")
                .AppendLine($"\tPublication Year: {PublicationYear}")
                .AppendLine($"\tDicrector Name: {DicrectorName}")
                .AppendLine($"\tCountry Name: {CountryName}");

            return stringBuilder.ToString();
        }
        #endregion
    }
}
