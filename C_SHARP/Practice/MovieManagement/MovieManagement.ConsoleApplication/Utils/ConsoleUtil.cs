﻿using Microsoft.Identity.Client;

namespace MovieManagement.ConsoleApplication.Utils
{
    internal class ConsoleUtil
    {
        internal static string ReadString(string prompt)
        {
            Console.Write(prompt);
            return Console.ReadLine() ?? string.Empty;
        }

        internal static int ReadInt(string prompt)
        {
            int result;
            do
            {
                Console.Write(prompt);
            } while (!int.TryParse(Console.ReadLine(), out result));
            return result;
        }

        internal static ushort ReadUShort(string prompt)
        {
            ushort result;
            do
            {
                Console.Write(prompt);
            } while (!ushort.TryParse(Console.ReadLine(), out result));
            return result;
        }

        internal static float ReadFloat(string prompt)
        {
            float result;
            do
            {
                Console.Write(prompt);
            } while (!float.TryParse(Console.ReadLine(), out result));
            return result;
        }

        internal static DateTime ReadDateTime(string prompt)
        {
            DateTime result;
            do
            {
                Console.Write(prompt);
            } while (!DateTime.TryParse(Console.ReadLine(), out result));
            return result;
        }

        internal static short ReadShort(string prompt)
        {
            short result;
            do
            {
                Console.Write(prompt);
            } while (!short.TryParse(Console.ReadLine(), out result));
            return result;
        }
    }

}
