﻿using MovieManagement.Application.Contracts.IService;
using MovieManagement.ConsoleApplication.Contracts;
using MovieManagement.ConsoleApplication.Utils;
using MovieManagement.Domain.Entities;
using MovieManagement.Domain.Shared.Common;
using static System.Reflection.Metadata.BlobBuilder;

namespace MovieManagement.ConsoleApplication.Presentations
{
    public class MoviePresentation : IMoviePresentation
    {
        #region Private fields
        private readonly IMovieService _movieService;
        #endregion

        #region Constructor
        public MoviePresentation(IMovieService movieService)
        {
            _movieService = movieService;
        }
        #endregion

        #region Public methods
        public async Task AddMovieAsync()
        {
            try
            {
                //create an object of Movie
                Movie movie = new Movie();

                //read all details from the user
                Console.WriteLine("\n***Add Movie***");

                movie.Title = ConsoleUtil.ReadString("Title: ");
                movie.PublicationYear = ConsoleUtil.ReadShort("Publication Year: ");
                movie.DicrectorName = ConsoleUtil.ReadString("Director Name: ");
                movie.CountryName = ConsoleUtil.ReadString("Country Name: ");

                await _movieService.CreateAsync(movie);

                Console.WriteLine("Movie added.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Movie not added.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        public async Task SearchMoviesAsync()
        {
            var menuName = "Search Movies Menu";
            var options = new Dictionary<string, Func<Task>>()
            {
                { "Search Movies By Title", SearchMoviesByTitleAsync },
                { "Search Movies By Publication Year", SearchMoviesByPublicationYearAsync },
                { "Search Movies By Director Name", SearchMoviesByDicrectorNameAsync },
                { "Search Movies By Country Name", SearchMoviesByCountryNameAsync }
            };

            await MenuUtil.ShowMenuAsync(menuName, options);
        }

        public async Task ViewMoviesAsync()
        {
            try
            {
                var menuName = "\n***View Movies***";
                var menuChoice = -1;
                var paging = new Paging { NumberPerPage = 2, PageNumber = 0 };
                var lastPage = -1;

                do
                {
                    //show menu
                    Console.WriteLine($"\n:::{menuName}:::");

                    Console.WriteLine($"Current Page: {paging.PageNumber}");

                    IEnumerable<Movie> movies = await _movieService.GetPagingAsync(paging);

                    foreach (var movie in movies)
                    {
                        Console.WriteLine(movie);
                    }

                    if (movies.Count() == 0)
                    {
                        lastPage = paging.PageNumber;
                        //paging.PageNumber--;
                        Console.WriteLine("This is final page.");
                    }

                    Console.WriteLine("1. Back");
                    Console.WriteLine("2. Next");
                    Console.WriteLine("3. Search Detail Movie By Id");

                    // Option for exit
                    Console.WriteLine("0. Exit");

                    do
                    {
                        Console.Write("Your choice: ");
                    } while (!int.TryParse(Console.ReadLine(), out menuChoice));

                    switch (menuChoice)
                    {
                        case 1:
                            if (lastPage == 0)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber--;
                            break;
                        case 2:
                            if (lastPage == 0 || lastPage == paging.PageNumber)
                            {
                                Console.WriteLine("This is final page.");
                                break;
                            }
                            paging.PageNumber++;
                            break;
                        case 3:
                            await SearchDetailMovieById();
                            break;
                    }
                } while (menuChoice != 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        #endregion

        #region Private Methods
        private async Task SearchMoviesByPublicationYearAsync()
        {
            try
            {
                Console.WriteLine("\n***Search Movies by Publication Year***");

                short publicationYearToSearch = ConsoleUtil.ReadShort("Publication Year To Search: ");

                IEnumerable<Movie> movies = await _movieService.GetByPublicationYearAsync(publicationYearToSearch);

                foreach (var movie in movies)
                {
                    Console.WriteLine(movie);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not search movies.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        private async Task SearchMoviesByCountryNameAsync()
        {
            try
            {
                Console.WriteLine("\n***Search Movies by Country Name***");

                string countryNameToSearch = ConsoleUtil.ReadString("Country Name To Search: ");

                IEnumerable<Movie> movies = await _movieService.GetByCountryNameAsync(countryNameToSearch);

                foreach (var movie in movies)
                {
                    Console.WriteLine(movie);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not search movies.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        private async Task SearchMoviesByDicrectorNameAsync()
        {
            try
            {
                Console.WriteLine("\n***Search Movies by Director Name***");

                string directorNameSearch = ConsoleUtil.ReadString("Director Name To Search: ");

                IEnumerable<Movie> movies = await _movieService.GetByDirectorNameAsync(directorNameSearch);

                foreach (var movie in movies)
                {
                    Console.WriteLine(movie);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not search movies.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        private async Task SearchMoviesByTitleAsync()
        {
            try
            {
                Console.WriteLine("\n***Search Movies by Title***");

                string titleToSearch = ConsoleUtil.ReadString("Title To Search: ");

                IEnumerable<Movie> movies = await _movieService.GetByTitleAsync(titleToSearch);

                foreach (var movie in movies)
                {
                    Console.WriteLine(movie);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not search movies.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        private async Task SearchDetailMovieById()
        {
            try
            {
                Console.WriteLine("\n***Search Detail Movie by Id***");

                int movieIdToSearch = ConsoleUtil.ReadInt("Movie Id: ");

                Movie? movie = await _movieService.GetDetailByIdAsync(movieIdToSearch);

                if (movie is null)
                {
                    Console.WriteLine($"Movie id: {movieIdToSearch} not existed");
                    return;
                }

                Console.WriteLine(movie);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not search movie.");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }
        #endregion
    }
}
