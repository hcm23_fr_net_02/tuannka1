﻿using MovieManagement.ConsoleApplication.Contracts;
using MovieManagement.ConsoleApplication.Utils;

namespace MovieManagement.ConsoleApplication
{
    public class Application
    {
        private readonly IMoviePresentation _moviePresentation;

        public Application(IMoviePresentation moviePresentation)
        {
            _moviePresentation = moviePresentation;
        }

        public async Task RunAsync()
        {
            var applicationName = "Movie Management Application";
            Console.WriteLine($"***{applicationName}***");

            var menuName = "Main Menu";
            var options = new Dictionary<string, Func<Task>>()
            {
                { "View Movies", _moviePresentation.ViewMoviesAsync },
                { "Add Movie", _moviePresentation.AddMovieAsync },
                { "Search Movies", _moviePresentation.SearchMoviesAsync },
            };

            await MenuUtil.ShowMenuAsync(menuName, options);

            //Exit 
            Console.WriteLine("\nSee you again");
        }
    }
}
