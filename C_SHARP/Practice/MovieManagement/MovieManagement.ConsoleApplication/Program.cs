﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MovieManagement.Application.Contracts.IRepository;
using MovieManagement.Application.Contracts.IService;
using MovieManagement.Application.Services;
using MovieManagement.ConsoleApplication;
using MovieManagement.ConsoleApplication.Contracts;
using MovieManagement.ConsoleApplication.Presentations;
using MovieManagement.Infrastructure;
using MovieManagement.Infrastructure.Repositories;

IHostBuilder hostBuider = Host.CreateDefaultBuilder();

hostBuider.ConfigureServices((hostContext, services) =>
{
    services.AddDbContext<ApplicationDbContext>(options =>
        options
            .UseSqlServer(hostContext.Configuration.GetConnectionString("DefaultConnection"))
            .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole().SetMinimumLevel(LogLevel.None)))
    );

    services.AddScoped<Application>();

    services.AddScoped<IUnitOfWork, UnitOfWork>();

    services.AddScoped<IMovieService, MovieService>();

    services.AddScoped<IMoviePresentation, MoviePresentation>();
});

IHost host = hostBuider.Build();

var app = host.Services.GetRequiredService<Application>();

await app.RunAsync();