﻿namespace MovieManagement.ConsoleApplication.Contracts
{
    public interface IMoviePresentation
    {
        Task AddMovieAsync();
        Task ViewMoviesAsync();
        Task SearchMoviesAsync();
    }
}
