﻿using MovieManagement.Domain.Entities;
using MovieManagement.Domain.Shared.Common;

namespace MovieManagement.Application.Contracts.IService
{
    public interface IMovieService
    {
        Task<IEnumerable<Movie>> GetPagingAsync(Paging paging);
        Task<IEnumerable<Movie>> GetByTitleAsync(string title);
        Task<IEnumerable<Movie>> GetByDirectorNameAsync(string directorName);
        Task<IEnumerable<Movie>> GetByCountryNameAsync(string countryName);
        Task<IEnumerable<Movie>> GetByPublicationYearAsync(short publicationYear);
        Task<Movie> GetDetailByIdAsync(int id);
        Task AddReviewByIdAsync(int id, Review entityReview);
        Task CreateAsync(Movie Movie);
        Task UpdateAsync(Movie Movie);
        Task<bool> DeleteAsync(int id);
    }
}
