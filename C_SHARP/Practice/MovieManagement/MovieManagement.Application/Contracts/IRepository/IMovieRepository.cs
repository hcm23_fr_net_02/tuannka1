﻿using MovieManagement.Domain.Entities;

namespace MovieManagement.Application.Contracts.IRepository
{
    public interface IMovieRepository : IRepository<Movie>
    {
        Task<Movie?> GetDetailByIdAsync(int id, bool tracked = false);
    }
}
