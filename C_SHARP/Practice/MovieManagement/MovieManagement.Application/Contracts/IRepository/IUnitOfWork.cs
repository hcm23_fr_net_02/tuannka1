﻿namespace MovieManagement.Application.Contracts.IRepository
{
    public interface IUnitOfWork
    {
        IMovieRepository Movie { get; }
        Task SaveAsync();
    }
}
