﻿using MovieManagement.Application.Contracts.IRepository;
using MovieManagement.Application.Contracts.IService;
using MovieManagement.Domain.Entities;
using MovieManagement.Domain.Shared.Common;
using System.Net.NetworkInformation;

namespace MovieManagement.Application.Services
{
    /// <summary>
    /// Service for managing Movies.
    /// </summary>
    public class MovieService : IMovieService
    {
        #region Private Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MovieRepository"></param>
        public MovieService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entityReview"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Task AddReviewByIdAsync(int id, Review entityReview)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create a new Movie.
        /// </summary>
        /// <param name="Movie"></param>
        /// <returns></returns>
        public async Task CreateAsync(Movie Movie)
        {
            try
            {
                await _unitOfWork.Movie.AddAsync(Movie);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                Movie? objFromRepo = await _unitOfWork.Movie.GetAsync(u => u.Id == id);
                if (objFromRepo is null)
                {
                    return false;
                }

                await _unitOfWork.Movie.RemoveAsync(objFromRepo);
                await _unitOfWork.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Movie?> GetByIdAsync(int id)
        {
            try
            {
                return await _unitOfWork.Movie.GetAsync(b => b.Id == id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Movie?> GetDetailByIdAsync(int id)
        {
            try
            {
                return await _unitOfWork.Movie.GetDetailByIdAsync(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetPagingAsync(Paging paging)
        {
            return await _unitOfWork.Movie.GetPagingAsync(paging: paging);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryName"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetByCountryNameAsync(string countryName)
        {
            return await _unitOfWork.Movie.GetAllAsync(m => m.CountryName.ToLower().Contains(countryName.ToLower()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directorName"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetByDirectorNameAsync(string directorName)
        {
            return await _unitOfWork.Movie.GetAllAsync(m => m.DicrectorName.ToLower().Contains(directorName.ToLower()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="publicationYear"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetByPublicationYearAsync(short publicationYear)
        {
            return await _unitOfWork.Movie.GetAllAsync(m => m.PublicationYear == publicationYear);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetByTitleAsync(string title)
        {
            return await _unitOfWork.Movie.GetAllAsync(m => m.Title.ToLower().Contains(title.ToLower()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Movie"></param>
        /// <returns></returns>
        public Task UpdateAsync(Movie Movie)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
